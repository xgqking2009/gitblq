/**
 * 设置LayaNative屏幕方向，可设置以下值
 * landscape           横屏
 * portrait            竖屏
 * sensor_landscape    横屏(双方向)
 * sensor_portrait     竖屏(双方向)
 */
window.screenOrientation = "sensor_landscape";

//-----libs-begin-----
loadLib("libs/laya.core.js")
loadLib("libs/laya.webgl.js")
loadLib("libs/laya.ui.js")
loadLib("libs/laya.d3.js")
loadLib("libs/ammo.js")
//-----libs-end-------
loadLib("js/game/blq.utils.js")
loadLib("js/game/blq.graphic.js")
loadLib("js/game/blq.phy.js")
loadLib("js/game/blq.input.js")
loadLib("js/game/blq.logic.js")
loadLib("js/bundle.js");
