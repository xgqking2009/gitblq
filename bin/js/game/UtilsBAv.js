GGlobal.Banners = {
  "home":"adunit-ff51e8097c80072f",
  "gamejs00":"adunit-6205f3bfb7578f88",
  "gamejs01":"adunit-c89b0c47fa123d11",
  "sign":"adunit-3f34179a58642c80",
  "invite":"adunit-39084d05737c145f"
}
GGlobal.createBannerAd = function(where,show,x,y,w,h){
  if(wx.createBannerAd){
    if(GGlobal.bannerAd) GGlobal.bannerAd.hide();
    var bannerAd;
    var sx,sy,sw,sh;
    if(!w&&!h){
      sy = x;sw = y;sx = (Laya.Browser.clientWidth - sw)/2;
      bannerAd = wx.createBannerAd({adUnitId: GGlobal.Banners[where], style: {left: sx, top: sy, width:sw } }) }
    else{
      bannerAd = wx.createBannerAd({adUnitId: GGlobal.Banners[where], style: {left: x, top: y, width:w, height:h } }) 
    }
    bannerAd.onError(err => {console.log(err);})
    if(show==true) bannerAd.show();else bannerAd.hide();
    GGlobal.bannerAd = bannerAd;
  }
}
GGlobal.Avs = {
  "sign":"adunit-a94c67c05fef8e3c",
  "chou":"adunit-bea5b9c5fdc586c4",
  "game0d":"adunit-5b927d3034d88e7f",
  "shopblq":"adunit-4d703508f761b624",
  "shopcj":"adunit-d77fc59f8395c627",
  "jlq":"adunit-d77fc59f8395c627"
};
GGlobal.showAdvertisement = function(where,callback){
  if(!wx.login){callback();return;}
  let adunit = GGlobal.Avs[where];
  let videoAd = wx.createRewardedVideoAd({ adUnitId: adunit });
  videoAd.onClose(function (s) {
    if(s){
      if(s.isEnded==false){
        eftFloat("视屏未观看完",'#000000',600,72);
      }else{
        eftFloat("获取激励",'#000000',600,72);
        callback();
      }
    }
    videoAd.offClose(this);
  });
  videoAd.onError(function (s) {videoAd.offError(this); });
  videoAd.load().then(() => videoAd.show()) .catch(
    function(err){
      // console.log(err.errMsg);
      eftFloat("观看视频太多，稍后再来",'#000000',600,60);
    }
  )
}
GGlobal.Navs = {
  "推广1":["wx120efbb1cc482428","pages/index/index?chid=11","","adv/bzicon.jpg"],
  "推广2":["wx120efbb1cc482428","pages/index/index?chid=13",""]
}
GGlobal.NavigateSkin = function(where){
  var nvs = GGlobal.Navs[where];
  return nvs[3];
}
GGlobal.NavigateTo = function(where,call_back){
  var nvs = GGlobal.Navs[where];
  var appid = nvs[0],path = nvs[1],param = nvs[2];

  wx.navigateToMiniProgram({
    appId: appid,
    path: path,
    extraData: param,
    envVersion: "release", // trial develop release
    success: function (res) {
        call_back && call_back(true, res);
    },
    fail: function (res) {
        call_back && call_back(false, res);
    },
  });
}

window.refreshAdvBar = function(advBar,appid){
    var advIcon = advBar.getChildByName("图标");
    var btnTry = advBar.getChildByName("试玩");
    var btnGet = advBar.getChildByName("领取");
    var btnGo = advBar.getChildByName("去玩");

    
    advIcon.skin = _r+GGlobal.NavigateSkin("推广1");
    var astate = GGlobal.sgame.advs[appid];
    if(!astate){
      btnTry.visible = true;btnGet.visible = btnGo.visible = false;
      btnTry.setOnClick(function(){
        GGlobal.AdvBarTime = performance.now();
        GGlobal.NavigateTo("推广1");
        GGlobal.AdvAppid = appid;
      })
    }else{
      if(astate==1){
        btnGet.visible = true;btnTry.visible = btnGo.visible = false;
        btnGet.setOnClick(function(arg){
          GGlobal.sgame.advs[arg] = 2;
          GGlobal.sgame.jlq+=2;
          refreshAdvBar(advBar,appid);
          refreshMSNum();
          GGlobal.SaveItem(null,null,true);
        },null,[appid])
      }else if(astate==2){
        btnGo.visible = true;btnTry.visible = btnGet.visible = false;
        advBar.getChildByName("奖励项").visible = false;
        btnGo.setOnClick(function(){
          GGlobal.NavigateTo("推广1");
        })
      }
    }
  }
GGlobal.Share = function(scene,callback){
  if(!wx.login){
    if(callback) callback();
  }
  console.log("Xgq share:"+scene);
  var data;
  var str = [
  "你可以很轻松的击倒一侧的单个球瓶吗？",
  "一球在手，呼朋唤友。",
  "来这里证明你是一个真正的“保龄球高手”！",
  "帅哥，保龄球了解一下？！滚滚更健康！"
  ];
  var nr = Math.floor(Math.random()*str.length);
  var ir = Math.floor(Math.random()*3);
  var surl = _r+"sharer"+(ir+1)+".jpg";
  if(scene=="Battle"){
    data = {title:"痛痛快快的来一场——保龄球！", imageUrl:surl, query:{openId:GGlobal.openId, roomId:GGlobal.roomId, mode:'BF'} } }
  else if(scene=="Inv"){
    surl = _r+"shareMv.jpg";
    data = {title:"与美女一起打保龄球，感觉就像在天堂！", imageUrl:surl, query:{openId:GGlobal.openId, mode:'Inv'} } }
  else {
    if(nr==0){
      surl = _r+"share1.jpg";
    }
    data = {
        title:str[nr],
        imageUrl:surl
    }
  }
  GGlobal.ShareStartTime = performance.now();
  GGlobal.ShareScene = scene;
  GGlobal.ShareBack = callback;
  function Share(data, call_back) {
    let query = undefined;
      if (data.query) {
          query = "";
          for (const k in data.query) {
              if (data.query.hasOwnProperty(k)) {
                  query += "&" + k + "=" + data.query[k];
              }
          }
          query.length > 1 && (query = query.substr(1));
      }
      if(wx.shareAppMessage){
          wx.shareAppMessage({
              title: data.title,
              imageUrl: data.imageUrl,
              query: query,
              success: function (res) {
                  console.log("share success !!!", res);
                  call_back && call_back(true, res);
              },
              fail: function (res) {
                  console.log("share fail !!!", res);
                  call_back && call_back(false, res);
              },
          });
      }
  }
  Share(data);
}
    // showAdvertisement: function (adunit, callback) {
    //     let videoAd = wx.createRewardedVideoAd({ adUnitId: adunit });
    //     debug_log("videoAd: ", videoAd);
        
    //     videoAd.onClose(function (s) {
    //         callback && callback(true, { isEnded: Boolean(s && s.isEnded || s === undefined) });
    //         videoAd.offClose(this);
    //     });
    //     videoAd.onError(function (s) {
    //         console.error(s);
    //         callback && callback(false, s);
    //         videoAd.offError(this);
    //     });
    //     videoAd.load().then(() => videoAd.show()).catch(err => {
    //         console.error(err);
    //         callback && callback(false, err);
    //     });
    // },