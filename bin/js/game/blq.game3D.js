window.blqScene3DChange = function(){
  if(GameGlobal.gameMode==1)
  {
    blqTurnPOrder();
    onNextLevel();
    blqLevelResetPost();
    recreateBlps();
    blqRefreshAv();
    refreshItemScore();
  }
}
window.blqScene3DFirst = function(){
  onNextLevel();
  resetOneBall();
  blqRefreshAv();
  refreshItemScore();
  var stime = 0;
  var nball = 0;
  var nlevel = 0,nblp = 0,lb = 0;
  var b = GGlobal.gmachined,db = 1;
  
  if(GameGlobal.IsMachine==true){
    var Prd = {nlevel:0,lb:0,v:0};
    var op = 1-GameGlobal.matchMeOrder;
    var m = GameGlobal.matchMeOrder;
    ftime = MRandom(b*1000,(b+db)*1000);
    Laya.timer.handlerLoop("GMTime",16,function(){
    stime+=16;
    if(stime>ftime){
      stime = 0;
      ftime = MRandom(b*1000,(b+db)*1000);
      var fv = 0;
      var lmap = processLDataMap(op,0);
      if(lmap.lb==1){
        fv = 10-Prd.v;
      }else if(lmap.lb==2){
        fv = Prd.v==10?10:(10-Prd.v);
      }
      v = MGenV(lmap.nlevel,lmap.lb,fv);

      var fb = false;
      if(GameGlobal.MMeLp==1||GameGlobal.MMeLevel==10) fb = true;
      else{
        if(GameGlobal.MMeLp==0&&GameGlobal.GameLevelScore[m][GameGlobal.MMeLevel]==10){
          fb = true;
        }
      }
      if(GameGlobal.MBiaoQin==true&&fb==true){
        var mscore = GameGlobal.GameLevelScore[m][GameGlobal.MMeLevel];
        var mr = Math.random();
        var gds = [1,3,4,6,7,9];var bds = [0,2,5,8,10,11,12,13,14];
        if(mr<0.5){
          if(mscore>=10){
            var nr = Math.floor(Math.random()*9);
            animBiaoQin(op,bds[nr]);
          }else{
            var nr = Math.floor(Math.random()*6);
            animBiaoQin(op,gds[nr]);
          }
        }
        GameGlobal.MBiaoQin = false;
      }
      Prd = processLData(op,GGlobal.GPlayer?GGlobal.GPlayer[nball]:v);
      if(Prd.ed == 1){
        GameGlobal.PEnds[op] = true;
      }
      var bps = GameGlobal.BpData[op];
      var ld = bps[bps.length-1];
      GameGlobal.BpData[op] = bps.slice(0,bps.length-1);
      var lmap = processLData(op,ld);
      if(lmap.ed==1) {
        GameGlobal.PEnds[op] = true;
        Laya.timer.handlerClear("GMTime");
      }
      else{
        var nmap = processLDataMap(op,0);
        GameGlobal.gameLevel[op] = nmap.nlevel;
      }
      refreshItemScore();
    }
  });
  }


  if(GameGlobal.gameMode==0||(GameGlobal.gameMode==1&&GameGlobal.turnMe==true)){
    blqStartFinger();
    var pose = new Laya.Vector3((GameGlobal.BPO==0?0:1000)+GameGlobal.SBOX,0.8400001525878906,12);
    var ball = sceneAddBall(GameGlobal.scene,pose);
    GameGlobal.ballinput = GameGlobal.ball;
    ball.meshRenderer.material.albedoTexture = GameGlobal.globalres[_r+"tex/blq"+(GGlobal.sgame.useq+1)+".jpg"];
  }else{
    GameGlobal.gameUi.sprFinger.visible = false;
  }
}

window.blqWaitBattleTime = function(){
  var stime = 0,atime = GGlobal.WaitPlayerTime*1000,bw = 350;
  blqClearBattleTime();
  GameGlobal.gameUi.pBattleTime.visible = true;
  if(GameGlobal.IsWaitMe){
    GameGlobal.gameUi.iWaitO.visible = false;
    GameGlobal.gameUi.iWaitMe.visible = true;
  }else{
    GameGlobal.gameUi.iWaitO.visible = true;
    GameGlobal.gameUi.iWaitMe.visible = false;
  }
  GameGlobal.gameUi.cBTime.width = bw;
  GameGlobal.gameUi.txtBTime.text = Math.floor((atime-stime)/1000)+"s";
  Laya.timer.handlerLoop("pwTimeLoop",1000,function(){
    stime+=1000;
    GameGlobal.gameUi.cBTime.width = (atime-stime)/atime*bw;
    GameGlobal.gameUi.txtBTime.text = Math.floor((atime-stime)/1000)+"s";
    if(stime>=atime){
      blqClearBattleTime();
      console.log("stime>=atime");
      onGameJs();
    }
  });
}

window.blqStartFinger = function(){
  var stime = 0,ey = GGlobal.FingerEy,atime = 1000,sc = 0.18;
  Laya.timer.handlerClear("fingerTimeLoop");
  var pose = new Laya.Vector3((GameGlobal.BPO==0?0:GameGlobal.BOY)+2000,0.8400001525878906,12);
  var ball = GameGlobal.ball;
  if(ball) {
    ball.transform.position = new Laya.Vector3(pose.x,pose.y,pose.z);
    ball.meshRenderer.material.albedoTexture = GameGlobal.globalres[_r+"tex/blq"+(GGlobal.sgame.useq+1)+".jpg"];
  }
  GameGlobal.gameUi.sprFinger.y = ey;GameGlobal.gameUi.sprFinger.visible = true;
  Laya.timer.handlerLoop("fingerTimeLoop",16,function(){
    stime+=16;
    if(stime>atime) stime = 0;
    GameGlobal.gameUi.sprFinger.y = ey-stime*sc;
  });
}
window.blqStartBattleTime = function(){
  fingerOp();
  GameGlobal.playerOrder = GameGlobal.matchMeOrder;
}
window.blqClearBattleTime = function(){
  GameGlobal.gameUi.pBattleTime.visible = false;
  Laya.timer.handlerClear("pwTimeLoop");
}
window.blqTurnPOrder = function(){
  if(GameGlobal.gameMode == 1){
    GameGlobal.playerOrder =  GameGlobal.matchMeOrder;// GameGlobal.playerOrder==1?0:1;
  }
  blqLevelReset(0);//tp 1
  fingerOp();
}
window.blqLevelReset = function(rtip){
  GameGlobal.battleSendOnce = true;
  GameGlobal.bRoundTip = rtip;
  GameGlobal.thinkingFhqOnce = true;
  GameGlobal.MachineOnce = 1;
  GameGlobal.nBlpDownCount = 0;
  GameGlobal.ballinput = null;
  GameGlobal.thinking = false;
  GameGlobal.gameSData = [];
  GameGlobal.playerOrder = GameGlobal.matchMeOrder;//notp
  GameGlobal.BPO = GameGlobal.playerOrder;
  GameGlobal.BPO = 0;
  GameGlobal.gameTimes[GameGlobal.matchMeOrder] = GameGlobal.MaxLevelTimes;
  GameGlobal.turnMe = (GameGlobal.matchMeOrder==GameGlobal.playerOrder);
  if(GameGlobal.turnMe) GameGlobal.cshootball = true;
  var kv = GameGlobal.playerOrder+1;
  GGlobal.imgAv3 = GGlobal["imgAv"+kv];GGlobal.txtAv3 = GGlobal["txtAv"+kv];
}
window.blqLevelResetPost = function(){
  var lv = GameGlobal.gameLevel[GameGlobal.playerOrder];
  for(var t = 0;t<3;t++){
    GameGlobal.Rounds[lv][""+GameGlobal.playerOrder+"blp"+t] = null;
    var tqiu = GameGlobal.Rounds[lv].getChildByName("第"+(t+1)+"球");
    if(tqiu) delete tqiu["v"+GameGlobal.playerOrder];
  }
  GameGlobal.GameLevelScore[GameGlobal.playerOrder][lv] = -1;
}
window.blqInitGame3D = function(mode){
  nptTest();
  initPhysics();
  initInput();
  GGlobal.joinBattle = false;
  if(GameGlobal.homeUi) GameGlobal.homeUi.visible = false;

  var lgameCongifg = [2,50,0.43,0.442,8,10.8, 0.25,0.25,0,0];
  for(var k = 1;k<10;k++){
    GGlobal.gameConfig[k] = lgameCongifg[k];
  }

  for(var k = 0;k<GameGlobal.RedTime.length;k++){
    Laya.timer.handlerClear("RedTime"+k);
  }
  var GConfig = GameGlobal.GConfig;
  var sw = GGlobal.maxscoreI>GGlobal.gameConfig[1]?1:
      GGlobal.maxscoreI/GGlobal.gameConfig[1];
  var bpw = GGlobal.gameConfig[2]*(1-sw)+GGlobal.gameConfig[3]*sw;
  var bpz = GGlobal.gameConfig[4]*(1-sw)+GGlobal.gameConfig[5]*sw;
  GConfig.blpw = bpw*GConfig.Gs;
  GConfig.blpz = bpz*GConfig.Gs-8;
  GConfig.gz = bpz*GConfig.Gs*2+5;
  GConfig.mvs = sw;
  GConfig.jzmin = GGlobal.gameConfig[6];GConfig.jzmax = GGlobal.gameConfig[7];
  window.Vector3 = Laya.Vector3;var tempBtVec3_1 = GameGlobal.tempBtVec3_1; var scene = GameGlobal.scene; var GConfig = GameGlobal.GConfig; scene.removeChildren();
  GameGlobal.camera = camera = (scene.addChild(new Laya.Camera(window.innerWidth / window.innerHeight, 0.1, 500)));camera.fieldOfView = GConfig.cf; camera.transform.translate(new Laya.Vector3(GConfig.cx, GConfig.cy, GConfig.cz)); camera.transform.rotate(new Laya.Vector3(-GConfig.crx, 0, 0), false, true);camera.clearColor = null;
  var directionLight = scene.addChild(new Laya.DirectionLight());directionLight.color = new Laya.Vector3(0.07, 0.07, 0.07);
  directionLight.transform.worldMatrix.setForward(new Laya.Vector3(1, -1, -1)); directionLight.shadow = true; 
  directionLight.shadowDistance = 100; directionLight.shadowResolution = 1024; directionLight.shadowPSSMCount = 1; 
  directionLight.shadowPCFType = 10;
  var material = null,material2 = null; for(var vk = 0;vk<1;vk++){GameGlobal.BPO = vk; 
  var box = createParalellepipedWithPhysics(GConfig.gx,GConfig.gz,2,0,0,0,material);if(box) box.meshRenderer.receiveShadow = true; createParalellepipedWithPhysics(GConfig.gcx,GConfig.gz,2,-GConfig.gx/2-GConfig.gcx/2,-GConfig.gcy/2,0,material2); createParalellepipedWithPhysics(GConfig.gcy,GConfig.gz,3,-GConfig.gx/2-GConfig.gcx,-GConfig.gcy,0,material); createParalellepipedWithPhysics(GConfig.gcx,GConfig.gz,2,GConfig.gx/2+GConfig.gcx/2,-GConfig.gcy,0,material2); createParalellepipedWithPhysics(GConfig.gcy,GConfig.gz,-3,GConfig.gx/2+GConfig.gcx,-GConfig.gcy/2,0,material); }
  var materialStart = new Laya.BlinnPhongMaterial();
  materialStart.albedoTexture = GameGlobal.globalres["res/startline.png"];
  GameGlobal.startLine = createParalellepiped(9.5,1.8,0.1,0,0.05,2,materialStart);
  materialStart.renderMode = 3;
  materialStart.albedoIntensity = 3;
  materialStart.albedoColor = GGlobal.sgame.usec==0?new Laya.Vector4(0.9,0.7,0.2,0.2):new Laya.Vector4(0.2,0.7,0.9,0.2);

  GameGlobal.GMEnd = false;
  var fx = GConfig.blpsx,fw = GConfig.blpw,fy = GConfig.blpy+10,fz = -GConfig.blpz;
  function addCJ(y,l){
    var p = new Laya.MeshSprite3D(GameGlobal.globalres[GGlobal.sgame.usec==0?
      (_r+"cj_01.lm"):
      (_r+"xuedi-cao.lm")]);
    p.transform.scale = new Laya.Vector3(10,10,10);
    p.transform.position = new Laya.Vector3(y,GGlobal.sgame.usec==0?1.45:0,(GGlobal.sgame.usec==0?128:145)+(67-GConfig.blpz));
    GameGlobal.scene.addChild(p);
    var materialc = new Laya.BlinnPhongMaterial();
    materialc.albedoTexture = GameGlobal.globalres[GGlobal.sgame.usec==0?(_r+"cj_01.jpg"):
    (_r+"cj_02.jpg")];
    materialc.albedoIntensity = l;
    p.meshRenderer.material = materialc;
    p.meshRenderer.receiveShadow = true;
  }
  addCJ(0,4.4);
  // addCJ(1000,3);
  function addBY(y,l){
    var materialc = new Laya.BlinnPhongMaterial();
    materialc.albedoTexture = GameGlobal.globalres[_r+"sky_baiyun.jpg"];
    materialc.albedoIntensity = l;
    var p = new Laya.MeshSprite3D(GameGlobal.globalres["res/sky-sky.lm"]);
    p.transform.rotationEuler = new Laya.Vector3(-90, 180, 0);
    p.transform.scale = new Laya.Vector3(10,10,10);
    p.transform.position = new Laya.Vector3(y, 0, 0);
    GameGlobal.scene.addChild(p);
    p.meshRenderer.material = materialc;
  }
  addBY(0,4.4);
  // addBY(1000,3);
  for(var ky = 0;ky<1;ky++){GameGlobal.BPO = ky;var kblp = 0;
    for (var k = 4; k > 0; k--) {
      for (var j = 0; j < k; j++) {
        var m = (k-1)/2;var x = fx + fw * (j-m) + ky*1000,y = 1.547, z = fz - (k-1) * GConfig.blph;var npos = new Vector3(x,y,z);
        var material = new Laya.BlinnPhongMaterial(); material.albedoTexture = GameGlobal.globalres["res/blp.jpg"]; 
        material.albedoColor = new Laya.Vector4(1, 1, 1, 0.5); material.albedoIntensity = 3.4; material.renderMode = 0; var blp = GameGlobal.scene.addChild(new Laya.MeshSprite3D(GameGlobal.globalres["res/blpms_m-blp_01.lm"]));
        blp.meshRenderer.material = material; blp.transform.name = ""+ky+"_blp"+kblp; blp.idx = k*100+j; createBlp(blp,0,npos,null,null);kblp++;
  }}}
  if(GameGlobal.gameMode==0)
    GameGlobal.playerOrder = 0;
  else
    GameGlobal.playerOrder = GameGlobal.matchMeOrder;
  blqScene3DFirst();
  GameGlobal.InGame = true;

//   //添加灯光投影 light.shadow=true; //产生投影的范围（如过小将不会产生投影） light.shadowDistance=45; //生成阴影贴图数量 light.shadowPSSMCount = 1; //模糊等级,越大越高,效果更好，更耗性能 light.shadowPCFType=1; //投影质量 light.shadowResolution=2048;
// 材质需要设置

// //产生阴影 sphere.meshRender.castShadow=true; //接受阴影 box.meshRender.receiveShadow=true;
}
