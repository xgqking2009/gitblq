window.processPLevelPre = function(nr){
  if(nr==0){
    var sd = GameGlobal.glvsd[nr];var sdstr = sd[0];var sdo = sd[2];
    GameGlobal.TotalLevel = sdo[0];
  }
}
window.refreshHomeLevel = function(){
  GameGlobal.homeUi.iGLvsBack.skin = _r+"glvs/hback.png";
  var si = getRankingLevel(GGlobal.sgame.glvs.lv,GameGlobal.glvsl);
  var s = false;
  GameGlobal.homeUi.iGLvs.skin = _r+"glvs/h"+(si[0]+1)+".png";
  if(si[1]-si[2]==GameGlobal.glvsl[si[0]]&&si[0]!=0) s = true;
  if(s==true){
    GameGlobal.homeUi.iGLvs.gray = true;
  }
}

window.getPLevelStar = function(lv){
  var sdo = GameGlobal.SDO;var glvs = GGlobal.sgame.glvs;
  var p = GameGlobal.playerOrder;
  var glvsd = GameGlobal.glvsd;
  var nr = Math.floor(lv/5),r = lv%5;
  var star = 0;
  if(nr==0){
    for(var k = 3;k>0;k--){
      var cv = glvsd[nr][2+r][2*k-1];
      if(GameGlobal.GameScore[p]>=cv){
        star = k;
        break;
      }
    }
  }
  return star;
}

window.getPLevelLvs = function(lv){
  var sdo = GameGlobal.SDO;var glvs = GGlobal.sgame.glvs;
  var p = GameGlobal.playerOrder;
  var glvsd = GameGlobal.glvsd;
  var nr = Math.floor(lv/5),r = lv%5;
  var star = 0;
  var lvs = [];
  for(var k = 1;k<=3;k++){
    lvs.push(glvsd[nr][2+r][2*k-1]);
  }
  return lvs;
}
//0 局数数据 1 反悔 2 表情 3 结束
window.processLDataMap = function(p,vd,n){
  var bpd = GameGlobal.BpData[p];
  var nlevel = 0,lb = 0,ed = 0,rbp = 0,eft = 0,v0 = 0,pv = 0;
  var nextlevel = 0;
  // GameGlobal.TotalLevel = 5;
  // bpd = [1,6,1,0,7,0,5,4,9,1,6,4,7,1,6,0,0,0,10];
  // vd = 0;
  if(bpd.length==0){
    if(vd==10){
      eft = 1;
      rbp = 1;
    }
  } 
  if(bpd.length!=0){
    var nlevel = 0;
    var lhit = 0;
    lb = -1;
    for(var k = 0;k<bpd.length+1;k++){
      var v = bpd[k];
      var l = k==bpd.length-1;
      var n = k==bpd.length;
      lb++; 
      if(n){
        v = pv;
      }
      if(nlevel<GameGlobal.TotalLevel-1){
        if(lb==2){
          nlevel++;lb = 0;
        }
        if(lb==0&&v==10&&!n){
          if(nlevel<GameGlobal.TotalLevel-1){
            nlevel++;lb = -1;
          }
        }
      }else if(nlevel==GameGlobal.TotalLevel-1){
        if((lb==1&&v+vd<10)||(lb==2))
          ed = 1;
      }
      if(n){
        if(lb==0&&vd==10){
          rbp = 1;eft = 1;
        }else if(lb==1){
          rbp = 1;if(v+vd==10) eft = 2;
          v0 = v;
        }
      }
      pv = v;
    }
  }
  return {nlevel:nlevel,lb:lb,ed:ed,v:vd,rbp:rbp,v0:v0,eft:eft};
}
window.MGenV = function(l,lb,fv){
  var vdp = GGlobal.gameConfig;
  var rs = [[vdp[12],vdp[13]],[vdp[14],vdp[15]],[vdp[16],vdp[17]],[vdp[18],vdp[19]]];
  if(GGlobal.TestD&&GGlobal.TestI<GGlobal.TestD.length){
    return GGlobal.TestD[GGlobal.TestI++];
  }
  function VLevel(pl,w){
    var v = rs[w][0]*(1-pl)+rs[w][1]*pl;
    return v;
  }
  var s = GGlobal.maxscoreI/49;
  var sI = Math.pow(s,0.6)*49
  var tpl = (vdp[10]+sI)/vdp[11],rv = 0;
  var r = Math.random();
  var vr;
  if(lb==0||lb==1) vr = VLevel(tpl,lb);
  else vr = VLevel(tpl,3);

  if(lb==0||lb==1){
    if(r<vr){
      if(lb==0) rv = 10;
      else {
        rv = fv;
      }
    }else{
      r = Math.random();
      if(lb==0) {
        fv = 9;
        vr = VLevel(tpl,2);
        var pv = Math.pow(r,vr);
        rv = Math.floor(fv*pv);
      }else {
        vr = VLevel(tpl,2);
        var pv = Math.pow(r,vr);
        rv = Math.floor(fv*pv);
      }
    }
  }else{
    r = Math.random();
    var pv = Math.pow(r,vr);
    rv = Math.floor(fv*pv);
  }
  return rv;
}
window.processLDataAdd = function(lmap,p,v){
  var nlevel = lmap.nlevel,lb = lmap.lb;
  SetRoundsDataLv(p,nlevel,lb,v);
  GameGlobal.BpData[p].push(v);
}
window.processLData = function(p,v){
  if(v<0){
    var at = 1;
  }
  var lmap = processLDataMap(p,v);
  var nlevel = lmap.nlevel,lb = lmap.lb;
  SetRoundsDataLv(p,nlevel,lb,v);
  GameGlobal.BpData[p].push(v);
  return lmap;
}
window.nptTest = function(){
  var utime = performance.now()+86400*3;
  var un = Math.floor(utime/86400)%7;
  console.log("Xgq un:::"+un);
  return;
  var op = 0;
  var fv = 0;
  var tps = {};
  for(var k = 1;k<=49;k++){
    tps[""+k] = 0;
    var s = k/49;
    GGlobal.maxscoreI = k;
    for(var kr = 0;kr<100;kr++){
      var REnd = false;
      var Prd = {nlevel:0,lb:0,v:0};
      GameGlobal.BpData[op].length = 0;
      while(!REnd){
        var lmap = processLDataMap(op,0);
        if(lmap.lb==1){
          fv = 10-Prd.v;
        }else if(lmap.lb==2){
          fv = Prd.v==10?10:(10-Prd.v);
        }
        v = MGenV(lmap.nlevel,lmap.lb,fv);
        // console.log("Xgq Lv[lmap]:"+v+"===="+lmap.nlevel+",lb:"+lmap.lb+",fv:"+fv);
        Prd = processLData(op,v);
        if(Prd.ed == 1){
          GameGlobal.PEnds[op] = true;
        }
        var bps = GameGlobal.BpData[op];
        var ld = bps[bps.length-1];
        GameGlobal.BpData[op] = bps.slice(0,bps.length-1);
        var lmap = processLData(op,ld);
        if(lmap.ed==1) {
          REnd = true;
          refreshItemScore();
          tps[""+k]+=GameGlobal.GameScore[op];
          if(kr==99){
            tps[""+k]/=100;
            console.log("Xgq ["+k+"]:"+tps[""+k]);
          }
        }
      }
    }
  }
}

