window.blqGoGame = function(where){
  GameGlobal.GameWhere = where;
  var that = this;
  Laya.Scene.open("test/gameUI.scene",true,Laya.Handler.create(null,function(scene){
    GameGlobal.gameUi = scene;
    if(GameGlobal.homeUi) GameGlobal.homeUi.visible = false;
    blqInitGame(that);
  }));
}

window.blqGameUiLoop = function(){
  // if(GameGlobal.gameMode==1){
    if(GameGlobal.bRoundTip>0){
      var vi;var ri = GameGlobal.gameUi.pRoundInfo;var isMe = GameGlobal.matchMeOrder==GameGlobal.playerOrder;
      ri.visible = true;
      {
        vi = ri.getChildByName("玩家使用");
        vi.visible = true;vi.alpha = 0;
        if(GameGlobal.bRoundTip==2){vi.getChildByName("反悔券").visible = true;vi.getChildByName("黄金球").visible = false;}
        else{vi.getChildByName("黄金球").visible = true;vi.getChildByName("反悔券").visible = false;}
        Laya.Tween.to(vi,{alpha:1},300,Laya.Ease.linearNone,Laya.Handler.create(null,function(){
            Laya.Tween.to(vi,{alpha:1},200,Laya.Ease.linearNone,Laya.Handler.create(null,function(){
              Laya.Tween.to(vi,{alpha:0},300,Laya.Ease.linearNone);
            }))
        }));
      }
      GameGlobal.bRoundTip = 0;
    }
}

window.GoToHome = function(){
  var that = this;
  function CloseDo(){
    Laya.Scene.open("test/home.scene",null,Laya.Handler.create(null,function(hscene){
        blqInitHome(hscene,that);
    }));
  }
  if(GameGlobal.gameMode==0||GameGlobal.IsMachine==true)
    CloseDo();
  else
    GGlobal.WssSocketClose(CloseDo);
}
window.blqInitGame = function(mode){
  Laya.timer.handlerClear("heartLoop");
  if(GGlobal.bannerAd) GGlobal.bannerAd.hide();
  var _ui = GameGlobal.gameUi;
  var p = GameGlobal.playerOrder;
  GameGlobal.UiMode = "game";
  adapterpx("game");
  GameGlobal.gameUi.btnBWin.visible = GameGlobal.gameUi.btnBLose.visible = false;
  GameGlobal.gameUi.btnTest.on("click",null,function(){
    Laya.timer.dumpHandlers();
  });
  
  if(GameGlobal.GameWhere=="PassLevel"){
    GameGlobal.LVS = getPLevelLvs(GameGlobal.PLV);var lvs = GameGlobal.LVS;
    GameGlobal.LVB = Math.floor(lvs[0]*0.8);
    var b = GameGlobal.LVB,vlen = lvs[2] - GameGlobal.LVB;
    var vxs = -15,vxe = 275,smw = 15;
    for(var k = 0;k<=3;k++){
      var sk = _ui.pGJTipCInfo.getChildByName("星"+k);
      if(k==0){
        sk.getChildByName("分数").text = GameGlobal.LVB;
      }else{
        var v = GameGlobal.LVS[k-1];var s = (v-b)/vlen;
        var x = vxs*(1-s)+vxe*s-smw;sk.x = x;
        sk.getChildByName("分数").text = GameGlobal.LVS[k-1];
      }
    }

    _ui.iGJBarTip.width = 6;
    _ui.pGJTip.visible = true; }else GameGlobal.gameUi.pGJTip.visible = false;

  refreshMSNum("game");
  function AskItemBack(){
    GameGlobal.gameUi.pMask.visible = GameGlobal.gameUi.pItemAsk.visible = false;GameGlobal.cshootball = true;
    GameGlobal.GameStop = false;
  }
  GameGlobal.gameUi.btnItemAskClose.setOnClick(function(){AskItemBack();})

  function UseItem(item,mode){
    var p = GameGlobal.playerOrder;
    var hasitem = true;
    if(mode=="useitem"){
      if(GGlobal.sgame[item]==0){
        eftFloat("道具数量不足",'#fbfbfb',300,48);
        AskItemBack();
        return;
      }else{
        if(GameGlobal.itemsUsed[p][item] == 3){
          eftFloat("已达次数上限",'#fbfbfb',300,48);
          return;
        }
      }
    }else{
      eftFloat("使用钻石免費使用道具",'#fbfbfb',300,48);
    }
    if(item=="hjq"){
        if(GameGlobal.canUseYellow==true){
          if(mode=="useitem"){
            GameGlobal.itemsUsed[p].hjq++;GGlobal.sgame.hjq--;
          } 
          GameGlobal.useYellowBall[p] = true;
          refreshMSNum("game");GGlobal.SaveItem(null,null,true);
          GameGlobal.bRoundTip = 3;
          GameGlobal.gameUi.btnYellow.visible = false;
          GameGlobal.canUseYellow = false;
        }
    }else if(item=="fhq"){
      if(mode=="useitem"){GameGlobal.itemsUsed[p].fhq++;GGlobal.sgame.fhq--;}
      GGlobal.SaveItem(null,null,true);
      GameGlobal.gameUi.btnFhq.visible = false;
      refreshMSNum("game");
      blqLevelReset(2); 
      restoreGame(GameGlobal.matchMeOrder);
      refreshItemScore();
    }
    AskItemBack();
    Laya.timer.handlerOnce("ItemOnce",300,function(){GameGlobal.cshootball = true;})
  }
  
  GameGlobal.gameUi.btnItemFree.setOnClick(function(){
    if(GGlobal.sgame.jlq>0){
      GGlobal.sgame.jlq--;
      UseItem(GameGlobal.AskItem,"usejlq");
    }else{
      eftFloat("钻石不足",'#fbfbfb',300,60);
    }
  })
  GameGlobal.gameUi.btnItemFreeNo.setOnClick(function(){
    AskItemBack();
    UseItem(GameGlobal.AskItem,"useitem");
  })
  function PAskItemShow(item){
    GameGlobal.AskItem = item;
    GameGlobal.cshootball = false;
    GameGlobal.gameUi.pMask.visible = true;
    GameGlobal.gameUi.pItemAsk.visible = true;
  }
  GameGlobal.gameUi.btnYellow.setOnClick(function(){
    PAskItemShow("hjq");
  },_s+"gameitem.mp3");

  GameGlobal.gameUi.btnFhq.setOnClick(function(){
    PAskItemShow("fhq");
  },_s+"gameitem.mp3");

  GameGlobal.gameUi.btnItemGet.setOnClick(bindItemGet,null,[1]);
  GameGlobal.gameUi.btnItemNoGet.setOnClick(function(){
  // GameGlobal.gameUi.pGetNoItem.visible = 
  GameGlobal.gameUi.pGetItem.visible = GameGlobal.gameUi.pMask.visible = false;
    if(GameGlobal.FnNextStep) GameGlobal.FnNextStep();
  })
  GameGlobal.FhqDisState[GameGlobal.playerOrder] = true;
  GameGlobal.gameUi.btnFhq.visible = false;
  GameGlobal.btnPanels = null;
  GameGlobal.gameMode = mode;
  GameGlobal.ResetVar();
  if(mode==0){
    GameGlobal.gameUi.pUserInfo2.visible = false;
    GameGlobal.turnMe = true;GameGlobal.BPO = 0;GameGlobal.IsMachine = false;
    GameGlobal.IsFriendBattle = false;
    GameGlobal.gameUi.pBiaoQin.visible = false;
    GameGlobal.gameUi.iQiPao1.visible = GameGlobal.gameUi.iQiPao2.visible = false;
    GameGlobal.matchMeOrder = 0;GameGlobal.playerOrder = 0;
    GameGlobal.PEnds[1] = true;
    refreshMSNum("game");
  }
  else if(mode==1) {
    // GameGlobal.gameUi.btnGameShop.visible = false;
    GameGlobal.gameUi.pBiaoQin.visible = true;
    GameGlobal.gameUi.btnBiaoQin.visible = true;
    GameGlobal.PlayerLeaveRoom = false;
    GameGlobal.gameUi.iQiPao1.visible = GameGlobal.gameUi.iQiPao2.visible = false;
    GameGlobal.gameUi.btnBiaoQin.getChildByName("表情").visible = true;
    GameGlobal.gameUi.btnBiaoQin.getChildByName("关闭").visible = false;
    GameGlobal.gameUi.pBiaoQinC.visible = false;
    GameGlobal.TotalLevel = GGlobal.GTotalLevel;
    GameGlobal.ShowBq = 1;
    GameGlobal.gameUi.btnBiaoQin.setOnClick(
      function(){
        if(GameGlobal.ShowBq==1){
          BqOn();
        }else {
          BqOff();
        }
    },_s+"gameitem.mp3");
    // console.log("Xgq set GameGlobal.gameMode:"+GameGlobal.gameMode);
  }
  blqRefreshAv();
  Laya.stage.gameLoop = gameLoop;
  {
    GameGlobal.updatePhy = true;
    GameGlobal.gameOver = false;

    
    blqRefreshAv();
    GameGlobal.Rounds = [];
    GameGlobal.Rounds2 = [];
    clearGameOnce();
    GameGlobal.gameUi.sprJd1.visible = GameGlobal.gameUi.sprJd2.visible = false;
    GameGlobal.ResetVar();
    // Laya.loader.load([
    //   {url: "prefab/局.prefab",type: Laya.Loader.PREFAB}],
    //   Laya.Handler.create(null,function(){
        var preRound = Laya.loader.getRes("prefab/局.prefab");
        var itemW = 125,itemH = 125,itemL = 20,itemT = 100;
        function addItem(k,p){
          var item;
          if(k!=GameGlobal.TotalLevel-1){
            item = preRound.create();
          }else{
            item = (p==0?GameGlobal.gameUi.iLastRound:GameGlobal.gameUi.iLastRound2);item.visible = true;
          }
          item.getChildByName("局数").text = ""+(k+1);
          item.getChildByName("第1球").text = item.getChildByName("第2球").text = item.getChildByName("分数").text = "";
          item.getChildByName("符第1球").visible = item.getChildByName("符第2球").visible = false;
          if(k==GameGlobal.TotalLevel-1){
            item.getChildByName("第3球").text = "";
          }
          item.state = "开始";
          var rounds = p==0?GameGlobal.gameUi.pRounds:GameGlobal.gameUi.pRounds2;
          rounds.addChild(item);
          item.x = Math.floor((k%5))*itemW+itemL;item.y = Math.floor((k/5))*itemH+itemT;
          return item;
        }
        GameGlobal.gameUi.pRounds.getChildByName("结果").visible = false;
        GameGlobal.gameUi.pRounds2.getChildByName("结果").visible = false;
        for(var k = 0;k<GameGlobal.TotalLevel;k++){
          GameGlobal.Rounds.push(addItem(k,0));
          GameGlobal.RoundsData[0].push({});
        }
        for(var k = 0;k<GameGlobal.TotalLevel;k++){
          GameGlobal.Rounds2.push(addItem(k,1));
          GameGlobal.RoundsData[1].push({});
        }
        GameGlobal.gameUi.pRounds.y = GGlobal.RoundSY;
        if(GameGlobal.gameMode==0){
          GameGlobal.gameUi.pRounds.height = 378;
        }else{
          GameGlobal.gameUi.pRounds.height = 240;
        }
    // }));
  }
  blqInitGame3D();
}
window.blqRefreshAv = function(){
  var mk = GameGlobal.matchMeOrder?4:3;
  for(var k = 1;k<=4;k++){
    GameGlobal.gameUi["imgAv"+k].mask = GameGlobal.imgMask;
    var vk;
    if(GameGlobal.gameOver==true){
      vk = mk;
    }else{
      if(k<=2){vk = k; }else{vk = mk; }
    }
    GameGlobal.gameUi["imgAv"+k].skin = GGlobal["imgAv"+vk];
    GameGlobal.gameUi["txtAv"+k].text = GGlobal["txtAv"+vk];
  }
  GameGlobal.turnMe = true;
  if(GameGlobal.turnMe==false){
    GameGlobal.gameUi.pItem.visible = false;
    GameGlobal.gameUi.pFhqInfo.visible = false;
  }else{
    GameGlobal.gameUi.pFhqInfo.visible = false;
    GameGlobal.gameUi.pItem.visible = true;
  }
}
window.blqJsAv = function(){
  for(var k = 3;k<=4;k++){
    var k2 = k-2;
    GameGlobal.gameUi["imgAv"+k].skin = GGlobal["imgAv"+k2];
    GameGlobal.gameUi["txtAv"+k].text = GGlobal["txtAv"+k2];
  }
}
window.blqShowJG = function(){
  if(GameGlobal.gameMode==0) return;
  var sv1 = GameGlobal.GameScore[0],sv2 = GameGlobal.GameScore[1];
  var bsjg = false;
  GameGlobal.gameUi.btnOverMoney.visible = false;
  function sJG(w1){
    var sJG1 = GameGlobal.gameUi.pRounds.getChildByName("结果"),sJG2 = GameGlobal.gameUi.pRounds2.getChildByName("结果");
    sJG1.visible = sJG2.visible = true;
    var eftJG = [];

    if(GameGlobal.matchMeOrder==w1){
      Laya.SoundManager.playSound(_s+"win.mp3");
    }else Laya.SoundManager.playSound(_s+"lose.mp3");

    if(w1==1){
      sJG1.getChildByName("赢").visible = false;sJG1.getChildByName("输").visible = true;
      sJG2.getChildByName("赢").visible = true;sJG2.getChildByName("输").visible = false;
      eftJG.push(sJG1.getChildByName("输"));eftJG.push(sJG2.getChildByName("赢"));
    }
    else{
      sJG2.getChildByName("赢").visible = false;sJG2.getChildByName("输").visible = true;
      sJG1.getChildByName("赢").visible = true;sJG1.getChildByName("输").visible = false;
      eftJG.push(sJG2.getChildByName("输"));eftJG.push(sJG1.getChildByName("赢"));
    }
    for(var k = 0;k<eftJG.length;k++){
      var eft = eftJG[k];eft.scaleX = eft.scaleY = 0;
      Laya.Tween.to(eft,{scaleX:1,scaleY:1},300,Laya.Ease.elasticOut,Laya.Handler.create(null,function(){
          if(GameGlobal.matchMeOrder==w1){
            GameGlobal.gameUi.btnBWin.visible = true;
            GameGlobal.gameUi.btnBLose.visible = false;
          }else{
            GameGlobal.gameUi.btnBWin.visible = false;
            GameGlobal.gameUi.btnBLose.visible = true;
          }
          if(GameGlobal.IsFriendBattle==true){
            GameGlobal.gameUi.btnBWin.visible = false;
            GameGlobal.gameUi.btnBLose.visible = true;
          }
        })
      );
    }
  }
  if(sv1<sv2){sJG(1); }else if(sv1>sv2){sJG(0); }
  else{
    GameGlobal.gameUi.btnBLose.visible = true;
  }
}
window.matchedDone = function(){
  var scene = GameGlobal.homeUi;
  GameGlobal.IsFriendBattle = false;
  scene.iRankingAv2.visible = false;
  matchedDoneOp();
  Laya.timer.handlerClear("GRankingLoop");
  Laya.timer.handlerOnce("matchedDone",500,function(){
    blqGoGame.bind(1)();
  })
}
window.matchedDoneOp = function(){
  var scene = GameGlobal.homeUi;
  scene.iRankingAvR.loadImage(GGlobal.avatarUrl2);
  scene.iRankingAvR.visible = true;
  GameGlobal.playerOrder = 0;
  GameGlobal.turnMe = (GameGlobal.matchMeOrder == GameGlobal.playerOrder);
  GameGlobal.bRoundTip = 0;//Tp 1
  scene.iRankingAvR.width = scene.iRankingAvR.height = 145;
}

window.blqInitHome = function(scene,mi){
  GameGlobal.homeUi = scene;
  refreshHomeLevel();
  if(GGlobal.bannerAd){
    GGlobal.bannerAd.hide();
  }
  Laya.timer.handlerClear("heartLoop");
  Laya.timer.handlerLoop("heartLoop",3000,function(){
    GGlobal.RequestPost(GGlobal.REMOTE_SERVER_URL+"heart",{openId:GGlobal.openId},
      function(info,jsData){
        if(jsData.data.state=="invlist"){
          GGlobal.invlist = jsData.data.invlist;
          refreshInvList();
        }
      },
      function(info,jsData){}
    )
  })
  function ClosePop(){
    scene.pMask1.visible = false;GGlobal.createBannerAd("home",true,GGlobal.BannerHy,Laya.Browser.clientWidth);
  }
  scene.iInvBar.skin = _r+"home/ibar.png";
  scene.iPWAvBar.skin = _r+"adv/bzhbar.jpg";
  scene.pChou.getChildByName("抽奖机").skin = _r+"Chou/bg.png";
  scene.pBattle.getChildByName("金杯").skin = _r+"home/jinbei.png";
  scene.pVS.getChildByName("底").skin = _r+"home/vs.png";
  scene.iPWVs.skin = _r+"home/vs.png";
  scene.btnPWAvBar.setOnClick(function(){
    GGlobal.NavigateTo("推广2");
  });
  for(var k = 1;k<=2;k++)
    scene.pCShop3.getChildByName("场景"+k).skin = _r+"home/cj0"+k+".png";

  GameGlobal.btnPanels = null;
  var mask = new Laya.Sprite(); mask.loadImage("game/圆形遮罩.png"); GameGlobal.imgMask = mask;
  Laya.stage.gameLoop = null;

  if(!mi) mi = 3;
  GameGlobal.BtnClick["btnGJ"] = function(){scene.pMask1.visible = true; scene.pLevel.visible = true;if(GGlobal.bannerAd) GGlobal.bannerAd.hide(); }
  scene.btnS.setOnClick(function(){scene.pMask1.visible = true; scene.pLS.visible = true; if(GGlobal.bannerAd) GGlobal.bannerAd.hide(); });
  scene.btnGJ.setOnClick(GameGlobal.BtnClick["btnGJ"]);
  scene.btnGJB.setOnClick(function(){
    scene.pMask1.visible = scene.pGJ.visible = true;
  })
  scene.btnGJBClose.setOnClick(function(){
    scene.pMask1.visible = scene.pGJ.visible = false;ClosePop();
  })
  scene.btnLevelClose.setOnClick(function(){
    scene.pMask1.visible = scene.pLevel.visible = false;ClosePop();
  })
  scene.pLevelInfo.visible = false;
  {
    var preRound = Laya.loader.getRes("prefab/冠军项.prefab");
    var itemW = 207,itemH = 225,itemL = 85,itemT = 115;
    var sl = 0;
    for(var r = 0;r<4;r++) for(var c = 0;c<3;c++) {
      var rc = r*3+c;
      var item = preRound.create();
      scene.pGJC.addChild(item);
      item.getChildByName("草").skin = _r+"glvs/hback.png";
      item.getChildByName("建筑").skin = _r+"glvs/h"+(rc+1)+".png";
      var si = getRankingLevel(GGlobal.sgame.glvs.lv,GameGlobal.glvsl)
      item.getChildByName("称呼").text = GameGlobal.glvsn[rc];
      
      var s = false;
      if(rc>=si[0]){
        if(rc>GGlobal.sgame.glvs.rlv){ s = true;}
      }
      item.getChildByName("锁").visible = item.getChildByName("锁提示").visible = s;
      if(s==true){
        item.getChildByName("建筑").gray = true;
        item.getChildByName("锁提示").text = "（"+sl+"关后解锁）";
      }
      sl+=GameGlobal.glvsl[rc];
      item.x = c*itemW+itemL;item.y = r*itemH+itemT;
    }
  }

  {
    function LevelGo(){
  // glvsd:[
  //       ["{0}局达到{1}","{0}分",
  //         [3,19,{m:100,d:0},21,{m:200,d:0},24,{m:400,d:1}],
  //         [3,32,{m:100,d:0},34,{m:200,d:0},35,{m:400,d:1}],
  //         [4,40,{m:100,d:0},45,{m:200,d:0},49,{m:400,d:1}],
  //         [4,48,{m:100,d:0},58,{m:200,d:0},62,{m:400,d:1}],
  //         [5,87,{m:100,d:0},96,{m:200,d:0},104,{m:400,d:5}]
  //       ],
  //       ["{0}球击中{1}瓶子","{0}瓶"
  //         [3,15,{m:100,d:0},18,{m:200,d:0},24,{m:400,d:1}],
  //         [3,20,{m:100,d:0},22,{m:200,d:0},26,{m:400,d:1}],
  //         [4,20,{m:100,d:0},28,{m:200,d:0},32,{m:400,d:1}],
  //         [4,26,{m:100,d:0},30,{m:200,d:0},34,{m:400,d:1}],
  //         [5,40,{m:100,d:0},44,{m:200,d:0},48,{m:400,d:5}]
  //       ]
  //     ],
      var lv = GGlobal.sgame.glvs.lv;
      if(this>lv){
        eftFloat("关卡未解锁","#0",600);
      }else{// [3,19,{m:100,d:0},21,{m:200,d:0},24,{m:400,d:1}],
        GameGlobal.LevelTipInfo = function(clv){
          var scene = GameGlobal.homeUi;
          scene.pMask2.visible = true;
          scene.pLevelInfo.visible = true;  
          var nr = Math.floor(clv/5);
          var sd = GameGlobal.glvsd[nr];var sdstr = sd[0];var sdo = sd[2+(clv%5)];
          GameGlobal.SDO = sdo;
          GameGlobal.PLV = clv;
          var dest = scene.pLevelInfo.getChildByName("通关描述");
          var tlv = scene.pLevelInfo.getChildByName("关卡");
          tlv.text = "第"+(clv+1)+"关";
          sdstr = sdstr.replace("{0}",sdo[0]);sdstr = sdstr.replace("{1}",sdo[1]);
          dest.text = sdstr;
          for(var k = 1;k<=3;k++){
            var desc = scene.pLevelInfo.getChildByName("星级说明"+k);
            var desk = desc.getChildByName("关卡说明");
            var tdes = sd[1];
            desk.text = tdes.replace("{0}",sdo[2*k-1]);
            var mnum = desc.getChildByName("金币").getChildByName("奖励");
            var dnum = desc.getChildByName("钻石").getChildByName("奖励");
            mnum.text = sdo[2*k].m;
            dnum.text = sdo[2*k].d;
          }
          scene.btnLevelInfoClose.setOnClick(function(){scene.pMask2.visible = scene.pLevelInfo.visible = false;})
          scene.btnLevelInfoGo.setOnClick(function(){
            processPLevelPre(nr);
            blqGoGame.bind(0)("PassLevel");
          })
        }
        GameGlobal.LevelTipInfo(this);
      }
    }
    var preRound = Laya.loader.getRes("prefab/关卡.prefab");
    var itemW = 207,itemH = 225,itemL = 100,itemT = 110;
    var sl = 0;
    var glvs = GGlobal.sgame.glvs;
    for(var r = 0;r<3;r++) for(var c = 0;c<3;c++) {
      var rc = r*3+c+glvs.lp*9;
      var rc1 = rc+1;
      var item = preRound.create();
      scene.pLevelC.addChild(item);
      item.getChildByName("关数").text = rc1;
      var star = glvs["lv"+rc];star = star?star:0;
      var xin = item.getChildByName("星星");
      for(var k = 1;k<=3;k++){
        var sicon = xin.getChildByName(""+k);
        sicon.getChildByName("有").visible = k<=star;
        sicon.getChildByName("无").visible = k>star;
      }
      var s = rc>glvs.lv;
      item.getChildByName("锁").visible = item.getChildByName("未解锁").visible = s;
      item.getChildByName("挑战").visible = !s;
      item.setOnClick(LevelGo.bind(rc));
      sl+=GameGlobal.glvsl[rc];
      item.x = c*itemW+itemL;item.y = r*itemH+itemT;
    }
  }

  scene.btnBF.setOnClick(function(){
    if(GameGlobal.BattleHomeState=='battle'){
      GameGlobal.BattleHomeState = 'waitcreateroom';
      netCreateRoom();
    }else if(GameGlobal.BattleHomeState=='toready'){
      var data = {
          command:"room",
          action:"ready",
          openId:GGlobal.openId
      };
      GGlobal.playersInfo[GGlobal.openId].isReady = true;
      refreshBattleInfo("update");
      GGlobal.WssSocketSend(JSON.stringify(data));
    }
    refreshBtnBF();
  })
  var mask = new Laya.Sprite();
  mask.loadImage("game/圆形遮罩.png");
  var pwmask = new Laya.Sprite();
  pwmask.loadImage("game/圆形遮罩.png");
  pwmask.width = pwmask.height = 90;
  GameGlobal.homeAvMask = pwmask;
  scene.imgAv.mask = mask;
  scene.imgAv.skin = GGlobal.avatarUrl;
  scene.txtAv.text = GGlobal.nickName;

  for(k = 1;k<=3;k++){GGlobal["imgAv"+k] = GGlobal.avatarUrl;GGlobal["txtAv"+k] = GGlobal.nickName;}
  function refreshShopItem(){
    var v = GGlobal.OwnBallon[GGlobal.pbi];
      for(var k = 1;k<=4;k++){scene["btnShop"+(k)].visible = false; }

      for(var k = 0;k<GameGlobal.pLiaos.length;k++){
        if(GGlobal.OwnBallon[k]==1){
          GameGlobal.pLiaos[k].getChildByName("蒙版").visible = false;
          GameGlobal.pLiaos[k].getChildByName("锁").visible = false;
        }
        GameGlobal.pLiaos[k].getChildByName("选中").visible = (k==GGlobal.pbi);
      }

      if(GGlobal.OwnBallon[GGlobal.pbi]==0&&GameGlobal.ShopTab==1){
        scene.btnShop1.visible = true;
      }
      if(GameGlobal.ShopTab==2){
        if(GGlobal.sgame.useq==GGlobal.qbi)  scene.btnShop3.visible = true; else scene.btnShop2.visible = true;
      }
      if(GameGlobal.ShopTab==3){
        if(GGlobal.sgame.usec==GGlobal.cbi)  scene.btnShop3.visible = true; else scene.btnShop4.visible = true;
      }
      for(var k = 0;k<GameGlobal.pQius.length;k++){
        GameGlobal.pQius[k].getChildByName("蒙版").visible = false;
        GameGlobal.pQius[k].getChildByName("锁").visible = false;
        GameGlobal.pQius[k].getChildByName("选中").visible = (k==GGlobal.qbi);
      }

      for(var k = 0;k<GameGlobal.pQiuDs.length;k++){
        GameGlobal.pQiuDs[k].getChildByName("选中").visible = (k==GGlobal.cbi);
      }
    }
    function refreshShopTab(){
      for(var k = 1;k<=3;k++){
        var di = scene["btnTabShop"+k];
        di.skin = "home/"+(k!=GameGlobal.ShopTab?"底层-未选中.png":"底层-选中.png");
        var text = di.getChildByName("Text")
        text.y = k==GameGlobal.ShopTab?21:18;
      }
      for(var k = 1;k<=3;k++){
        scene["pCShop"+k].visible = (k==GameGlobal.ShopTab);
      }
      refreshShopItem();
    }
    function bindShopBtn(){
        scene.pfShop.visible = false;
        function ConsumeJLQ(callBack){
          if(GGlobal.sgame.jlq>0){  GGlobal.sgame.jlq--;refreshMSNum();callBack();} else eftFloat("砖石不足","#0",600);
        }
        for(var k = 1;k<=4;k++){
          scene["btnShop"+k].setOnClick((function(){
            if(GameGlobal.ShopTab==1&&this==1){
              if(GGlobal.maxscore<500){
                eftFloat("金幣不足，无法购买","#0",600);
              }else{
                GGlobal.maxscore-=500;GGlobal.OwnBallon[GGlobal.pbi] = 1;
                refreshShopItem();
                refreshMSNum();GGlobal.SaveItem(null,null,true);
              }
            }
            else if(GameGlobal.ShopTab==2){
              if(this==2){
                ConsumeJLQ(function(){GGlobal.sgame.useq = GGlobal.qbi;refreshShopItem();GGlobal.SaveItem(null,null,true);})
              }
            }
            else if(GameGlobal.ShopTab==3){
              if(this==4){
                ConsumeJLQ(function(){GGlobal.sgame.usec = GGlobal.cbi;refreshShopItem();GGlobal.SaveItem(null,null,true);})
              }
            }
          }).bind(k));
        }
        for(var k = 1;k<=3;k++){
          scene["btnTabShop"+k].setOnClick((function(){
            GameGlobal.ShopTab = this;
            refreshShopTab();
          }).bind(k));
        }
        GameGlobal.ShopTab = 1;refreshShopTab();
    }
    function refreshPWRanking(){
      GameGlobal.homeUi.pPWMe.visible = false;
      GGlobal.RequestPost(GGlobal.REMOTE_SERVER_URL+'getRankPW',{openId:GGlobal.openId},
        function(info,jsData){
          var code = jsData['code'];
          if(code == "0"){
            var pPWMe = GameGlobal.homeUi.pPWMe;
            var iImgAv = GameGlobal.homeUi.iPWIcon;
            pPWMe.visible = true; 
            iImgAv.mask = GameGlobal.homeAvMask;iImgAv.skin = GGlobal.avatarUrl;
            iImgAv.width = iImgAv.height = 80;
            var mrank = jsData['data']['mrank'];
            var r = mrank.rank,rs = mrank.score;
            var ir = pPWMe.getChildByName("排名项").getChildByName("名次");
            var tr = pPWMe.getChildByName("排名项").getChildByName("排名");
            var pn = pPWMe.getChildByName("玩家名字");
            var n16 = GGlobal.ClipStringLength(GGlobal.nickName,16);
            pn.text = n16;
              
            var star = Math.floor(rs%100000000);
            var rls = getRankingLevel(GGlobal.maxscoreI,GameGlobal.RankingLS);
            var rd = rls[0];var el = GameGlobal.RankingLS.length-1;
            if(rd>=el){ rd = el;}

            GameGlobal.homeUi.iRStarMe.text = GameGlobal.RankingTx[rd];
            GameGlobal.homeUi.iRStarMeIcon.skin = _r+("PW/"+(rd+1)+".png");
            var r1 = r+1;
            if(r1<=3){
              ir.visible = true;tr.visible = false;
              for(var k = 1;k<=3;k++){ ir.getChildByName(""+k).visible = k==r1;}
            }else{
              ir.visible = false;tr.visible = true;
              tr.text = ""+r1;
            }
          }
        },
        function(info,jsData){})
      var rls = getRankingLevel(GGlobal.maxscoreI,GameGlobal.RankingLS);
      var rl = rls[0],ress = rls[1] - GGlobal.maxscoreI;
      var el = GameGlobal.RankingLS.length-1;
      var cls = [];
      if(rl>=el){
        cls.push(el);
        scene.iRanking2.visible = false;
      }else{
        cls.push(rl);cls.push(rl+1);
        scene.iRanking2.visible = true;
      }
      for(var k = 0;k<cls.length;k++){
        var item = scene["iRanking"+(k+1)],clv = cls[k];
        if(k==0){
          item.setOnClick(function(){
            var kv = getRankingLevel(GGlobal.maxscoreI,GameGlobal.RankingLS);
            var pay = GameGlobal.RankingSM[kv[0]];
            if(!wx.login||GGlobal.islocal==true||GGlobal.istest==true) pay = 1;
            if(GGlobal.maxscore<pay){
              eftFloat("金币不足,请到训练场获取",'#666666',600,30);
              return;
            }else{
              eftFloat("花费"+pay+"金币参加排位赛",'#666666',800,36);
              GGlobal.maxscore-=pay;refreshMSNum();GGlobal.SaveItem(null,null,true);
            }
            scene.pRankingPair.visible = true;
            netMatchingPlayer();
            scene.iRankingAvL.loadImage(GGlobal.avatarUrl);scene.iRankingAvL.width = scene.iRankingAvL.height = 145;
            scene.iRankingAvR.visible = false;
            var stime = 0;
            Laya.timer.handlerLoop("GRankingLoop",16,function(){
              var nst = 10;
              stime++;var rs = stime%nst; var s = 1.0+(rs<nst/2?rs:nst-rs)*0.2/nst;
              scene.iRankingAv2.scaleX = scene.iRankingAv2.scaleY = s;
              if(GameGlobal.matched==1){
                GameGlobal.IsMachine = false;
                matchedDone();
              }
            });
          })
        }
        item.getChildByName("信息").text = GameGlobal.RankingTx[clv]; item.getChildByName("奖金").text = "奖金："+GameGlobal.RankingRM[clv]; item.getChildByName("参赛费用").text = GameGlobal.RankingSM[clv];
        if(k!=0){item.getChildByName("蒙版").visible = true; item.getChildByName("锁").visible = true; }else{item.getChildByName("蒙版").visible = false; item.getChildByName("锁").visible = false; }
        var rslv = GameGlobal.RankingLS[clv];
        item.getChildByName("图标").skin = (_r+"PW/"+(clv+1)+".png");
        var istars = item.getChildByName("星星");
        var ex = 197,sw = 25;
        for(var sk = 0;sk<7;sk++){
          var nstar = istars.getChildByName(""+(sk+1));
          nstar.x = ex - sw * sk;
          if(k==0){
            var v8 = istars.getChildByName("8");
            if(rl==el+1){
              v8.visible = true;
              v8.getChildByName("数量").text = GGlobal.maxscoreI - 48;
              for(var jk = 1;jk<=7;jk++) istars.getChildByName(""+jk).visible = false;
            }else{
              v8.visible = false;
              if(sk<rslv){
                if(sk<ress){nstar.visible = true; nstar.loadImage("home/星星-空.png"); }
                else nstar.loadImage("home/星星.png");
              }
              else nstar.visible = false;
            }
          }else{
            if(sk<rslv){
              nstar.visible = true;
              nstar.loadImage("home/星星-空.png");
            }else nstar.visible = false;
          }
        }
      }
    }
    function refreshRankItem(){
      GameGlobal.homeUi.oRank.postMsg(GameGlobal.GRankData);
    }
    function refreshRankTab(){
      var city = -1;
      if(GameGlobal.RankTab>=2){
        city = GameGlobal.RankTab==2?GGlobal.city:-1;
        GGlobal.GetWorldRank(function(data){
          var rdata = data.data;
          var rankdata = [];var rankme;
          for(var k = 0;k<rdata.length;k++){
            var item = rdata[k];
            if(k==0){
              rankme = {rank:item.rank,KVDataList:[{key:"main",value:item.score}],avatarUrl:GGlobal.avatarUrl,nickname:GGlobal.nickName};
            }else{
              var avatarUrl;
              if(item==null||item.avatarUrl==null) continue;
              if(item.avatarUrl.indexOf("http:")!=-1)  
                avatarUrl = "https:"+item.avatarUrl.substring(5); 
              else 
                avatarUrl = item.avatarUrl;
              rankdata.push({KVDataList:
                [{key:"main",value:item.score}],
                avatarUrl:avatarUrl,nickname:item.nickName});
            }
          }
          GameGlobal.GRankData = {type: GameGlobal.RankTab==2?"rankm":"rank",page:0,rankme:rankme,rankdata:rankdata};
          GameGlobal.homeUi.oRank.postMsg(GameGlobal.GRankData);
        },city);
      }else{
        GameGlobal.homeUi.oRank.postMsg({type:"rankf",avatarUrl:GGlobal.avatarUrl});
      }
      for(var k = 1;k<=3;k++){
        var c = scene["btnTabRank"+k].getChildByName("内容");
        var di = c.getChildByName("底");
        di.loadImage(k==GameGlobal.RankTab?"home/底层-选中.png":"home/底层-未选中.png");
        di.y = k==GameGlobal.RankTab?-5:0;
      }
      refreshRankItem();
    }
    function bindRankBtn(){
        GameGlobal.GRankData = [];
        for(var k = 1;k<=3;k++){
          scene["btnTabRank"+k].setOnClick((function(){
            GameGlobal.RankTab = this;
            refreshRankTab();
          }).bind(k));
        }
        GameGlobal.RankTab = 1;refreshRankTab();
        scene.btnRankUp.setOnClick(function(){
          GameGlobal.homeUi.oRank.postMsg({type:"pageUp"});
          // refreshRankItem();
        })
        scene.btnRankDown.setOnClick(function(){
          GameGlobal.homeUi.oRank.postMsg({type:"pageDown"});
          // refreshRankItem();
        })
    }
  function showPanel(v){
    if(GGlobal.bannerAd) GGlobal.bannerAd.hide();
    if(v=="Sign"){
      adapterpx("Sign");
      scene.pSign.visible = true;
      scene.pMask1.visible = true;
      GGlobal.createBannerAd("sign",true,Laya.Browser.clientHeight-GGlobal.BannerHh,Laya.Browser.clientWidth);
    }else if(v=="Main"){
      if(GameGlobal.btnLogicSel==3)
        GGlobal.createBannerAd("home",true,GGlobal.BannerHy,Laya.Browser.clientWidth);
      scene.pSign.visible = scene.pInvite.visible = scene.pChou.visible = false;
      scene.pSign.getChildByName("签到天数").visible = scene.pChou.getChildByName("抽奖项").visible = false;
      scene.pMask1.visible = scene.pMask2.visible = scene.pGetItem.visible = scene.pSign.visible = 
      scene.pChou.visible = false;
      scene.pGJ.visible = false;
      scene.pLevel.visible = false;
      refreshRed();
    }else if(v=="Chou"){
      scene.pChou.visible = true;
      scene.pMask1.visible = true;
      if(GGlobal.sgame.chou==0){  
        scene.btnChouM.visible = false;scene.btnChouFree.visible = true;}
      else{
        scene.btnChouM.visible = true;scene.btnChouFree.visible = false;}
    }else if(v=="Invite"){
      // adapterpx("Invite");
      // GGlobal.createBannerAd("invite",true,Laya.Browser.clientHeight-GGlobal.BannerHh,Laya.Browser.clientWidth);
      scene.pInvite.visible = true;
      scene.pMask1.visible = true;
    }
  }
  var csx = 64,csy = 300,csw = 150,csh = 170,cey = csy+3*csh,cex = csx + 3*csw;
  function getChouItem(v){
    var kline = Math.floor(v/3),k3 = Math.floor(v%3),x,y;
    var item = {};
    if(kline==0){
      item.x = k3*csw+csx;item.y = csy; }else if(kline==1){item.x = 3*csw+csx;item.y = csy+k3*csh; }else if(kline==2){item.x = cex-k3*csw;item.y = cey; }else if(kline==3){item.x = csx;item.y = cey - k3*csh; }
    return item;
  }
  var lsx = 122,lsy = 450,lsw = 160,lsh = 161;
  function getLiaoItem(v){
    var kline = Math.floor(v/4),k4 = Math.floor(v%4),x,y;
    var item = {};
    item.x = lsx + lsw * k4;
    item.y = lsy + lsh * kline;
    return item;
  }
  refreshMSNum();
  scene.pMask1.on("click",null,function(){})
  scene.pMask2.on("click",null,function(){})
  scene.btnSign.setOnClick(function(){showPanel("Sign"); } )
  scene.btnChou.setOnClick(function(){showPanel("Chou"); } )
  scene.btnInvite.setOnClick(function(){showPanel("Invite"); } )
  var advBar = scene.pAdvC.getChildByName("推广条").getChildByName("推广项");
  var appid = "wx120efbb1cc482428";
  refreshAdvBar(advBar,appid);
  function JLClick(){
    Laya.timer.handlerClear("btnJLR");
    scene.pMask1.visible = true;
    scene.pJLQ.visible = true;
    if(GGlobal.gameConfig[0]==0){
      scene.pAdvC.visible = false;
    }else{
      refreshAdvBar(advBar,appid);
    }
    if(GGlobal.gameConfig[0]==0) scene.btnJLClose.visible = true;
    else{
        scene.btnJLClose.visible = false;
        Laya.timer.handlerOnce("JLClose",1000,function(){
        scene.btnJLClose.visible = true;
      })
    }
    if(GGlobal.bannerAd) GGlobal.bannerAd.hide();
  }
  scene.btnJL.setOnClick(JLClick);
  scene.btnJLFree.setOnClick(function(){
    function callBack(){
      scene.pMask1.visible = false; scene.pJLQ.visible = false;
      GGlobal.sgame.jlq++;refreshMSNum();GGlobal.SaveItem(null,null,true);
    }
    if(!wx.login) callBack();
    else  {
      if(GGlobal.gameConfig[0]==1) GGlobal.Share("jlq",callBack);
      else GGlobal.showAdvertisement("jlq",callBack);
    }
  })
  scene.btnJLClose.setOnClick(function(){
    scene.pMask1.visible = false; scene.pJLQ.visible = false; 
    eftBtnScale(scene.btnJL,"btnJLScale");
    GGlobal.createBannerAd("home",true,GGlobal.BannerHy,Laya.Browser.clientWidth);
  })

  scene.pJLQ.visible = false;
  scene.btnInv.setOnClick(function(){
    GGlobal.Share("Inv");
    GGlobal.GetLayaInvite();
  })

    function chouClick(mode){
      var _it = 0,ctime = 3300;
      
      GameGlobal.chouMode = mode;
      Laya.getset(0,scene.iChou,"it",function(){
        return _it;
      },function(v){
        var nv = Math.floor(v);
        var rv = Math.floor(v%12);
        var p = getChouItem(rv);
        this.x = p.x-4;this.y = p.y-4;
      })
      var kc = 48;
      var ittar;
      var rnd = Math.random();
      var gcs = GGlobal.gameConfig;
      if(rnd<gcs[20]){ittar = 49; }else if(rnd<gcs[21]){ittar = 55; }//鑽石
      else if(rnd<gcs[22]){ittar = 50; }else if(rnd<gcs[23]){ittar = 54; }//反悔券
      else if(rnd<gcs[24]){ittar = 52; }else if(rnd<gcs[25]){ittar = 58; }//黄金球
      else if(rnd<gcs[26]){ittar = 59; }else if(rnd<gcs[27]){ittar = 53; }//金币
      else if(rnd<gcs[28]){ittar = 51; }else if(rnd<=gcs[29]){ittar = 57; } 

      // ittar = 49;
      
      var InOut=function(t,b,c,d){var s = Math.pow(t/d,0.8); return-c *0.5 * ( Math.cos(Math.PI * s)-1)+b; }

      function chouCallback(){
        
        if(GameGlobal.chouMode=="free"){
          scene.btnChouFree.visible = false;
          scene.btnChouM.visible = true;
        }else{
          if(GGlobal.sgame.jlq>=1){
            GGlobal.sgame.jlq-=1;
          }else{
            eftFloat("钻石不足","#000000",600);
            return;
          }
        }
        scene.btnChouM.disabled = true;
        scene.btnChouClose.visible = false;
        Laya.timer.handlerOnce("playChou",200,function(){
          Laya.SoundManager.playSound(_s+"chou2.mp3");
        });
        Laya.Tween.to(scene.iChou, {it:ittar}, ctime, InOut, Laya.Handler.create(null,function(){
          Laya.timer.handlerOnce("chouStop",500,function(){
            scene.btnChouClose.visible = true;
            var r = Math.floor(ittar%12);
            infoGetItem(scene,"chou",r);
          });
        }), 0);

        GGlobal.RequestPost(GGlobal.REMOTE_SERVER_URL+'choufree',
          {openId:GGlobal.openId},
          function(info,jsData){
          },
          function(info,jsData){ 
          }
        )
      }
      if(GGlobal.gameConfig[0]!=0&&GameGlobal.chouMode=="free"){
        GGlobal.showAdvertisement("chou",chouCallback);
        //GGlobal.Share("chouFree",chouCallback);
      }else{
        chouCallback();
      }
    }
    scene.btnChouFree.setOnClick(chouClick,null,["free"])
    scene.btnChouM.setOnClick(chouClick,null,["m"])
  function refreshRed(){
    var flags = [];
    flags.push(!GGlobal.isSignIn);flags.push(!GGlobal.sgame.chou);
    var v3;
    if(GGlobal.invlist&&GGlobal.invlist.length==12){
      v3 = false;
      for(var k = 0;k<4;k++){
        if(GGlobal.invlist[k*3+2] == 0){
          v3 = true;
        }
      }
    }else{
      v3 = true;
    }
    flags.push(v3);
    for(var k = 0;k<3;k++){
      Laya.timer.handlerClear("RedTime"+k);
      var spr =  k == 0?GameGlobal.homeUi.btnSign:k==1?GameGlobal.homeUi.btnChou:GameGlobal.homeUi.btnInvite;
      spr = spr.getChildByName("红点");
      if(flags[k]){
        spr.visible = true;
        var td = {spr:spr,stime:0};
        Laya.timer.handlerLoop("RedTime"+k,200,(function(){
          this.stime+=200;
          this.spr.visible = Math.floor(this.stime/200)%2;
        }).bind(td));
      }else spr.visible = false;
    }
  }
  showPanel("Main");
  scene.pLS.visible = false;
  scene.btnSignClose.setOnClick(function(){showPanel("Main"); })
  scene.btnChouClose.setOnClick(function(){showPanel("Main"); })
  scene.btnInviteClose.setOnClick(function(){showPanel("Main"); })
  scene.btnItemGet.setOnClick(bindItemGet,null,[1])
  scene.btnItemGetD.setOnClick(bindItemGet,null,[2])
  scene.btnSGameH.setOnClick(function(){GameGlobal.TotalLevel = GGlobal.GTotalLevel/2;blqGoGame.bind(0)();})
  scene.btnSGameF.setOnClick(function(){GameGlobal.TotalLevel = GGlobal.GTotalLevel;blqGoGame.bind(0)();})
  scene.btnRoundClose.setOnClick(function(){scene.pLS.visible = false;ClosePop();})

  //GameConfigToDo
  if(GGlobal.gameConfig[0]==0){
    scene.btnItemGetD.visible = false;
    scene.btnItemGet.x = 360;
  }

  function OnNoItem(){
    scene.pGetNoItem.visible = scene.pMask1.visible = scene.pMask2.visible = false;
    scene.pMask1.visible = true;
    if(GameGlobal.chouMode=="free"){
      GGlobal.sgame.chou = 1;
    }
  }
  // scene.btnNoItem.setOnClick(OnNoItem);
  GameGlobal.UiMode = "home";
  GameGlobal.homeUi = scene;

  // GameGlobal.homeUi.pMask2.on("click",null,function(){
  //   if(GameGlobal.ChouNoGetItem==true){
  //     GameGlobal.ChouNoGetItem = false;
  //     OnNoItem();
  //   }
  // })

  {
    var preRound = Laya.loader.getRes("prefab/签到天数.prefab");
    var itemW = 207,itemH = 233,itemL = 155,itemT = 400;
    GameGlobal.Days = [];
    GameGlobal.homeUi.pDays.removeChildren();
    for(var k = 0;k<7;k++){
      var item;
      if(k<6) {
        item = preRound.create();
        item.getChildByName("底板").skin = _r+"home/day1.png";
      }
      else{
        item = GameGlobal.homeUi.btnDay7;
        item.getChildByName("背景").skin = _r+"home/day7.png";
      } 
      
      item.setOnClick((function(){
        if(GGlobal.isSignIn==0){
          function SignCallBack(){
            GGlobal.RequestPost(GGlobal.REMOTE_SERVER_URL+'sign',
              {openId:GGlobal.openId},
              function(info,jsData){
                if(jsData.data.signCount)
                  GGlobal.signCount = parseInt(jsData.data.signCount);
                else
                  GGlobal.signCount = 0;
                infoGetItem(scene,"sign");
              },
              function(info,jsData){ 
              }
            )
          }
          if(GGlobal.gameConfig[0]==2){
            GGlobal.showAdvertisement("sign",SignCallBack);
            // GGlobal.Share("SignDay",SignCallBack);
          }else{
            SignCallBack();
          }
        }
        else{
          if(this<GGlobal.signCount) eftFloat("已签到","#000000",600);
          else eftFloat("签到时间还未到","#000000",600);
        }
      }).bind(k));
      var mc = item.getChildByName("图标"), mv = item.getChildByName("数量");
      var sitem = GameGlobal.SItems[k];
      for(var sk in sitem){
        if(sk=="m"){mc.loadImage("game/金币图标.png"); }else if(sk=="hjq"){mc.loadImage("game/黄金球.png"); }else if(sk=="fhq")
        {mc.loadImage("game/反悔券.png"); } else if(sk=="jlq"){mc.loadImage("home/钻石.png")}
        mv.text = sitem[sk];
      }
      mc.width = mc.height = 80;
      item.getChildByName("天数").text =  "第"+(k+1)+"天";
      GameGlobal.homeUi.pDays.addChild(item);
      GameGlobal.Days.push(item);
      if(k<6) {
        item.x = Math.floor((k%3))*itemW+itemL;item.y = Math.floor((k/3))*itemH+itemT;
      }
    }
    refreshSign();
  }
  {
    var preRound = Laya.loader.getRes("prefab/抽奖项.prefab");
    GameGlobal.pChous = [];
    GameGlobal.homeUi.pChous.removeChildren();
    
    for(var k = 0;k<12;k++){
      var item = preRound.create();
      var md = item.getChildByName("底").skin = _r+"home/cjx.png";
      var mc = item.getChildByName("图标"), mv = item.getChildByName("数量");
      var sitem = GameGlobal.CItems[k];
      for(var sk in sitem){
        if(sk=="m"){mc.skin = "game/金币图标.png"; }else if(sk=="hjq"){mc.skin = "game/黄金球.png"; }else if(sk=="fhq"){mc.skin = "game/反悔券.png";} 
        else if(sk=="hb"){mc.skin = "home/红包.png";}else  if(sk=="jlq"){mc.skin = "home/钻石.png"}
        mv.text = sitem[sk];
      }
      mc.width = mc.height = 80;
      GameGlobal.homeUi.pChous.addChild(item);
      var p = getChouItem(k);
      item.x = p.x;item.y = p.y;
    }
  }

  {
    var preRound = Laya.loader.getRes("prefab/聊天表情.prefab");
    GameGlobal.pLiaos = []; GameGlobal.pQius = []; GameGlobal.pQiuDs = [];
    GameGlobal.homeUi.pCShop1.removeChildren();
    for(var k = 0;k<15;k++){
      var item = preRound.create();
      var mc = item.getChildByName("表情"), mv = item.getChildByName("底板");
      var vk = ((k%5)+1);
      mv.skin = _r+"home/bqt"+vk+".png";
      mc.skin = _r+"home/bq"+(k+1)+".png";
      mc.width = mc.height = 120;
      GameGlobal.homeUi.pCShop1.addChild(item);GameGlobal.pLiaos.push(item);
      var p = getLiaoItem(k);
      item.x = p.x;item.y = p.y;
      item.setOnClick((function(){
        GGlobal.pbi = this;
        refreshShopItem();
      }).bind(k));
    }
    for(var k = 0;k<15;k++){
      var item = preRound.create();
      var mc = item.getChildByName("表情"), mv = item.getChildByName("底板");
      var vk = ((k%5)+1);
      mv.skin = _r+"home/bqt"+vk+".png";
      mc.skin = _r+"home/qxg"+(k+1)+".png";mc.width = mc.height = 120;
      GameGlobal.homeUi.pCShop2.addChild(item);GameGlobal.pQius.push(item);
      var p = getLiaoItem(k);
      item.x = p.x;item.y = p.y;
      item.setOnClick((function(){
        GGlobal.qbi = this;
        refreshShopItem();
      }).bind(k));
    }
    for(var k = 0;k<2;k++){
      var item = scene.pCShop3.getChildByName("场景"+(k+1));
      GameGlobal.pQiuDs.push(item);
      item.setOnClick((function(){
        GGlobal.cbi = this;
        refreshShopItem();
      }).bind(k));
    }

    GGlobal.pbi = 0;//GGlobal.qbi = GGlobal.cbi = 0;
    refreshShopItem();
    bindShopBtn();
    bindRankBtn();
  }
  {
    var preRound = Laya.loader.getRes("prefab/邀选项.prefab");
    GameGlobal.iInvUs = [];
    GameGlobal.homeUi.pInviteUp.removeChildren();
    var sx = 45,sy = 12,sw = 128;
    for(var k = 0;k<4;k++){
      var item;
      if(k==3){item = scene.iInviteItem4;}else{
        item = preRound.create();GameGlobal.homeUi.pInviteUp.addChild(item);
        var icon = item.getChildByName("图标");
        icon.loadImage("game/黄金球.png");
        icon.width = icon.height = 62;
        icon.y-=2;
      }
      GameGlobal.iInvUs.push(item);
      if(k!=3) { item.x = sx + sw * k; item.y = sy;}
    }
  }

  {
    var preRound = Laya.loader.getRes("prefab/受邀玩家.prefab");
    GameGlobal.iInvDs = [];
    GameGlobal.homeUi.pInviteDown.removeChildren();
    var sx = 70,sy = 750,sw = 150;
    for(var k = 0;k<4;k++){
      var item;
      item = preRound.create();GameGlobal.homeUi.pInviteDown.addChild(item);
      GameGlobal.iInvDs.push(item);
      { item.x = sx + sw * k; item.y = sy;}
      item.on("click",null,(function(){
        GGlobal.invlist[this*3+2] = 1;
        if(this==3){
          GGlobal.maxscore+=200;GGlobal.sgame.hjq++;
          refreshMSNum();GGlobal.SaveItem(null,null,true);
        }else{
          GGlobal.maxscore+=200;GGlobal.sgame.hjq++;
          refreshMSNum();GGlobal.SaveItem(null,null,true);
        }
        refreshInvList();
        GGlobal.RequestPost(GGlobal.REMOTE_SERVER_URL+'invget',
            {openId:GGlobal.openId,v:this},
            function(info,jsData){
              },
            function(info,jsData){
              if(cb) cb();
        });
      }).bind(k));
    }
    refreshInvList();
  }
  // scene.btnAPw.setOnClick(function(){
  //   GameGlobal.BtnPanelClick[""+1]();
  // });
  var hitTime = 200;
  var bks = [0,0,127,256,387,519];
  function showBtnPanel(){
    var pw = 720;
    if(!GameGlobal.btnPanels){
      GameGlobal.btnPanels = []; 
      GameGlobal.btnPanels.push(scene.pRank); 
      GameGlobal.btnPanels.push(scene.pShop); 
      GameGlobal.btnPanels.push(scene.pGame); 
      GameGlobal.btnPanels.push(scene.pRanking); 
      GameGlobal.btnPanels.push(scene.pBattle);
    }
    // console.log("Xgq Battle---showBtnPanel:"+GameGlobal.btnLogicSel);
    for(var k = 0;k<GameGlobal.btnPanels.length;k++){
       GameGlobal.btnPanels[k].x = pw*k;
       GameGlobal.btnPanels[k].visible = true;
    }
    var tx = -(GameGlobal.btnLogicSel-1)*pw;
    scene.pMain.visible = true;

    if(GameGlobal.btnLogicSel!=3){
      if(GGlobal.bannerAd) GGlobal.bannerAd.hide();
    }else{
      GGlobal.createBannerAd("home",true,GGlobal.BannerHy,Laya.Browser.clientWidth);
    }
    
    if(GameGlobal.btnLogicSelPre!=GameGlobal.btnLogicSel){
      Laya.Tween.to(scene.pMain, {x:tx}, hitTime, Laya.Ease.linearNone, null, 0);
    }else{
      scene.pMain.x = tx;
      scene.btnBgSel.x = bks[GameGlobal.btnLogicSel];
      var pre = 3;
      if(GameGlobal.btnLogicSel<pre){
          for(var ck = GameGlobal.btnLogicSel;ck<pre;ck++){
            var bmr = scene["btnHome"+ck];
            bmr.loadImage("home/主按钮底-右.png");
            var c = bmr.getChildAt(0);c.x+=15;bmr.x+=56;
          }
      }else{
          for(var ck = pre;ck<GameGlobal.btnLogicSel;ck++){
          var bmr = scene["btnHome"+ck];
          bmr.loadImage("home/主按钮底-左.png");
          var c = bmr.getChildAt(0);c.x-=15;bmr.x-=56;
        }
      }
    }
    if(GameGlobal.btnLogicSel==1){
      GameGlobal.RankTab = 1;
      refreshRankTab();
    }
    else if(GameGlobal.btnLogicSel==4){
      refreshPWRanking();
    }else if(GameGlobal.btnLogicSel==5){
      refreshBattleInfo("init");
    }
    if(GameGlobal.btnLogicSel!=5){
      GGlobal.WssSocketClose(function(){});
      GGlobal.joinBattle = false;
    }
    if(GameGlobal.btnLogicSel!=3){
      
    }
  }
  GameGlobal.btnLogicSelPre = GameGlobal.btnLogicSel = mi;
  adapterpx("home");
  showBtnPanel();
  for(var ck = 1;ck<6;ck++){
    if(GameGlobal.btnLogicSel!=ck)  scene["btnHomeS"+ck].alpha = 0;
    else scene["btnHome"+ck].alpha = 0;
  }
  GameGlobal.BtnPanelClick = {};
  for(var k = 1;k<6;k++){
    GameGlobal.BtnPanelClick[""+k] = (function(){
        Laya.SoundManager.playSound(_s+"bd.mp3");
        if(this==GameGlobal.btnLogicSel) return;

        GameGlobal.btnLogicSelPre = GameGlobal.btnLogicSel;
        var ue = scene.btnBgSel;
        var u1 = scene["btnHome"+this];
        var u2 = scene["btnHome2"];
        var bsp = scene["btnHomeS"+GameGlobal.btnLogicSel];
        Laya.Tween.to(bsp, {alpha:0}, hitTime, Laya.Ease.linearNone, null, 0);
        var bsup = scene["btnHome"+GameGlobal.btnLogicSel];
        Laya.Tween.to(bsup, {alpha:1,scaleX:1,scaleY:1}, hitTime, Laya.Ease.linearNone, null, 0);
        var bsc = scene["btnHomeS"+this];
        Laya.Tween.to(bsc, {alpha:1}, hitTime, Laya.Ease.linearNone, null, 0);
        Laya.Tween.to(ue, { x: bks[this]}, hitTime, Laya.Ease.elasticOut,null, 0);
        var bsuc = scene["btnHome"+this];
        Laya.Tween.to(bsuc, {alpha:0}, hitTime, Laya.Ease.linearNone, null, 0);
        Laya.Tween.to(bsuc, {scaleX:1.2,scaleY:1.2}, hitTime, Laya.Ease.linearNone, null, 0);
        if(this<GameGlobal.btnLogicSel){
          for(var ck = this;ck<GameGlobal.btnLogicSel;ck++){
            var bmr = scene["btnHome"+ck];
            bmr.loadImage("home/主按钮底-右.png");
            var c = bmr.getChildAt(0);c.x+=15;
            Laya.Tween.to(bmr, {x:bmr.x+56}, hitTime, Laya.Ease.linearNone, null, 0);
          }
        }else{
            for(var ck = GameGlobal.btnLogicSel;ck<this;ck++){
            var bmr = scene["btnHome"+ck];
            bmr.loadImage("home/主按钮底-左.png");
            var c = bmr.getChildAt(0);c.x-=15;
            Laya.Tween.to(bmr, {x:bmr.x-56}, hitTime, Laya.Ease.linearNone, null, 0);
          }
        }
        GameGlobal.btnLogicSel = this;
        showBtnPanel();
    }).bind(k);
    scene["btnHome"+k].on("click",null,GameGlobal.BtnPanelClick[""+k])
  }
  eftBtnScale(scene.btnJL,"btnJLScale");
  // JLClick();
  // GGlobal.createBannerAd("home",true,GGlobal.BannerHy,Laya.Browser.clientWidth);
}
window.gameLoop = function(){
  if(blqGameUiLoop){
    blqGameUiLoop();
  }
  updatePhysics(0);
} 