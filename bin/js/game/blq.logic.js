
window.SetRoundsDataLv = function(w,lv,idx,v){
  GameGlobal["RoundsData"][w][lv]["blp"+idx] = v;
}
window.GetRoundsDataLv = function(w,lv,idx){
  return GameGlobal["RoundsData"][w][lv]["blp"+idx];
}
window.refreshItemScore = function(){
  var Rounds;
  if(GameGlobal.gameOver==true){
    Rounds = GameGlobal.playerOrder == 0?GameGlobal.Rounds:GameGlobal.Rounds2;
    GameGlobal.gameUi.pRounds2.visible = GameGlobal.gameUi.pRounds.visible = true;
    if(GameGlobal.gameMode==0)
      GameGlobal.gameUi.pRounds2.visible = false;
  }
  else{
    GameGlobal.gameUi.pRounds2.visible = false;
    Rounds = GameGlobal.Rounds;
  }
  function GetTotalScore(p,l){var s = 0; var lvs = GameGlobal.GameLevelScore[p]; if(lvs[l]==-1) return lvs[l]; else{for(var k = 0;k<=l;k++){s+=lvs[k]; } return s; } }
  //tp
  var rds,ps = {};
  for(var p = 0;p<=GameGlobal.gameMode;p++){
    GameGlobal.GameScore[p] = 0;
    var lvs = GameGlobal.GameLevelScore[p];
    var rhits = [],bhits = [];
    var lahit = false;
    for(var lv = 0;lv<Rounds.length;lv++){
      var nv0 = GetRoundsDataLv(p,lv,0);nv1 = GetRoundsDataLv(p,lv,1);
      nv0 = nv0==-1?10:nv0; nv1 = nv1==-2?(10-nv0):nv1;
      lvs[lv] = -1;
      for(var t = 0;t<3;t++){
        if(lv==1&&t==2){
          var st = 1;
        }
        var v = GetRoundsDataLv(p,lv,t);
        if(v==undefined) break;
        var badd2 = false;
        if(t==0) lvs[lv] = nv0;
        else if(t==1) lvs[lv] = nv0+nv1;
        else lvs[lv] = nv0+nv1+v;
        if(t==0) GameGlobal.GameScore[p]+=nv0;
        else if(t==1&&nv0!=10) GameGlobal.GameScore[p]+=nv1;
        // else {
        //   GameGlobal.GameScore[p]+=v;
        // }
        var bMove = false,rW = 3;
        for(var k = 0;k<rhits.length/rW;k++){
          rhits[k*rW+1]--;
          var vd;if(v==-1) vd = nv0;else if(v==-2){vd = nv1;}else vd = v;
          rhits[k*rW+2]+=vd;
          GameGlobal.GameScore[p]+=vd;
          var rv = rhits[k*rW+1];var plv = rhits[k*rW];
          lvs[plv] = rhits[k*rW+2];
          if(rv==0) bMove = true;
        }
        if(bMove==true) rhits = rhits.slice(rW,rW*2);
        for(var k = 0;k<bhits.length/2;k++){
          var vd;if(v==-1) vd = nv0;else if(v==-2){vd = nv1;}else vd = v;
          bhits[k*2+1]+=vd;var plv = bhits[k*2];
          GameGlobal.GameScore[p]+=vd;
          lvs[plv] = bhits[k*2+1];
        }
        bhits = [];
        if(t>=0){    
          if(t==0){
            if(nv0>=GameGlobal.FullBD){
              SetRoundsDataLv(p,lv,0,-1);
              if(rhits.length==6) rhits = rhits.slice(rW,rW*2);
              rhits.push(lv);rhits.push(2);rhits.push(10);
              if(lv==GameGlobal.TotalLevel-1){
                lahit = 'AllHit';
              }
            }
          }
          else if(t==1){
            if(nv0+nv1>=GameGlobal.FullBD&&
              (lv<GameGlobal.TotalLevel-1||
              (lv==GameGlobal.TotalLevel-1&&lahit!='AllHit'))){
              // if(GameGlobal.GameAddBlq[GameGlobal.playerOrder]==0){
                SetRoundsDataLv(p,lv,1,-2);
              //} 
              bhits.push(lv);bhits.push(10);
            }
          }
        }
      }
      
    }
    var l = GameGlobal.gameLevel[p];
    var sl,el;
    if(l==0) el = 1;
    else if(l>=GameGlobal.TotalLevel-1)
      el = GameGlobal.TotalLevel-1;
    else
      el = l;
    sl = el-1; 
    if(el==GameGlobal.TotalLevel-1){
      ps["0"] = GameGlobal.gameUi["iRound"+p+"0"];
      ps["1"] = GameGlobal.gameUi["iRound"+p+"2"];
      GameGlobal.gameUi["iRound"+p+"1"].visible = false;
      GameGlobal.gameUi["iRound"+p+"2"].visible = true;
    }else{
      ps["0"] = GameGlobal.gameUi["iRound"+p+"0"];
      ps["1"] = GameGlobal.gameUi["iRound"+p+"1"];
      GameGlobal.gameUi["iRound"+p+"2"].visible = false;
      GameGlobal.gameUi["iRound"+p+"1"].visible = true;
    }
    if(sl>=0&&el>=0){
      for(var l = sl;l<=el;l++){
        var r = ps[""+(l-sl)];
        r.getChildByName("局数").text = (l+1);
        var kv = GameGlobal.GameLevelScore[p][l];
        for(var t = 0;t<3;t++){
          var t1 = t+1;
          var txtScore = r.getChildByName("第"+t1+"球");
          var s = r.getChildByName("符第"+t1+"球");
          var v = GetRoundsDataLv(p,l,t);
          if(v==-1){
            txtScore.text = "";s.loadImage("game/全中1.png");s.visible = true;
          }else if(v==-2){
            txtScore.text = "";s.loadImage("game/全中2.png");s.visible = true;
          }else if(v>=0){
            if(s) s.visible = false;if(txtScore) txtScore.text = v;
          }else{
            if(s) s.visible = false;if(txtScore) txtScore.text = "";
          }
        }
        r.getChildByName("分数").text = kv==-1?"":GetTotalScore(p,l);
      }      
    }
    if(GameGlobal.LeaveGame[p]==1) GameGlobal.GameScore[p] = -1; 
  }

  for(var k = 0;k<Rounds.length;k++){
    var kv = GameGlobal.GameLevelScore[GameGlobal.playerOrder][k]
    var lv = GetTotalScore(GameGlobal.playerOrder,k)
    Rounds[k].getChildByName("分数").text = lv==-1?"":lv;

    for(var t = 0;t<3;t++){
      var t1 = t+1;
      var txtScore = Rounds[k].getChildByName("第"+t1+"球");
      var s = Rounds[k].getChildByName("符第"+t1+"球");
      var v = GetRoundsDataLv(GameGlobal.playerOrder,k,t);
      if(v==-1){
        txtScore.text = "";s.loadImage("game/全中1.png");s.visible = true;
      }else if(v==-2){
        txtScore.text = "";s.loadImage("game/全中2.png");s.visible = true;
      }else if(v>=0){
        if(s) s.visible = false;if(txtScore) txtScore.text = v;
      }else{
        if(s) s.visible = false;if(txtScore) txtScore.text = "";
      }
    }   
  }
}
window.onViewCurrentBall = function(){
  var pl = GameGlobal.matchMeOrder;
  // if(GameGlobal.Once[pl] == true){
    // GameGlobal.Once[pl] = false;
    // gameDvs();
    var lmap;
    var v;
    if(GGlobal.dVs){
      v = GGlobal.dVs[GameGlobal.BpData[pl].length];
    }else{
      var pmap = processLDataMap(pl,0);
      v = GameGlobal.nBlpDownCount-(pmap.v0==10?0:pmap.v0);
    }
    lmap = processLData(pl,v);
    netGameLData([0,v]);
    if(lmap.eft==1){GameGlobal.HjpShowOnce = true;eftJd(GameGlobal.gameUi.sprJd1);}
    else if(lmap.eft==2){eftJd(GameGlobal.gameUi.sprJd2);}
    
    eftRound();
    var ps = GameGlobal.GameScore[pl];
    refreshItemScore();refreshGameScore();
    var pn = GameGlobal.GameScore[pl];
    var pus = ps/GameGlobal.LVS[2];
    var pue = 0,tw = 300;
    if(pn>GameGlobal.LVB){
      pue = (pn-GameGlobal.LVB)/(GameGlobal.LVS[2]-GameGlobal.LVB);
    }
    Laya.Tween.to(GameGlobal.gameUi.iGJBarTip,{width:pue*tw+6},500);
    GameGlobal.MBiaoQin = true;
    GameGlobal.MMeLevel = lmap.nlevel;
    GameGlobal.MMeLp = lmap.lb;
  // }
  return;
  if(GameGlobal.Once[GameGlobal.playerOrder] == true){
    GameGlobal.Once[GameGlobal.playerOrder] = false;

    var t = GameGlobal.LevelTimes[GameGlobal.playerOrder]-GameGlobal.gameTimes[GameGlobal.playerOrder]-1;
    var lv = GameGlobal.gameLevel[GameGlobal.playerOrder];
    var t1 = t+1;
    var txtScore = GameGlobal.Rounds[lv].getChildByName("第"+t1+"球");

    console.log("pviv["+GameGlobal.playerOrder+"]:"+GameGlobal.gameTimes[GameGlobal.playerOrder]+" "+lv+" "+t1+" "+txtScore);
    var v;var vblp0 = GetRoundsDataLv(GameGlobal.playerOrder,lv,0);var vblp1 = GetRoundsDataLv(GameGlobal.playerOrder,lv,1);
    var nv0 = vblp0?(vblp0==-1?10:vblp0):0;
    var nv1 = vblp1?(vblp1==-2?10:vblp1):0;
    if(GameGlobal.GameAddBlqState[GameGlobal.playerOrder] == 'AllHit'){
      if(t==1)  v = GameGlobal.nBlpDownCount;
      else if(t==2){
        if(vblp1==10)  v = GameGlobal.nBlpDownCount;
        else v = GameGlobal.nBlpDownCount - vblp1;
      }
    }
    else if(GameGlobal.GameAddBlqState[GameGlobal.playerOrder] == 'BHit')  v = GameGlobal.nBlpDownCount;
    else {
      if(t==0) v = GameGlobal.nBlpDownCount;
      else if(t==1){
        if(vblp0==-1) v = GameGlobal.nBlpDownCount;
        else v = GameGlobal.nBlpDownCount - vblp0;
      }else if(t==2){
        if(vblp0==-1){
          if(vblp1==-2)
            v = GameGlobal.nBlpDownCount;
          else 
            v = GameGlobal.nBlpDownCount-vblp1;
        }else{
          // console.log("Xgq Error.......");
          v = 0;
        }
      }
    }
    SetRoundsDataLv(GameGlobal.playerOrder,lv,t,v);
    netGameLData([0,v]);
    // var ls = nv0+nv1+v;
    // GameGlobal.GameLevelScore[GameGlobal.playerOrder][lv] = ls;
    // var rhits = GameGlobal.RoundsAHit[GameGlobal.playerOrder];
    // var bMove = false,rW = 3;
    // for(var k = 0;k<rhits.length/rW;k++){
    //   rhits[k*rW+1]--;
    //   rhits[k*rW+2]+=v;
    //   GameGlobal.GameScore[GameGlobal.playerOrder]+=v;
    //   var rv = rhits[k*rW+1];var plv = rhits[k*rW];
    //   GameGlobal.GameLevelScore[GameGlobal.playerOrder][plv] = rhits[k*rW+2];
    //   if(rv==0) bMove = true;
    // }
    // if(bMove==true) GameGlobal.RoundsAHit[GameGlobal.playerOrder] = GameGlobal.RoundsAHit[GameGlobal.playerOrder].slice(rW,rW*2);
    // var bhits = GameGlobal.RoundsBHit[GameGlobal.playerOrder];
    // for(var k = 0;k<bhits.length/2;k++){
    //   bhits[k*2+1]+=v;var plv = bhits[k*2];
    //   GameGlobal.GameScore[GameGlobal.playerOrder]+=v;
    //   GameGlobal.GameLevelScore[GameGlobal.playerOrder][plv] = bhits[k*2+1];
    // }
    // GameGlobal.RoundsBHit[GameGlobal.playerOrder] = [];
    eftRound();
    // if(GameGlobal.GameAddBlqState[GameGlobal.playerOrder]!='BHit'&&GameGlobal.GameAddBlqState[GameGlobal.playerOrder]!='AllHit')
    // {
    //   GameGlobal.GameScore[GameGlobal.playerOrder]+=v;refreshGameScore();
    // }
    if(GameGlobal.GameAddBlq[GameGlobal.playerOrder]>0){
      GameGlobal.GameAddBlq[GameGlobal.playerOrder]--;
    }
    if(t>=0){    
      txtScore.text = ""+v;
      if(t==0){
        if(v>=GameGlobal.FullBD){
          SetRoundsDataLv(GameGlobal.playerOrder,lv,0,-1);
          var sprJd = GameGlobal.gameUi.sprJd;
          eftJd(sprJd);
          if(GameGlobal.RoundsAHit[GameGlobal.playerOrder].length==6){
             GameGlobal.RoundsAHit[GameGlobal.playerOrder] = GameGlobal.RoundsAHit[GameGlobal.playerOrder].slice(rW,rW*2);
          }
          if(lv==GameGlobal.TotalLevel-1&&GameGlobal.GameAddBlqOnce[GameGlobal.playerOrder]==true){
            if(GameGlobal.GameAddBlq[GameGlobal.playerOrder]==0){
              GameGlobal.GameAddBlqState[GameGlobal.playerOrder] = 'AllHit';
              GameGlobal.LastLevelAllHit[GameGlobal.playerOrder] = 'AllHit';
              GameGlobal.LevelTimes[GameGlobal.playerOrder]++;
              GameGlobal.gameTimes[GameGlobal.playerOrder] = GameGlobal.GameAddBlq[GameGlobal.playerOrder] = 2;
            }
            GameGlobal.GameAddBlqOnce[GameGlobal.playerOrder] = false;
          }
          GameGlobal.HjpShowOnce = true;
          // GameGlobal.RoundsAHit[GameGlobal.playerOrder].push(lv);
          // GameGlobal.RoundsAHit[GameGlobal.playerOrder].push(2);
          // GameGlobal.RoundsAHit[GameGlobal.playerOrder].push(v);
        }
      }
      else if(t==1){
        var av = v+vblp0;
        if(av>=GameGlobal.FullBD&&
          (lv<GameGlobal.TotalLevel-1||
          (lv==GameGlobal.TotalLevel-1&&GameGlobal.LastLevelAllHit[GameGlobal.playerOrder]!='AllHit'))){
          // if(GameGlobal.GameAddBlq[GameGlobal.playerOrder]==0){
          //   SetRoundsDataLv(GameGlobal.playerOrder,lv,1,-2);
          // } 
          if(lv==GameGlobal.TotalLevel-1&&GameGlobal.GameAddBlqOnce[GameGlobal.playerOrder]==true){
            GameGlobal.GameAddBlqState[GameGlobal.playerOrder] = 'BHit'
            GameGlobal.HjpShowOnce = true;
            GameGlobal.LevelTimes[GameGlobal.playerOrder]++;
            GameGlobal.gameTimes[GameGlobal.playerOrder] = GameGlobal.GameAddBlq[GameGlobal.playerOrder] = 1;
            GameGlobal.GameAddBlqOnce[GameGlobal.playerOrder]=false;
          }
          var sprJd = GameGlobal.gameUi.sprJd;
          eftJd(sprJd);
          // GameGlobal.RoundsBHit[GameGlobal.playerOrder].push(lv);
          // GameGlobal.RoundsBHit[GameGlobal.playerOrder].push(av);
        }
      }
    }   
    refreshItemScore();
    refreshGameScore();
  }
}


window.gameDvs = function(){
  if(GGlobal.dVs){
    if(GameGlobal.gameTimes[GameGlobal.playerOrder]==1) GameGlobal.nBlpDownCount = GGlobal.dVs[GameGlobal.dCount++];
    else 
    { 
      if(GGlobal.dVs[GameGlobal.dCount-1]==10){
        GameGlobal.nBlpDownCount = GGlobal.dVs[GameGlobal.dCount++];
      }
      else{
        GameGlobal.nBlpDownCount = GGlobal.dVs[GameGlobal.dCount-1] + GGlobal.dVs[GameGlobal.dCount]; 
        GameGlobal.dCount++
      }
    }
  }
}

window.onGameJs = function(){
  if(GameGlobal.gameOver==true) return;
  var _ui = GameGlobal.gameUi;
  _ui.pItemAsk.visible = false;
  _ui.pJsAsk.visible = false;
  blqClearBattleTime();
  Laya.timer.handlerClear("pwTimeLoop");
  _ui.sprFinger.visible = false;
  GameGlobal.gameOver = true;
  if(GameGlobal.gameMode==0)
  {
    _ui.pRounds.y = 400;
    _ui.pRounds.alpha = 1;
    _ui.btnOverMoney.visible = true;
    _ui.btnBWin.visible = _ui.btnBLose.visible = false;
  }else{
    _ui.pRounds.y = 300;
    _ui.pRounds.alpha = 1;
    _ui.btnOverMoney.visible = false;
  }
  GameGlobal.playerOrder = 0;
  refreshItemScore();
  refreshGameScore();

  if(GameGlobal.gameMode==1){
    GameGlobal.playerOrder = 1;
    refreshItemScore();
  }
  _ui.imgAv3.skin = GGlobal.imgAv1;
  _ui.txtAv3.text = GGlobal.txtAv1;
  blqJsAv();
  blqShowJG();
  _ui.pOver.visible = true;

  _ui.hOver1.visible = true;
  _ui.hOver2.visible = false;
  _ui.pGame.visible = false;
  _ui.pMask.visible = true;
  function DMGetAnim(d,hpage){
    Laya.SoundManager.playSound(_s+"jinbi.mp3");
    _ui.btnOverGet.visible = _ui.btnOverDGet.visible = false;
    GameGlobal.JsDGet = true;
    var stime = 0,atime = 1500,btime = 1000,sx = 260,sw = 100,sy = 900,sh = 50,tx = 115,ty = 100,tw = 36,th = 25;
    var iss = GGlobal.maxscore;
    _ui.txtJsM1.text = _ui.txtJsM.text = iss;
    if(hpage==4){
      var preRound = Laya.loader.getRes("prefab/星星.prefab");
      var item = preRound.create();
      var stx = 500,sty = 40;
      item.x = sx+30;item.y = sy;
      _ui.pMAnim.addChild(item);
      Laya.Tween.to(item,{alpha:1,scaleX:3.0,scaleY:3.0},100,Laya.Ease.linearNone,Laya.Handler.create(null,
        function(){
          Laya.Tween.to(item,{x:stx,y:sty,scaleX:1.5,scaleY:1.5},500,Laya.Ease.linearNone,Laya.Handler.create(null,function(){
            Laya.Tween.to(item,{alpha:0},50,Laya.Ease.linearNone,Laya.Handler.create(null,function(){
              GGlobal.maxscoreI++;refreshMSNum("game");GGlobal.SaveItem(null,null,true,GameGlobal.GameScore[GameGlobal.matchMeOrder]);
            }))
          }))
      }))
    }
    GGlobal.maxscore+=d;
    GGlobal.SaveItem(null,null,true,GameGlobal.GameScore[GameGlobal.matchMeOrder]);
    Laya.timer.handlerLoop("DMLoop",16,function(){
        stime+=16;
        if(stime<=atime){
          _ui.txtJsM.text = iss + Math.floor(stime/atime*d);
          var r = Math.random();
          if(r<0.3){
            var preRound = Laya.loader.getRes("prefab/金币.prefab");
            var item = preRound.create();
            var itx =  Math.random()*tw+tx,ity = Math.random()*th+ty,rt = Math.random()*100+100;
            item.x = Math.random()*sw+sx;
            item.y = Math.random()*sh+sy;
            _ui.pMAnim.addChild(item);
            Laya.Tween.to(item,{alpha:1,scaleX:1.5,scaleY:1.5},100,Laya.Ease.linearNone,Laya.Handler.create(null,
              function(){
                Laya.Tween.to(item,{x:itx,y:ity,scaleX:0.5,scaleY:0.5},rt,Laya.Ease.linearNone,Laya.Handler.create(null,function(){
                  Laya.Tween.to(item,{alpha:0},50,Laya.Ease.linearNone,Laya.Handler.create(null,function(){
                  }))
                }))
            }))
          }
        }
        if(stime>=atime+btime){
          _ui.txtJsM.text = iss + d;
          _ui.pMAnim.removeChildren();
          Laya.timer.handlerClear("DMLoop");
          GoToHome.bind(hpage)();
        }
    })
  }
  if(GameGlobal.GameWhere=="PassLevel"){
    _ui.hOver1.visible = false; _ui.hOver3.visible = true; _ui.pRounds.visible = false; _ui.pRounds2.visible = false; _ui.hOver31.visible = false; _ui.hOver32.visible = false;
    var btndlq = _ui.hOver31.getChildByName("领取双倍"); var btnlq = _ui.hOver31.getChildByName("领取"); 
    var ijb = _ui.hOver3.getChildByName("金币"); var izs = _ui.hOver3.getChildByName("钻石"); var sdo = GameGlobal.SDO;var glvs = GGlobal.sgame.glvs;
    var star = getPLevelStar(GameGlobal.PLV);var dg = glvs["lv"+glvs.lv]!=3;eftLaterShow(btnlq,"hOver31LQ",1500);

    var titarget;var ti = _ui.hOver3.getChildByName("挑战信息");var tiw = ti.getChildByName("挑战成功");var til = ti.getChildByName("挑战失败");
    function GoToS2(){_ui.hOver32.visible = _ui.btnHOver3Back.visible = true;_ui.hOver31.visible = false;_ui.btnHOver3Back.visible = true;
      _ui.btnHOver3Ag.setOnClick(function(){blqGoGame.bind(0)("PassLevel");})
      _ui.btnHOver3Next.setOnClick(function(){GoToHome.bind(3)();
        GameGlobal.BtnClick["btnGJ"]();
        GameGlobal.LevelTipInfo(GGlobal.sgame.glvs.lv);
      })
    }

    if(star>0){  tiw.visible = true;til.visible = false;titarget = tiw;ijb.visible = izs.visible = true;}
    else{tiw.visible = false;til.visible = true;titarget = til;ijb.visible = izs.visible = false;GoToS2();}

    titarget.scaleX = titarget.scaleY = 0.0;
    Laya.Tween.to(titarget,{scaleX:1.0,scaleY:1.0},900,Laya.Ease.elasticOut,Laya.Handler.create(null,
      function(){
    }))
    if(star>0){
      var os = glvs["lv"+glvs.lv];os = os?os:0;
      if(star>os){glvs["lv"+glvs.lv] = star;}
      if(GameGlobal.PLV==GGlobal.sgame.glvs.lv)
        GGlobal.sgame.glvs.lv++; 
      GGlobal.SaveItem(null,null,true); 
      var xin = _ui.hOver3.getChildByName("星星");
      for(var k = 1;k<=3;k++){
        var nxin = xin.getChildByName(""+k);nxin.scaleX = nxin.scaleY = 1;
        nxin.visible = false;
        if(k<=star){
          nxin.gray = false;
          Laya.timer.handlerOnce("StarJS"+k,k*300,(function(){
            var that = this;that.visible = true;
            Laya.Tween.to(that,{scaleX:2,scaleY:2},200,Laya.Ease.linearNone,Laya.Handler.create(null,function(){
              Laya.Tween.to(that,{scaleX:1,scaleY:1},200,Laya.Ease.elasticOut);
            }));
          }).bind(nxin));
        }else{
          nxin.gray = true;
        }
      }
      Laya.timer.handlerOnce("LJS",800,function(){_ui.hOver31.visible = true;})
    }
    for(k = 1;k<=3;k++){ _ui.hOver3s.getChildByName(""+k).gray = k>star;}
    izs.visible = star>0&&dg;
    btndlq.setOnClick(function(){
      function LevelDlq(){
        var rl = glvs.lv%5;
        var m = sdo[2*rl+2].m,d = sdo[2*rl+2].d;
        GGlobal.maxscore+=m;
        if(dg){GGlobal.sgame.jlq+=d; }
        refreshMSNum("game");
        GoToS2();
      }
      GGlobal.Share("闯关双倍",LevelDlq);
    })
    btnlq.setOnClick(function(){GoToS2();})
    _ui.btnHOver3Back.setOnClick(function(){GoToHome.bind(3)();})

    return;
  }
  GameGlobal.GameAddBlqState[GameGlobal.playerOrder] = '';
  if(GameGlobal.gameMode==0){
    _ui.btnOverMoney.visible = true;
    _ui.fTgs.value = ""+GameGlobal.GameScore[GameGlobal.playerOrder];
    GGlobal.createBannerAd("gamejs00",true,Laya.Browser.clientHeight-GGlobal.BannerHh,Laya.Browser.clientWidth);
    _ui.btnOverMoney.setOnClick(function(){
      _ui.btnOverMoney.visible = false;
      _ui.hOver1.visible = false;
      _ui.btnOverGet.visible = false;
      _ui.hOver2.visible = true;
      GGlobal.createBannerAd("gamejs01",true,Laya.Browser.clientHeight-GGlobal.BannerHh,Laya.Browser.clientWidth);
      if(GGlobal.gameConfig[0]!=0){
        Laya.timer.handlerOnce("JsDGet",1500,function(){
          if(GameGlobal.JsDGet==false)  _ui.btnOverGet.visible = true;
        });
      }else{
        _ui.btnOverGet.visible = true;
      }
      _ui.pRounds.visible = false;
      eftFTgs(_ui.fTgs);
    });    
  }else{
    if(GameGlobal.IsFriendBattle==true){
      _ui.btnBLose.setOnClick(function(){
        GoToHome.bind(5)();
      }); 
    }else{
      _ui.btnBLose.setOnClick(function(){
        _ui.btnBLose.visible = false;
        _ui.hOver1.visible = false;
        _ui.pRounds.visible = false;
        _ui.pRounds2.visible = false;
        _ui.pJsAsk.visible = true;
        _ui.btnBJ.setOnClick(function(){
          if(GGlobal.sgame.jlq>=2){
            eftFloat("保级成功",'#fbfbfb',300);
            GGlobal.sgame.jlq-=2;refreshMSNum("game");GGlobal.SaveItem(null,null,true);
            Laya.timer.handlerOnce("BaoJi",1000,function(){
              GoToHome.bind(4)();
            })
          }else{
            eftFloat("钻石不足，无法保级",'#fbfbfb',300);
          }
        })
        _ui.btnBJNo.setOnClick(function(){
          if(GGlobal.maxscoreI>0){
            GGlobal.maxscoreI--;refreshMSNum("game");GGlobal.SaveItem(null,null,true);
          }
          GoToHome.bind(4)();
        })
      }); 
      _ui.btnBWin.setOnClick(function(){
        _ui.btnBWin.visible = false;
        var kv = getRankingLevel(GGlobal.maxscoreI,GameGlobal.RankingLS);
        var get = GameGlobal.RankingRM[kv[0]];
        DMGetAnim(get,4);
      });
    }
  }
  _ui.txtJsM.text = _ui.txtJsM1.text = GGlobal.maxscore;
  _ui.txtJsS.text = _ui.txtJsS1.text = GGlobal.maxscoreI;
  GameGlobal.JsDGet = false;
  _ui.btnOverGet.setOnClick(function(){
    DMGetAnim(GameGlobal.GameScore[GameGlobal.playerOrder],3);
  });
  //GameConfigToDo
  if(GGlobal.gameConfig[0]==0){
    _ui.btnOverDGet.visible = false;
  }else{
    var txt = _ui.btnOverDGet.getChildByName("Text");
    txt.text = (GGlobal.sgame.jsend==0?"五倍":"双倍")+"领取";

    _ui.btnOverDGet.setOnClick(function(){
      function callBack2(){
        DMGetAnim(GameGlobal.GameScore[GameGlobal.playerOrder]*2,3);
      }
      function callBack5(){
        GGlobal.RequestPost(GGlobal.REMOTE_SERVER_URL+'game0d',
          {openId:GGlobal.openId},
          function(info,jsData){
            GGlobal.sgame.jsend = 1;
          },
          function(info,jsData){}
        )
        DMGetAnim(GameGlobal.GameScore[GameGlobal.playerOrder]*5,3);
      }
      if(GGlobal.gameConfig[0]==1){
        GGlobal.Share("单人游戏双倍获取",callBack);
      }
      else {
        if(GGlobal.sgame.jsend==0)
          GGlobal.showAdvertisement("game0d",callBack5); 
        else
          GGlobal.Share("单人游戏双倍获取",callBack2);
      }
    });
  }
  GameGlobal.LevelTimes[GameGlobal.playerOrder] = GameGlobal.MaxLevelTimes;
}

window.onNextLevel = function(){
  GameGlobal.cFollow = 0;
  return;
  if(!(GameGlobal.GameAddBlqState[GameGlobal.playerOrder] == 'AllHit'||GameGlobal.GameAddBlqState[GameGlobal.playerOrder] == 'BHit')){
    GameGlobal.gameTimes[GameGlobal.playerOrder] = GameGlobal.LevelTimes[GameGlobal.playerOrder];
  }
  var tb = testGameEnd(GameGlobal.playerOrder);
  if(tb){
    // onGameJs();
  }else{
    var addLevel = true;
    if(GameGlobal.GameAddBlqState[GameGlobal.playerOrder] == 'AllHit'||GameGlobal.GameAddBlqState[GameGlobal.playerOrder] == 'BHit'){
      if(GameGlobal.gameLevel[GameGlobal.playerOrder]==GameGlobal.TotalLevel-1) 
        addLevel = false;
      if(GameGlobal.nBlpDownCount==10){
        newBlp = true;
      }
    }else{
      newBlp = true;
    }
    if(newBlp==true){
      GameGlobal.nBlpDownCount = 0;
      recreateBlps();
    }
    if(addLevel==true) {
      GameGlobal.gameLevel[GameGlobal.playerOrder]++;
    }
  }
  // console.log("pvil["+GameGlobal.playerOrder+"]:"+GameGlobal.gameLevel[GameGlobal.playerOrder]);
}
window.testGameEnd = function(p){
  if((GameGlobal.gameLevel[p]==GameGlobal.TotalLevel-1)&&(GameGlobal.GameAddBlq[p]==0||
    GameGlobal.gameTimes[p]==0)){
    return true;
  }else {
    return false;
  }
}


window.fadeBall = function(){
  if(GameGlobal.ball&&GameGlobal.ball.lact==true){
    GameGlobal.ball.transform.position = new Vector3(-1000,-100,0);
    GameGlobal.ball.lact = false;
    if(GameGlobal.turnMe){
      GameGlobal.gameSData = [];pumpGData();
      netGameSData(2);
    }
  }
  GameGlobal.PollBall = false;
}
window.resetOneBall = function(slevel){

  GameGlobal.gameSDataType = 0;
  GameGlobal.ballSkSize1 = 0;
  GameGlobal.TrailOnce = true;
  GameGlobal.ballShootCount = 0;
  GameGlobal.ballSocketCount2 = 0;
  GameGlobal.cshootball = true;
  GameGlobal.ballinput = null;
  GameGlobal.ballDataRecOnce = true;
  GameGlobal.cFollow = 0;
  GameGlobal.gameSData = [];
  GameGlobal.MachineOnce = 1;
  GameGlobal.useYellowBall[GameGlobal.playerOrder] = false;
  GameGlobal.canUseYellow = true;
  GameGlobal.BlpBombOnce = true;
  if(GameGlobal.BPO==1)
    GameGlobal.camera.transform.position = (new Vector3(GameGlobal.GConfig.cx+GameGlobal.BOY,GameGlobal.GConfig.cy+1,GameGlobal.GConfig.cz));
  else
    GameGlobal.camera.transform.position = (new Vector3(GameGlobal.GConfig.cx,GameGlobal.GConfig.cy,GameGlobal.GConfig.cz));
  
  if(slevel){
    blqStartBattleTime();
  }
}

window.processGameHjqGet = function(){
  resetOneBall();
  if(GameGlobal.HjpGet==true){
    GameGlobal.cshootball = false;
    infoGetItem(GameGlobal.gameUi,"game",0);
    GameGlobal.FnNextStep = endOneBall;
    GameGlobal.gameUi.pGetItem.visible = true;
  }else{
    endOneBall();
  }
  GameGlobal.HjpGet = false;
}

window.removeBallRigidBody = function(){
  var idx = GameGlobal.rigidBodies.indexOf(GameGlobal.ball);
  if(idx!=-1){
    if(GameGlobal.ball.userData&&GameGlobal.ball.userData.physicsBody){
      GameGlobal.physicsWorld.removeRigidBody(GameGlobal.ball.userData.physicsBody);
      GameGlobal.ball.userData.physicsBody = null;
    }
    GameGlobal.rigidBodies.splice(idx,1);
  }
  idx = GameGlobal.rigidBodies2.indexOf(GameGlobal.ball);
  if(idx!=-1){
    if(GameGlobal.ball.userData&&GameGlobal.ball.userData.physicsBody){
      GameGlobal.physicsWorld2.removeRigidBody(GameGlobal.ball.userData.physicsBody);
      GameGlobal.ball.userData.physicsBody = null;
    }
    GameGlobal.rigidBodies2.splice(idx,1);
  }
}
window.refreshGameScore = function(){
  if(GameGlobal.gameOver == true){
    GameGlobal.gameUi.txtGs.visible = GameGlobal.GameScore[0]!=-1;
    GameGlobal.gameUi.txtGs2.visible = GameGlobal.GameScore[1]!=-1;
    GameGlobal.gameUi.txtGs.value = GameGlobal.GameScore[0];
    GameGlobal.gameUi.txtGs2.value =  GameGlobal.GameScore[1];
  }else{
    GameGlobal.gameUi.txtGs.visible =  GameGlobal.GameScore[GameGlobal.matchMeOrder]!=-1;
    GameGlobal.gameUi.txtGs.value = GameGlobal.GameScore[GameGlobal.matchMeOrder];
  }
  // if(GameGlobal.gameMode==0)
  //   GameGlobal.gameUi["txtUs"+GameGlobal.playerOrder].text = GameGlobal.GameScore[GameGlobal.playerOrder];
  // else{
  //   GameGlobal.gameUi["txtUs00"].text = GameGlobal.GameScore[0];
  //   GameGlobal.gameUi["txtUs1"].text = GameGlobal.GameScore[1];
  // }
}
