window.processFingerInput = function(){
  var Sd1 = 0.05,Sd2 = 3.0,S1 = 0.08;
  var sx,sz;var bmoveposc = GameGlobal.bmoveposc;
  var cpose = bmoveposc.length>2?bmoveposc[bmoveposc.length - 1]:bmoveposc[0]; 
  var cposs = bmoveposc[0]; 
  // console.log("Xgq cpose:"+cpose.x+" "+cpose.z);
  dposes = new Laya.Vector3(cpose.x-cposs.x,0,cpose.z-cposs.z);
  dn = new Laya.Vector3(dposes.x,dposes.y,dposes.z);
  dlength = Math.sqrt(dn.x*dn.x+dn.y*dn.y+dn.z*dn.z);
  if(dlength>0.00001){
    dn.x/=dlength;dn.y/=dlength;dn.z/=dlength;
  } else {dn.x = 0;dn.y = 0;dn.z = 0;}
  var tl = 0,s = 0,cs = 0;
  var maxl = -1000000;
  GameGlobal.blv = 0;
  var ce = bmoveposc[bmoveposc.length-1];

  sx = bmoveposc[0].x;sz = bmoveposc[0].z;
  var cdpos = new Laya.Vector3(ce.x-sx,0,ce.z-sz);
  var clen = v3len(cdpos);
  for(var k = 0;k<bmoveposc.length;k++){
    var bmp = bmoveposc[k];
    // console.log("Xgq bmoveposc["+k+"]:"+bmp.x+" "+bmp.z);
    var p = bmoveposc[k]; 
    var dx = p.x - sx,dz = p.z - sz;
    var sdl = Math.sqrt(dx*dx+dz*dz);
    var ndx = dx/sdl,ndz = dz/sdl;
    var ncs = ndx*dn.z-ndz*dn.x;  
    if(ncs>0) cs+=1; else cs-=1;
    var dot = dx*dn.x+dz*dn.z;
    var dnx = dn.x* dot,dnz = dn.z* dot;
    var dpx = dx - dnx,dpz = dz - dnz;  
    var l = Math.sqrt(dpx*dpx+dpz*dpz);
    if(l>maxl) maxl = l;
    tl+=l;
  }
  var dsrad = maxl/clen,psrad;var s1;
  if(dsrad<S1) {  psrad = dsrad*Sd1; s1 = psrad;}
  else {psrad = dsrad*Sd2; s1 = Math.pow((psrad-S1),0.94)+S1*Sd1; }
  GameGlobal.blv = s1 * Math.sign(cs);
}

window.insertMp = function(x,y){
  var mouseCoords = GameGlobal.mouseCoords,plane = GameGlobal.plane; var GConfig = GameGlobal.GConfig; var outMp =new Laya.Vector3(),touchRay=Laya.Input3D._tempRay0,viewport=GameGlobal.camera.viewport; mouseCoords.x = x* viewport.width; mouseCoords.y = y*viewport.height-1; var cx = x*window.innerWidth,cy = y *window.innerHeight; GameGlobal.bmoveposc.push({x:cx,z:cy});
  GameGlobal.camera.viewportPointToRay(mouseCoords,touchRay);var outMp =new Laya.Vector3(); CollisionUtils.intersectsRayAndPlaneRP(touchRay,plane,outMp);  

  if(outMp.x>-GConfig.IBRect.w&&outMp.x<GConfig.IBRect.w&&
        outMp.z>GConfig.IBRect.t&&outMp.z<GConfig.IBRect.d)
  {
    // console.log("Xgq outMp："+outMp.x+" "+outMp.y+" "+outMp.z);
    if(GameGlobal.bmovepos.length==0){
      GameGlobal.sMPos = new Laya.Vector3(outMp.x,outMp.y,outMp.z);
      GameGlobal.bmovepos.push(outMp);
    }else{
      var sMPos = GameGlobal.sMPos;
      var dx = outMp.x - sMPos.x,dy = outMp.y - sMPos.y,dz = outMp.z - sMPos.z;
      var l = Math.sqrt(dx*dx+dy*dy+dz*dz);
      if(l<0.3||dz>0){
        GameGlobal.bmovepos.length = 0;
        GameGlobal.bmoveposc.length = 0;
        GameGlobal.stime = performance.now();
      }else{
        GameGlobal.bmovepos.push(outMp);
      }
    }
    if(GameGlobal.ball) GameGlobal.ball.transform.position = new Laya.Vector3(outMp.x+GameGlobal.SBOX,outMp.y,outMp.z);
  }  
}
