window.updateHumanOther = function(){
    if(GameGlobal.ballDataRecOnce==true&&GameGlobal.gameSData.length>0){
      GameGlobal.GDataState = 1;
      GameGlobal.Once[GameGlobal.playerOrder] = false;
      GameGlobal.ballDataRecOnce = false;
      GameGlobal.pbodys = [];
      GameGlobal.battlePack = 0;
      GameGlobal.battleTurnThinking = false;
      if(!GameGlobal.ball) sceneAddBall(GameGlobal.scene,new Laya.Vector3(0,100,0));
      var rigidBodies = GameGlobal.BPO==0?GameGlobal.rigidBodies:GameGlobal.rigidBodies2;
      Array.prototype.push.apply(GameGlobal.pbodys,rigidBodies);
      if(rigidBodies.length<11) GameGlobal.pbodys.push(GameGlobal.ball);
      resetShootOneBall(GameGlobal.useYellowBall[GameGlobal.playerOrder]);
      Laya.timer.handlerOnce("updateHumanOther",500,function(){GameGlobal.GDataState=2;GameGlobal.ballinput = GameGlobal.ball;})
    }
    function endGData(soff){
      if(soff>=GameGlobal.gameSData.length&&GameGlobal.battlePack==2)
      {
        GameGlobal.GDataState = 3;
        GameGlobal.gameSData = [];
        GameGlobal.ballShootCount = GameGlobal.ballSocketCount2 = 0;
        GameGlobal.ballSkSize1 = 0;
        GameGlobal.gameSDataType = 0;
      }
    }
    if(GameGlobal.GDataState==2){
      if(GameGlobal.gameSDataType == 0){
        var soff = GameGlobal.ballShootCount*6;
        if(soff<GameGlobal.ballSkSize1)
        {
          var px = GameGlobal.gameSData[soff],py = GameGlobal.gameSData[soff+1],pz = GameGlobal.gameSData[soff+2];
          var rx = GameGlobal.gameSData[soff+3],ry = GameGlobal.gameSData[soff+4],rz = GameGlobal.gameSData[soff+5];
          GameGlobal.ball.transform.position = new Laya.Vector3(px,py,pz);
          GameGlobal.ball.transform.rotationEuler = new Laya.Vector3(rx,ry,rz);
          GameGlobal.ballShootCount++;
        }
        if(GameGlobal.battlePack>0&&soff>=GameGlobal.ballSkSize1){  
          // console.log("Xgq GameGlobal.ballSkSize1")
          GameGlobal.gameSDataType = 1;
        }
        endGData(soff);
      }else {
        var soff = GameGlobal.ballShootCount*6 + GameGlobal.ballSocketCount2 * 66;
        if(soff<GameGlobal.gameSData.length){
          for(var k = 0;k<GameGlobal.pbodys.length;k++){
            var objThree = GameGlobal.pbodys[k];
            var asoff = soff + k*6;
            var px = GameGlobal.gameSData[asoff],py = GameGlobal.gameSData[asoff+1],pz = GameGlobal.gameSData[asoff+2];
            var rx = GameGlobal.gameSData[asoff+3],ry = GameGlobal.gameSData[asoff+4],rz = GameGlobal.gameSData[asoff+5];
            objThree.transform.position = new Laya.Vector3(px,py,pz);
            objThree.transform.rotationEuler = new Laya.Vector3(rx,ry,rz);
          }
          GameGlobal.ballSocketCount2++;
        }
        endGData(soff);
      }
    }
}