
window.onShootBall = function(){
    
    var dtime = GameGlobal.etime - GameGlobal.stime;
    var bmovepos = GameGlobal.bmovepos,scene = GameGlobal.scene,GConfig = GameGlobal.GConfig;
    var margin = GameGlobal.margin,GConfig = GameGlobal.GConfig;
    var bmoveposc = GameGlobal.bmoveposc;
    var pl = GameGlobal.matchMeOrder;
    var physicsWorld,rigidBodys;
    if(GameGlobal.BPO==0) {
      physicsWorld = GameGlobal.physicsWorld;
      rigidBodies = GameGlobal.rigidBodies;
    }
    else {
      physicsWorld = GameGlobal.physicsWorld2;
      rigidBodies = GameGlobal.rigidBodies2;
    }
    var ballMass = GConfig.bm; var ballRadius = GConfig.br;
    var ball;var poss,pose,posl,dposes,ts,length,dn,dlength,pose2,deslen,olength;
    function v3len(v){return Math.sqrt(v.x*v.x+v.y*v.y+v.z*v.z);}
    function v3sub(a,b) {return new Laya.Vector3(a.x-b.x,a.y-b.y,a.z-b.z);}
    function v3n(v){var s = v3len(v);if(s<0.0001) return new Laya.Vector3(0,0,0);else return new Laya.Vector3(v.x/s,v.y/s,v.z/s);}
    var GConfig = GameGlobal.GConfig;
    GConfig.Skew1 = 600;GConfig.Skew2 = 2000;GConfig.Skew3 = 3;
    GConfig.bmax = 65;GConfig.bscale = 120;
    GConfig.bmin = 20;
    var pIsM = GameGlobal.IsMachine==true&&!GameGlobal.turnMe;
    var sdes;
    if(!pIsM){if(bmovepos.length<2) return;
      poss = bmovepos[0];
      pose = bmovepos[bmovepos.length - 1];
      pose2 = bmovepos.length>2?bmovepos[bmovepos.length - 3]:bmovepos[0]; 
      pose2 = bmovepos.length>3?bmovepos[bmovepos.length - 4]:bmovepos[0]; 
      dposes = v3sub(pose,poss);
      dpos = v3sub(pose,pose2);dpos = v3n(dpos);
      deslen = Math.abs(bmovepos[bmovepos.length - 1].z - bmovepos[0].z);
      
      dtime = Math.pow(dtime,0.9);
      ts = 1.0*deslen*GConfig.bscale/dtime;dpos.x *=ts;dpos.y*=ts;dpos.z*=ts;
      length = v3len(dpos);
      olength = length;
      if(dpos.z>0||length<2)  return;
      GameGlobal.gameUi.sprFinger.visible = false;
      Laya.timer.handlerClear("fingerTimeLoop");
      saveGame(GameGlobal.matchMeOrder);
      var absx = Math.abs(dpos.x);
      if(absx>GConfig.bmaxvx){
        var s = GConfig.bmaxvx/absx;
        dpos.x *=s;dpos.y*=s;dpos.z*=s;
      }
      if (length > GConfig.bmax) {
        dpos.x/=length;dpos.y/=length;dpos.z/=length;
        dpos.x *=GConfig.bmax;dpos.y*=GConfig.bmax;dpos.z*=GConfig.bmax;
      }else if(length<1){
        dpos.x = 0,dpos.y = 0,dpos.z = -20;
      }
      else if(length<GConfig.bmin){
        dpos.x/=length;dpos.y/=length;dpos.z/=length;
        dpos.x *=GConfig.bmin;dpos.y*=GConfig.bmin;dpos.z*=GConfig.bmin;
      }
    }
    if(GameGlobal.useYellowBall[pl]){
      pose.x = pose.x*0.3;
    }
    if(GameGlobal.ball){
      ball = GameGlobal.ball; ball.transform.position = new Vector3(pose.x,pose.y,pose.z); ball.transform.rotation = new Laya.Quaternion(0,0,0,1);
    }else{
      ball = sceneAddBall(scene,pose);
    }
    removeBallRigidBody();
    GameGlobal.ballinput = GameGlobal.ball;
    resetShootOneBall(GameGlobal.useYellowBall[pl]);
    var ballShape = new Ammo.btSphereShape(ballRadius*GameGlobal.trand*1.1); ballShape.setMargin(margin); 
    var ballBody = createRigidBody(ball, ballShape, ballMass, pose, null);
    setBodyFriction(ballBody);
    GameGlobal.SBPZ = pose.z;
    var mv = v3n(dpos);var mlen = v3len(dpos); var mvdir = new Laya.Vector3(0,0,mv.z*mlen); var mvs = GConfig.mvs; var msr,ms; if(GameGlobal.useYellowBall[pl]){msr = 0.85; }else{msr = GConfig.jzmax*(1-mvs)+mvs*GConfig.jzmin; } ms = 1 - msr; var mt = new Laya.Vector3(mvdir.x*msr+dpos.x*ms,mvdir.y*msr+dpos.y*ms,mvdir.z*msr+dpos.z*ms);
    if(GameGlobal.useYellowBall[pl]) ballBody.setLinearVelocity(new Ammo.btVector3(mt.x*1.3,0,mt.z*1.3)); else ballBody.setLinearVelocity(new Ammo.btVector3(mt.x,0,mt.z));
    GameGlobal.PollBall = true;
    processFingerInput();
    Laya.SoundManager.playSound(_s+"gd.mp3");
    clearGameOnce();
}

window.saveGame = function(pl){
  var save = {};
  var blpdata = {};
  var CameraData = [];
  var GameSaveStr;
  if(pl==GameGlobal.matchMeOrder)
  {
    var transformAux1 = GameGlobal.transformAux1;
    var camera = GameGlobal.camera;
    var rigidBodies = GameGlobal.rigidBodies;
    for(var k = 0;k<rigidBodies.length;k++){
      var objThree = rigidBodies[k];
      if(objThree.userData.blpstate=="Stand"){
        var objPhys = objThree.userData.physicsBody;
        var ms = objPhys?objPhys.getMotionState():null;ms.getWorldTransform(transformAux1);
        var p = transformAux1.getOrigin();var q = transformAux1.getRotation();
        var sz = [];sz.push(p.x());sz.push(p.y());sz.push(p.z());sz.push(q.x());sz.push(q.y());sz.push(q.z());sz.push(q.w());
        blpdata[""+k] = sz;
      }
    }
    GameSaveStr = ["RoundsData","RoundsAHit","RoundsBHit","GameScore","GameLevelScore",
    "gameTimes","gameLevel","LevelTimes","GameAddBlqState","Once","FhqDisState","BpData","useYellowBall"];

    var cpos = camera.transform.position;var rot = camera.transform.rotation;
    CameraData.push(cpos.x);CameraData.push(cpos.y);CameraData.push(cpos.z);
    CameraData.push(rot.x);CameraData.push(rot.y);CameraData.push(rot.z);CameraData.push(rot.w);
  }else{
    GameSaveStr = ["RoundsData","BpData"];
  }
  
  var GameSave = {};
  for(var k = 0;k<GameSaveStr.length;k++){
    GameSave[GameSaveStr[k]] = GGlobal.CLONE(GameGlobal[GameSaveStr[k]][pl]);
  }
  GameGlobal.SaveData[pl] = {
    GameSave:GameSave,
    CameraData:CameraData,
    nBlpDownCount:GameGlobal.nBlpDownCount,Hjp:GameGlobal.Hjp,blpdata:blpdata};
}
window.restoreGame = function(pl){
  var SaveData = GameGlobal.SaveData[pl];
  var GConfig = GameGlobal.GConfig;
  if(pl==GameGlobal.matchMeOrder){
    GameGlobal.Hjp = SaveData.Hjp;
    GameGlobal.nBlpDownCount = SaveData.nBlpDownCount;
    var camera = GameGlobal.camera;
    var CameraData = SaveData.CameraData;
    var physicsWorld = GameGlobal.physicsWorld;
    camera.transform.position = new Laya.Vector3(CameraData[0],CameraData[1],CameraData[2]);
    camera.transform.rotation = new Laya.Quaternion(CameraData[3],CameraData[4],CameraData[5],CameraData[6]);
    GameGlobal.ballinput = null;
    var rigidBodies = GameGlobal.rigidBodies;
    for(var i = 0;i<rigidBodies.length;i++){
      var item = rigidBodies[i];
      var opos = item.userData.originpos,body = item.userData.physicsBody;
      if(body) physicsWorld.removeRigidBody(body);

      if(item.userData.blpstate){  
        var ol = item.userData.localInertia;
        if(item.meshRenderer.material){
          item.meshRenderer.material.renderMode = 0;
        }
        item.meshRenderer.material.albedoTexture = GameGlobal.globalres[i==SaveData.HJP?"res/blp_01.jpg":"res/blp.jpg"];
        var bdata = SaveData.blpdata[""+i];
        if(bdata){
          item.userData.blpstate = "Stand";
          opos = new Laya.Vector3(bdata[0],bdata[1],bdata[2]);
          item.transform.position = new Vector3(opos.x,opos.y,opos.z);
          item.transform.rotation = new Laya.Quaternion(bdata[3],bdata[4],bdata[5],bdata[6]);
        }else{
          item.userData.blpstate = "Down";
          item.transform.position = new Vector3(opos.x,opos.y,opos.z);
          item.transform.rotation = new Laya.Quaternion(0,0,0,1);
          item.meshRenderer.material.renderMode = 2;
          item.meshRenderer.material.albedoColorA = 0;
        }

        var transform = new Ammo.btTransform();transform.setIdentity();
        transform.setOrigin(new Ammo.btVector3(opos.x, opos.y, opos.z));

        var motionState = new Ammo.btDefaultMotionState(transform);
        var localInertia = new Ammo.btVector3(ol.x, ol.y, ol.z);
        var rbInfo = new Ammo.btRigidBodyConstructionInfo(item.userData.mass, motionState, GConfig.blpshape, localInertia);
        var body = new Ammo.btRigidBody(rbInfo);
        body.name = "blp";
        if(bdata) physicsWorld.addRigidBody(body);
        item.userData.physicsBody = body;
        item.name = "restoreBlp"+i;
        body.setActivationState(4);
      }
    }
    netGameLData([1]);
  }
  for(var key in SaveData.GameSave){
    GameGlobal[key][pl] = GGlobal.CLONE(SaveData.GameSave[key]);
  }
  refreshItemScore();
  refreshGameScore();
}
window.resetShootOneBall = function(usey){
  // blqClearBattleTime();
  var trand = usey?1.25:1;
  var ball = GameGlobal.ball;
  var pl = GameGlobal.matchMeOrder;
  if(usey==true){
    ball.meshRenderer.material.albedoTexture = GameGlobal.globalres["res/blq_02.jpg"];
  }else{
    ball.meshRenderer.material.albedoTexture = GameGlobal.globalres[_r+"tex/blq"+(GGlobal.sgame.useq+1)+".jpg"]
  }
  ball.transform.scale = new Laya.Vector3(trand,trand,trand);
  GameGlobal.cFollow = 1;GameGlobal.cshootball = false;
  GameGlobal.bPrez = 10000;
  GameGlobal.gameTimes[pl]--;
  GameGlobal.ballShootCount = 0;
  GameGlobal.ballShootCountOnce = 1;
  GameGlobal.gameSDataType = 0;
  if(!(GameGlobal.gameMode==1&&GameGlobal.IsMachine==false&&GameGlobal.turnMe==false))
   GameGlobal.gameSData = [];
  GGlobal.sendCount = 0;
  GameGlobal.trand = trand;
  GameGlobal.gameUi.btnYellow.visible = false;
  GameGlobal.gameUi.btnFhq.visible = false;
}
window.initInput = function() {
    var mouseCoords = GameGlobal.mouseCoords,plane = GameGlobal.plane;
    var GConfig = GameGlobal.GConfig;
    
    if(GameGlobal.initInputOnce==true){
      canvas.addEventListener('touchstart', function (e) {
        // console.log("Xgq onShootBall touchstart "+GameGlobal.cshootball);
        GameGlobal.turnMe=true;
        if(GameGlobal.cshootball&&GameGlobal.gameOver==false&&GameGlobal.turnMe==true&&!GameGlobal.PEnds[GameGlobal.matchMeOrder]){
          
          GameGlobal.ballTouchStart = 1;
          GameGlobal.bmovepos = [];
          GameGlobal.bmoveposc = [];
          insertMp(e.touches[0].clientX/window.innerWidth,e.touches[0].clientY/window.innerHeight);
          GameGlobal.stime = performance.now();
        }
      }, false);
      canvas.addEventListener('touchmove', function (e) {
        // console.log("Xgq touchmove::"+performance.now());
        // console.log("Xgq onShootBall touchmove "+GameGlobal.cshootball);
        GameGlobal.turnMe=true;
        if (GameGlobal.cshootball&&GameGlobal.gameOver==false&&GameGlobal.turnMe==true&&GameGlobal.ballTouchStart==1&&!GameGlobal.PEnds[GameGlobal.matchMeOrder]){
          insertMp(e.touches[0].clientX/window.innerWidth,e.touches[0].clientY/window.innerHeight);
        }
      }, false);
      canvas.addEventListener('touchend', function (e) {
        // console.log("Xgq onShootBall touchend "+GameGlobal.ballTouchStart+" "+GameGlobal.cshootball);
        GameGlobal.turnMe=true;
        if (GameGlobal.cshootball&&GameGlobal.gameOver==false&&GameGlobal.turnMe==true&&GameGlobal.ballTouchStart==1&&!GameGlobal.PEnds[GameGlobal.matchMeOrder]){
          insertMp(e.changedTouches[0].clientX/window.innerWidth,e.changedTouches[0].clientY/window.innerHeight);
          GameGlobal.etime = performance.now();
          
          onShootBall();
          // GameGlobal.ShootBallFunc = onShootBall;
        }
        if(GameGlobal.gameOverOnce==false){
          GameGlobal.gameOver = false;
        }
        GameGlobal.ballTouchStart = 0;
      }, false);
      GameGlobal.initInputOnce = false;
    }
  }  
