//wxfb9dd4da0977f2e0 wxd4ab5786068547cf a57a0f4b7ef9c0e6344e357b07d5f5ca
// window.addPlugin = function(){
            // var preRound = Laya.loader.getRes("prefab/round.prefab");
            // var itemW = 100,itemH = 80,itemL = 60,itemT = 500;
            // for(var k = 0;k<10;k++){
            //   var item = ct.create();
            //   item.x = (k%5)*itemW+itemL;item.y = (k/5)*itemH+itemT;
            //   self.gameUi.pRounds.addChild(item);
            // }
// }
window.v3len = function(v){return Math.sqrt(v.x*v.x+v.y*v.y+v.z*v.z);}
window.v3sub = function(a,b) {return new Laya.Vector3(a.x-b.x,a.y-b.y,a.z-b.z);}
window.v3n = function(v){var s = v3len(v);if(s<0.0001) return new Laya.Vector3(0,0,0);else return new Laya.Vector3(v.x/s,v.y/s,v.z/s);}
var __proto = Laya.Timer.prototype;
window.mapWorld = function(){
  var p,r;
  if(GameGlobal.BPO==0){
    p = GameGlobal.physicsWorld,r = GameGlobal.rigidBodies;
  }else{
    p = GameGlobal.physicsWorld2,r = GameGlobal.rigidBodies2;
  }
  return [p,r];
}
__proto.dumpHandlers = function(){
  // var s = 0;
  // for(var k = 0;k<this._handlers.length;k++){
  //   var item = this._handlers[k];
  //   if(item.method){
  //     console.log("handlers["+s+"]:"+item.des+" method:"+item.method);
  //     s++;
  //   }
  // }
}
__proto.handlerClear = function(timename){
  if(TimeGlobal[timename]){
    this.clear(null,TimeGlobal[timename]);  
    TimeGlobal[timename] = null;
  }
}
__proto.handlerLoop = function(des,delay,method,time){
  this.handlerClear(des);
  TimeGlobal[des] = method;
  var handler=this._create(false,true,delay,null,method,null,true);
  handler.des = des;
  return handler;
}

__proto.handlerOnce = function(des,delay,method){
  if(method) {
    method.des = des;
    Laya.timer.once(delay,null,method);
  }
}
window.onbeforeunload = function(){
  var litem = localStorage.getItem("battleInfo");
  if(litem){
    var bitem = JSON.parse(litem);
    if(bitem.openId==GGlobal.openId){
      localStorage.removeItem("battleInfo");
    }
  }
  localStorage.removeItem("Inv");
};
window.eftLaterShow = function(target,des,delay){
  target.visible = false;
    Laya.timer.handlerOnce(des,1500,function(){
        target.visible = true;
    });
}
window.eftBtnScale = function(target,des,s,delay){
  var stime = 0;
  var ts = s?s:1.2;
  var td = delay?delay:300;
  Laya.timer.handlerLoop(des,16,function(){
    if(stime==0){
        Laya.Tween.to(target,{scaleX:ts,scaleY:ts},td,Laya.Ease.linearNone,Laya.Handler.create(null,function(){
          Laya.Tween.to(target,{scaleX:1,scaleY:1},td,Laya.Ease.linearNone,Laya.Handler.create(null,function(){
          }));
        }),0,true);
    }
    stime+=16;
    if(stime>td+100){
      stime = 0;
    }
  })
}
window.eftBlpFade = function(target){
  target.meshRenderer.material.renderMode = 2;
  target.meshRenderer.material.albedoColorA = 0.5;
  // console.log("Xgq eftBlpFade BPO:"+GameGlobal.BPO);
  if(GameGlobal.BPO==0){
    physicsWorld = GameGlobal.physicsWorld,rigidBodies = GameGlobal.rigidBodies;
  }else{
    physicsWorld = GameGlobal.physicsWorld2,rigidBodies = GameGlobal.rigidBodies2;
  }

  var bd = {p:physicsWorld,t:target,r:rigidBodies};
  // physicsWorld.removeRigidBody(target.userData.physicsBody);
  // target.userData.physicsBody = null;
  Laya.Tween.to(target.meshRenderer.material,{albedoColorA:0},1500,Laya.Ease.linearNone,Laya.Handler.create(null,
      (function(){
        if(this.t.userData.blpstate=="Down"){
          this.p.removeRigidBody(this.t.userData.physicsBody);
          this.t.userData.physicsBody = null;
        }
      }).bind(bd)
    ));
}

window.eftFlash = function(target,delay){
  return Laya.timer.handlerLoop(delay,function(){
    target.visible = !target.visible;
  });
}

window.eftBtnSkew = function(target,name){
  Laya.timer.handlerLoop("BtnSkew"+name,16,function(){
    
  })
}

window.eftJd = function(sprJd,name,s){
  if(GameGlobal.EftOnce["eftJd"]==true){
    var ts;
    if(!name) {
      Laya.SoundManager.playSound(_s+"huanhu.mp3");
      ts = 1.5;
    }else{ts = s;}
    sprJd.visible = true;sprJd.alpha = 1.0;
    sprJd.scaleX = sprJd.scaleY = 0.5;
    Laya.Tween.to(sprJd,{scaleX:ts,scaleY:ts},300,Laya.Ease.elasticOut,Laya.Handler.create(null,
    function(){
      Laya.Tween.to(sprJd,{alpha:0},GameGlobal.VTime,Laya.Ease.linearNone,null,0);
    }),0);
    GameGlobal.EftOnce["eftJd"] = false;
  }
}
  // var kv = getRankingLevel(GGlobal.maxscoreI);
  // var pay = RankingSM[kv[0]];
  // if(GGlobal.maxscore<pay){
  //   eftFloat("金币不足无法参加排位赛",'#0',600);
  // }else{
    
  // }
window.eftFloat = function(txt,color,py,fontSize){
  if(GameGlobal.EftFloat==true){
    var ui = Laya.stage.getChildAt(Laya.stage.numChildren-1);
    var text = new Laya.Text();
    text.fontSize = fontSize?fontSize:48;text.color = color?color:"#000000";text.align = 'center';text.width = 720;text.text = txt;text.alpha = 0.0;
    ui.addChild(text);text.y = py?py:500;
    Laya.Tween.to(text,{alpha:1.0},300,Laya.Ease.linearNone,Laya.Handler.create(null,
    function(){
      Laya.Tween.to(text,{alpha:0},500,Laya.Ease.linearNone,Laya.Handler.create(null,function(){
        text.removeSelf();
        GameGlobal.EftFloat = true;
      }),0);
    }),0);
    GameGlobal.EftFloat = false;
  }
}
window.animBiaoQin = function(v,bv){
  var iqp = GameGlobal.gameUi["iQiPao"+(v+1)];
  var c = iqp.getChildByName("气泡").getChildByName("表情");
  var bv1 = bv+1;
  Laya.SoundManager.playSound(_s+"n"+bv1+".mp3");
  c.skin = _r+("home/bq"+bv1+".png");c.width = 160;c.height = 160;
  iqp.visible = true;iqp.alpha = 1;
  var ox = iqp.scaleX,oy = iqp.scaleY;
  iqp.scaleX = iqp.scaleY = 0;
  Laya.Tween.to(iqp,{scaleX:ox,scaleY:oy},1000,Laya.Ease.elasticOut,Laya.Handler.create(null,function(){
    Laya.Tween.to(iqp,{scaleX:ox,scaleY:oy},1000,Laya.Ease.linearNone,Laya.Handler.create(null,function(){
      Laya.Tween.to(iqp,{alpha:0},200,Laya.Ease.linearNone,Laya.Handler.create(null,function(){
        iqp.visible = false;
      }));
    }));
  }),0,true);
}
window.getBiaoQin = function(){
  var o = [];
  for(var k = 0;k<GGlobal.OwnBallon.length;k++){
    if(GGlobal.OwnBallon[k]==1){
      o.push(k);
    }
  }
  return o;
}

window.eftFTgs = function(target){
    if(GameGlobal.EftOnce["eftFTgs"]==true){
      target.visible = true;
      target.scaleX = target.scaleY = 0.5;
      Laya.Tween.to(target,{scaleX:1,scaleY:1},300,Laya.Ease.elasticOut,Laya.Handler.create(null,
      function(){
        // Laya.Tween.to(sprJd,{alpha:0},GameGlobal.VTime,Laya.Ease.linearNone,null,0);
      }),0);
      GameGlobal.EftOnce["eftFTgs"] = false;
  }
}
window.battleShare = function(){
    GGlobal.Share("Battle");
    GameGlobal.BattleHomeState = "toready";
    refreshBattleInfo("update");
}
window.refreshGameItemBtn = function(mode){
  if(mode=='hjq'){
    GameGlobal.gameUi.btnYellow.visible = true;
  }else if(mode=='fhq'){
    GameGlobal.gameUi.btnFhq.visible = true;
  }
}
window.refreshBattleInfo = function(mode,sstate){
  if(mode=="init"){
    // if(!sstate){
      GGlobal.playersInfo = {};
      GameGlobal.BattleHomeState = "battle";
      GGlobal.playersInfo[""+GGlobal.openId] = {
        avatar:GGlobal.avatarUrl,name:GGlobal.nickName,order:0,isDead:0,isReady:0,score:0};
    // }else{
    //   var mInfo = GGlobal.CLONE(GGlobal.playersInfo);
    //   GGlobal.playersInfo = {};
    //   GGlobal.playersInfo[""+GGlobal.openId] = mInfo[""+GGlobal.openId];
    // }
    // if(GGlobal.joinBattle!=true){
      refreshBattleInfo("update");
    // }else{
    //   refreshBattleInfo("update");
    // }
  }else if(mode=="update"){
    if(GameGlobal.BattleHomeState!="battlegame"){
      var pInfos = [];var pVs = GameGlobal.homeUi.pBattle.getChildByName("VS");
      var iInfos = [];iInfos.push(pVs.getChildByName("左"));iInfos.push(pVs.getChildByName("右"));
      for(var k = 0;k<2;k++){for(var key in GGlobal.playersInfo){var v = GGlobal.playersInfo[key]; if(v.order==k){pInfos.push(v); } } }
      for(var k = 0;k<2;k++){
        var v = pInfos[k];var vi = iInfos[k];
        var vimg = vi.getChildByName("头像"),vstate = vi.getChildByName("状态"),vname = vi.getChildByName("昵称");
        if(k>=pInfos.length){vimg.visible = false;}else{
          vimg.visible = true;vimg.width = vimg.height = 138;vimg.loadImage(v.avatar);}
        vname.text = k>=pInfos.length?"":v.name;
        if(GameGlobal.BattleHomeState=="battle"||GameGlobal.BattleHomeState=="waitcreateroom")
          vstate.text = "";
        else
          vstate.text = k>=pInfos.length?"":(v.isReady?"已准备":"未准备");
        if(k<pInfos.length&&GameGlobal.BattleHomeState!="battle"){
          if((GGlobal.joinBattle==true&&k==1)||(GGlobal.joinBattle==false&&k==0)){
            if(!v.isReady) GameGlobal.BattleHomeState = "toready";
            else GameGlobal.BattleHomeState = "ready"; 
          }       
        }
      }
    }
    refreshBtnBF();
  }
}
window.refreshBtnBF = function(){
  var mode = GameGlobal.BattleHomeState;
  var bf = GameGlobal.homeUi.btnBF,bt = GameGlobal.homeUi.txtBFTime;
  bt.visible = false;
  bf.visible = true;
  // console.log("Xgq refreshBtnBF:"+mode);
  if(mode=="battle"){
    bf.getChildByName("发起挑战").visible = true;
    bf.getChildByName("准备").visible = false;
  }
  else if(mode=="waitcreateroom"){
    bf.visible = false;
  }
  else if(mode=="toready"){
    bf.getChildByName("发起挑战").visible = false;
    bf.getChildByName("准备").visible = true;
  }else if(mode=="ready"){
    bf.visible = false;
  }else if(mode=="battlegame"){
    bt.visible = true;
  }
}
window.eftRound = function(){
  // return;
  var pRounds = GameGlobal.gameUi.pRounds;var _it = 0;
  Laya.getset(0,pRounds,"it",function(){
    return _it;
  },function(v){
    _it = v;
    if(GameGlobal.gameOver==true){
      pRounds.alpha = 1;
    }else{
      pRounds.alpha = _it;
    }
  })
  pRounds.visible = true; 
  pRounds.alpha = 0;
  Laya.Tween.to(pRounds,{it:1.0},100,Laya.Ease.linearNone,Laya.Handler.create(null,
    function(){
      Laya.Tween.to(pRounds,{it:1.0},GameGlobal.VTime,Laya.Ease.linearNone,Laya.Handler.create(null,function(){
        Laya.Tween.to(pRounds,{it:0.0},200,Laya.Ease.linearNone,Laya.Handler.create(null,function(){
        }));
      }));
    }),
  0);
}
window.clearGameOnce = function(){
  GameGlobal.Once[GameGlobal.matchMeOrder] = true;
  for(var k in GameGlobal.EftOnce){
    GameGlobal.EftOnce[k] = true;
  }
}
window.getRankingLevel = function(v,vs){
  var kv = 0;
  for(var k = 0;k<vs.length;k++){
    kv+=vs[k];
    if(kv>v){  return [k,kv,kv-v];}
  }
  return [vs.length,0,0];
}
window.Vector3 = Laya.Vector3; 
var Gs = 5; 
var GameGlobal = {
  hc:0,
  UpdatePhyFlag:false,
  initInputOnce:true,
  gravityConstant:78,
  Gs:Gs,
  FullBD:10,
  MaxLevelTimes:2,
  TotalLevel:10,
  GCreateBlp:[1,1],
  Hjp:-1,
  HjpShowOnce:false,
  HjgGet:false,
  BlpBombOnce:true,
  OPLeave:false,
  Once:[false,false],
  camera:null,scene:null,gblp:null,
  VTime:800,
  FirstLoading:true,
  
  EftOnce:{"eftJd":true,"eftRound:":true,"eftFTgs":true,"dbg":true},
  EftFloat:true,
  GDataState:0,
  GameAddBlq:[0,0],
  GameAddBlqState:['',''],
  GameAddBlqOnce:[true,true],
  RoundsAHit:[[],[]],
  RoundsBHit:[[],[]],
  BpData:[[],[]],
  PEnds:[],
  RoundsData:[[],[]],
  GameScore:[0,0],
  GameScorePre:[0,0],
  gameTimes:[0,0],
  LevelTimes:[2,2],
  SaveData:[{},{}],
  gameLevel:[0,0],
  LeaveGame:[0,0],
  itemsUsed:[{fhq:0,hjq:0},{fhq:0,hjq:0}],
  useYellowBall:[false,false],
  FhqDisState:[false,false],
  GameLevelScore:[[],[]],
  BattleHomeState:'battle',
  RedTime:[null,null,null],
  PollBall:false,
  ballDataRecOnce:true,
  dCount:0,
  BOY:1000,
  BPO:0,
  F0Count:0,
  thinking:false,
  battlePack:0,
  ballTouchStart:0,
  SBOX:2000,
  InGame:false,
  
  GConfig: (function(){
    return {
      bmin:3,
      bmaxvx:2000,
      cx: 0, cy: 2.0*Gs, cz: 4.2*Gs,cf:60,
      crx:20*Math.PI/180,
      burl:'http://192.168.1.185/blp.obj',
      gpy:0.84,
      gx: 1.8*Gs, gy:2*Gs,gz:31*Gs,gf:0.2,
      gcx:0.5*Gs,gcy:0.05*Gs,
      gbx:0.1*Gs,gby:1.0*Gs,
      br:0.168*Gs,bm:1*Gs,blf:1,brf:0.5,
      // var s = (vlen*GConfig.Skew1)/(GConfig.Skew2+vlen*vlen*GConfig.Skew3);
      bfF:35,bmax:70,bscale:50,bf:150*Gs,Skew1:1000,Skew2:15,Skew3:0.5,IBRect:{w:4,t:2,d:14.8},
      blpf:0.5,blprf:0.2,
      blpm:0.5*Gs,
      blpy:0.2, blpsx:0*Gs,blpw:0.48*Gs,blpz:15*Gs-8,blph:0.41568*Gs,
      gmachine:true,
      gmachinetime:100000,
      gmachineleft:true,
      Gs:Gs
    }
  }).bind(GameGlobal)(),
  margin:0.01,
  rigidBodies:[],
  rigidBodies2:[],
  dbgBattle:0,
  battleSendOnce:true,
  battleTurnThinking:false,
  loadingres:[
  _s+"bj5.mp3",
  _r+"sky_baiyun.jpg",
  "res/startline.png",
  _r+"cj_01.jpg",
  _s+"bd.mp3",
  _s+"open.mp3",
  _s+"lose.mp3",
  _s+"item.mp3",
  _s+"chou2.mp3",
  _s+"gd.mp3",
  _s+"gameitem.mp3",
  _s+"huanhu.mp3",
  _s+"jinbi.mp3",
  _s+"pz.mp3",
  _r+"cj_01.lm",
  "res/sky-sky.lm",
  "res/nq1.png",
  "res/nq2.png",
  "res/nq3.png",
  "res/nq4.png",
  _r+"cj_02.jpg",
  "res/qiu-blq_01.lm","prefab/局.prefab",
  "prefab/受邀玩家.prefab","prefab/邀选项.prefab",
  "prefab/抽奖项.prefab","prefab/签到天数.prefab","prefab/冠军项.prefab",
  "prefab/金币.prefab","prefab/聊天表情.prefab","prefab/星星.prefab","prefab/游戏聊天表情.prefab",
  "res/blpms_m-blp_01.lm","res/blps-Box001.lm",
  "res/blq_02.jpg","res/blq_01.jpg","res/blp.jpg","res/blp_01.jpg",
  _r+"xuedi-cao.lm",
  ],
  globalres:{},
  LastLevelAllHit:['',''],
  UserInfoDone:false,
  IsDbg:0,
  BtnSkew:{},
  NoTp:1,
  ResetVar:function(){
    this.MBiaoQin = false;
    this.GameStop = false;
    this.OPLeave = false;
    this.JLQUseItem = {hjq:1,fhq:1};
    this.LeaveGame = [0,0];
    this.InGame = false;
    this.PEnds = [false,false];
    this.BattleHomeState = 'battlegame';
    this.UpdatePhyFlag = false;
    this.Once = [false,false];
    this.FhqDisState = [false,false];
    this.BPO = this.F0Count = this.GDataState = 0;
    this.EftFloat = true;
    this.BpData = [[],[]];
    this.GCreateBlp = [1,1];
    this.rigidBodies = [];
    this.rigidBodies2 = [];
    this.RoundsData = [[],[]];
    this.HjgGet = false;
    this.ball = null;
    this.ballinput = null;this.gameOver = false;
    this.dCount = 0;
    this.cshootball = true;
    this.thinking = false;
    this.bmovepos = [];
    this.battleCanFinish = false;
    this.battleSendOnce = true;
    this.battleTurnThinking = false;
    this.nBlpDownCount = 0;this.gdp = null;
    this.BlpBombOnce = true;
    this.Hjp = -1;
    this.HjpShowOnce = false;
    this.PollBall = false;
    for(var k = 0;k<2;k++){
      this.GameScore[k] = 0;
      this.useYellowBall[k] = false;
      this.GameScorePre[k] = 0;
      this.LastLevelAllHit[k] = '';
      this.GameAddBlq[k] = 0;
      this.GameAddBlqState[k] = '';
      this.GameAddBlqOnce[k] = true;
      this.RoundsAHit[k] = []; 
      this.RoundsBHit[k] = []; 
      this.gameLevel[k] = -1;
      this.gameTimes[k] = this.LevelTimes[k] = this.MaxLevelTimes;
      this.itemsUsed = [{fhq:0,hjq:0},{fhq:0,hjq:0}]
      this.GameLevelScore[k] = [];
      for(var j = 0;j<20;j++){
        this.GameLevelScore[k].push(-1);
      }
      //GameGlobal.Rounds
    }
    this.gameLevel[0] = 0;
    this.gameLevel[1] = 0;//tp
    this.WaitBattleOnce = true;
    if(this.gameMode==0){
      this.PEnds[1] = true;
    }
  },
  collisionConfiguration:null,dispatcher:null,broadphase:null,solver:null,physicsWorld:null,
  transformAux1:null,tempBtVec3_1:null,
  gdp:null,
  cshootball:true,
  ballinput:null,
  ball:null,
  GMEnd:false,
  mouseCoords:new Laya.Vector2(),bmovepos:[],stime:null,etime:null,blv:0,
  nTime:0,pTime:0,cFollow:0,bPrez:0,gry:0,nBlpDownCount:0,
  SItems:[
    {m:1000},
    {jlq:5},
    {m:1500},
    {hjq:5},
    {m:2500},
    {fhq:5},
    {jlq:10}
  ],
  CItems:[
    {hb:"100元"},
    {jlq:"5钻石"},
    {fhq:"5张"},
    {m:1000},
    {hjq:"5个"},
    {m:500},
    {fhq:"5张"},
    {jlq:"5钻石"},
    {hb:"50元"},
    {m:2500},
    {hjq:"5个"},
    {m:300}
  ],
  BiaoQinImgs:[
  "哭","舌头","怒","喜","笑",
  "闭嘴","单眼眨","恶魔","流汗","墨镜",
  "难受","生病","随便","无奈","晕"],
  RankingLS:[1,2,3,3,4,4,4,5,5,5,6,6,7],
  RankingSM:[100,100,200,200,300,300,400,500,600,750,1000,1250,1500,1500],
  RankingRM:[200,200,400,400,500,500,700,700,1000,1200,1600,2000,2500,2500],
  RankingTx:["入门新手","起步熟手","坚韧黑铁","顽强青铜","傲气白银","荣耀黄金","巧思白金","睿智黑金","尊贵铂金","永恒钻石","光荣史诗","体坛传奇","最强王者"],
  glvsn:["初出茅庐","小区冠军","街道冠军","乡镇冠军","市冠军","省冠军","全国冠军","亚运会冠军","世锦赛冠军","世界杯冠军","奥运冠军","大满贯"],
  glvsd:[
        ["{0}局达到{1}分即可通关","{0}分",
          [3,15,{m:100,d:0},25,{m:200,d:0},35,{m:400,d:1}],
          [3,25,{m:100,d:0},35,{m:200,d:0},50,{m:400,d:1}],
          [4,50,{m:100,d:0},60,{m:200,d:0},70,{m:400,d:1}],
          [4,65,{m:100,d:0},75,{m:200,d:0},85,{m:400,d:1}],
          [5,75,{m:100,d:0},85,{m:200,d:0},105,{m:400,d:5}]
        ],
        ["{0}球击中{1}瓶子即可通关","{0}瓶"
          [3,15,{m:100,d:0},20,{m:200,d:0},25,{m:400,d:1}],
          [3,18,{m:100,d:0},25,{m:200,d:0},28,{m:400,d:1}],
          [4,20,{m:100,d:0},22,{m:200,d:0},30,{m:400,d:1}],
          [4,25,{m:100,d:0},30,{m:200,d:0},35,{m:400,d:1}],
          [5,30,{m:100,d:0},37,{m:200,d:0},46,{m:400,d:5}]
        ]
      ],
  glvsl:[5,5,8,8,8,8,8,8,8,8,8,8],
  SpItems:{
  },
  BtnClick:{}
}
for(var k = 1;k<=15;k++){
  GameGlobal.loadingres.push(_r+"tex/blq"+(k)+".jpg");
  GameGlobal.loadingres.push(_s+"n"+(k)+".mp3");
}

window.setBodyFriction = function(ballBody){
  var GConfig = GameGlobal.GConfig;
  ballBody.setFriction(GConfig.blf); ballBody.setRollingFriction(GConfig.brf);
  ballBody.setSpinningFriction(0.1);
  ballBody.setAnisotropicFriction(new Ammo.btVector3(1,1,1),2);
}

window.sceneAddBall = function(scene,pose){
  var ball = scene.addChild(new Laya.MeshSprite3D(GameGlobal.globalres["res/qiu-blq_01.lm"]));
  var ballhelp = scene.addChild(new Laya.MeshSprite3D());

  Laya.Sprite3D.load("res/trail/Trail.lh", Laya.Handler.create(null, function(sprite){
    GameGlobal.TrailObj = sprite;
    ballhelp.addChild(sprite);
  }));

  ball.meshRenderer.castShadow = true;
  ball.transform.translate(new Laya.Vector3(pose.x,pose.y,pose.z));
  ball.transform.name = "ball";
  var materialc = new Laya.BlinnPhongMaterial();
  materialc.albedoIntensity = 5;
  ball.meshRenderer.material = materialc;
  GameGlobal.ball = ball;
  GameGlobal.ballhelp = ballhelp;
  return ball;
}

window.clearGameFhqLoop = function(){
  Laya.timer.handlerClear("GameLoop");GameGlobal.gameUi.txtFhqTime.text="";
}
window.infoGetItem = function(scene,mode,vl){
  GameGlobal.ChouNoGetItem = false;

  if(mode=="sign"||mode=="chou"||mode=="noitem"){
    scene.pMask1.visible = false;
    scene.pMask2.visible = true;
    scene.btnChouM.disabled = false;
    if(mode=="noitem"){
      scene.pGetNoItem.visible = true;
      Laya.timer.handlerOnce("NoItem",500,function(){
        scene.pGetNoItem.visible = scene.pMask1.visible = scene.pMask2.visible = false;
        scene.pMask1.visible = true;
        if(GameGlobal.chouMode=="free"){
          GGlobal.sgame.chou = 1;
        }
      })
      // GameGlobal.ChouNoGetItem = true;
      return;
    }
  }else if(mode=="game"){
    scene.pMask.visible = true;
  }
  scene.pGetItem.visible = true;
  var sitem;
  GameGlobal.GetItemMode = mode;
  if(mode=="sign"||mode=="chou"){
    if(mode=="sign"){
      sitem = GameGlobal.SItems[GGlobal.signCount-1];
    }else if(mode=="chou"){
      sitem = GameGlobal.CItems[vl];
      GameGlobal.GetItemV = vl;
    }
    for(var sk in sitem)
    {
      var t = sk,v = sitem[sk];
      var icon = scene.pGetItem.getChildByName("物品");
      icon.skin = t=="m"?"game/金币图标.png":t=="fhq"?"game/反悔券.png":t=="jlq"?"home/钻石.png":"game/黄金球.png";
      icon.width = icon.height = 120;
      var des = scene.pGetItem.getChildByName("描述");
      var num = scene.pGetItem.getChildByName("数量");num.text = v;
      if(t=="m"){des.text = "";}
      if(t=="fhq"){
        des.text = "可让玩家反悔失败的投球，提升分数";
      }else if(t=="hjq"){
        des.text = "体型、威力变大  提升“全中”概率";
      }else if(t=="jlq"){
        des.text = "可用于兑换游戏所需的道具";
      }
    }
  }else if(mode=="game"){
    var icon = scene.pGetItem.getChildByName("物品");var des = scene.pGetItem.getChildByName("描述");
    icon.skin = "game/黄金球.png";
    des.text = "体型、威力变大  提升“全中”概率";
    icon.width = icon.height = 120;
    GameGlobal.gameUi.btnItemNoGet.visible = false;
    if(GGlobal.gameConfig[0]!=0){
      Laya.timer.handlerOnce("btnItemNoGet",1500,function(){
        GameGlobal.gameUi.btnItemNoGet.visible = true;
      })
    }
  }
}

window.turnNextFn = function(){
  blqScene3DChange();
  netTurnPlayer();
  resetOneBall();
  clearGameFhqLoop();
}
window.fingerOp = function(){
  GameGlobal.turnMe = true;
  if(GameGlobal.gameOver==false&&(GameGlobal.gameMode==0||(GameGlobal.gameMode==1&&GameGlobal.turnMe==true))){
    blqStartFinger();
  }else{
    GameGlobal.gameUi.sprFinger.visible = false;
  }
}
window.endOneBall = function(){
  var turnNext = false;
  var slevel = true;
  var pl = GameGlobal.matchMeOrder;
  var bps = GameGlobal.BpData[pl];
  var ld = bps[bps.length-1];
  GameGlobal.BpData[pl] = bps.slice(0,bps.length-1);
  var lmap = processLData(pl,ld);
  if(lmap.rbp==1) recreateBlps();
  if(lmap.ed==1) {
    // console.log("Xgq 1 set PEnds[1]  true");
    GameGlobal.PEnds[pl] = true;
    netGameLData([3]);
  }
  else{
    var nmap = processLDataMap(pl,0);
    GameGlobal.gameLevel[pl] = nmap.nlevel;
    fingerOp();
  }
  refreshGameItemBtn('hjq');
  refreshGameItemBtn('fhq');
  resetOneBall();
  refreshItemScore();
}
window.loadingEnd = function(){
  var s = GameGlobal.globalres["res/blps-Box001.lm"]._getPositions();
  GameGlobal.GConfig.blpshape = (function(){
  var shape = new Ammo.btConvexHullShape();
          var scale = 1;    
          for (var i = 0, il = s.length; i < il; i ++) {
            tempBtVec3_1.setValue(s[i].x* scale*0.9, s[i].y* scale, s[i].z* scale*0.9);
            var lastOne = (i >= (il - 3));
            shape.addPoint(tempBtVec3_1, lastOne);
          }
        return shape;
      })()
      var stime = 0;
      GameGlobal.loadingLoop = Laya.timer.handlerLoop("loadingLoop",20,function(){
        stime+=20;
        GameGlobal.loadingUi.barLoading.width+=2;
  })
}

window.LoadingRes = function(){
  GameGlobal.transformAux1 = new Ammo.btTransform();
  GameGlobal.tempBtVec3_1 = new Ammo.btVector3(0, 0, 0);

  function LoadRes(ls){
    var tempBtVec3_1 = GameGlobal.tempBtVec3_1;
    if(ls.length==0) {
      var s = GameGlobal.globalres["res/blps-Box001.lm"]._getPositions();
      GameGlobal.GConfig.blpshape = (function(){
        var shape = new Ammo.btConvexHullShape();
        var scale = 1;    
            for (var i = 0, il = s.length; i < il; i ++) {
              tempBtVec3_1.setValue(s[i].x* scale*0.9, s[i].y* scale, s[i].z* scale*0.9);
              var lastOne = (i >= (il - 3));
              shape.addPoint(tempBtVec3_1, lastOne);
            }
        return shape;
      })()
      var stime = 0;
      GameGlobal.loadingLoop =  Laya.timer.handlerLoop("loadingLoop",20,function(){
        stime+=20;
        GameGlobal.loadingUi.barLoading.width+=5;
        if(GameGlobal.loadingUi.barLoading.width>=450) {
          Laya.timer.handlerClear("loadingLoop");
          GameGlobal.loadingUi.btnSGame.visible = true;
          GameGlobal.loadingUi.iLoadingBar.visible = false;
        }
      });
    }
    else {
      var item = ls[0];
      var loader;
      var loadfun;
      if(item.indexOf(".lm")!=-1){
        loader = Laya.Mesh;loadfun = loader.load;
      }else if(item.indexOf(".jpg")!=-1||item.indexOf(".png")!=-1){
        loader = Laya.Texture2D;loadfun = loader.load;
      }else if(item.indexOf(".mp3")!=-1||item.indexOf(".wav")!=-1){
        loadfun = function(url,complete){
          Laya.loader.create(url,complete,null,/*Laya3D.MESH*/"sound");
        }
      }else if(item.indexOf(".prefab")){
        loadfun = function(url,complete){
          Laya.loader.create(url,complete,null,/*Laya3D.MESH*/"prefab");
        }
      }
      loadfun(item,
        Laya.Handler.create(null,function(res){
          GameGlobal.globalres[item] = res;
          ls = ls.splice(1,ls.length-1);
          GameGlobal.loadingUi.barLoading.width = 150*(GameGlobal.loadingres.length-ls.length)/GameGlobal.loadingres.length;
          LoadRes(ls);
        })
      );
    }
  }
  var tmploading = []; for(var k = 0;k<GameGlobal.loadingres.length;k++){tmploading.push(GameGlobal.loadingres[k]); }
  var lrescount = 0;
  LoadRes(tmploading);
}
GameGlobal.plane = new Laya.Plane(new Laya.Vector3(0,1,0),-GameGlobal.GConfig.br*1.1);
window.GameGlobal = GameGlobal;
