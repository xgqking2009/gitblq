// https://ldc.layabox.com/doc/?nav=zh-as-2-2-3
  window.createBlp = function(object,mass,pos,quat,material){
      var physicsWorld,rigidBodies;
      if(GameGlobal.BPO==0){
        physicsWorld = GameGlobal.physicsWorld,rigidBodies = GameGlobal.rigidBodies;
      }
      else{
        physicsWorld = GameGlobal.physicsWorld2,rigidBodies = GameGlobal.rigidBodies2;
      }
      var GConfig = GameGlobal.GConfig;
      object.userData = {};
      object.transform.position = new Vector3(pos.x,pos.y,pos.z);
      object.name = "Blp";
      object.meshRenderer.castShadow = true;
      var shape = GConfig.blpshape;
      var transform = new Ammo.btTransform();
      transform.setIdentity();
      var mass = GConfig.blpm;
      transform.setOrigin(new Ammo.btVector3(pos.x, pos.y, pos.z));
      var localInertia = new Ammo.btVector3(0, 0, 0);
      shape.calculateLocalInertia(mass, localInertia);
      localInertia = new Ammo.btVector3(localInertia.x(), localInertia.y(), localInertia.z());
      var motionState = new Ammo.btDefaultMotionState(transform);
      var rbInfo = new Ammo.btRigidBodyConstructionInfo(mass, motionState, shape, localInertia);
      var body = new Ammo.btRigidBody(rbInfo);
      physicsWorld.addRigidBody(body);
      body.name = "blp";
      object.userData.physicsBody = body;
      object.userData.blpstate = "Stand";
      object.userData.originpos = new Vector3(pos.x,pos.y,pos.z);
      object.userData.mass = mass;
      object.lact = true;
      object.userData.localInertia = new Vector3(localInertia.x(),localInertia.y(),localInertia.z());

      body.setFriction(GConfig.blpf);
      body.setRollingFriction(GConfig.blprf);
      if (mass > 0) {
        rigidBodies.push(object);
        body.setActivationState(4);
      }
      var btVecUserData = new Ammo.btVector3(pos.x, pos.y, pos.z);
      btVecUserData.threeObject = object;
      body.setUserPointer(btVecUserData);
    }
window.recreateBlps = function(){
  GameGlobal.nBlpDownCount = 0;
  var rigidBodies,physicsWorld;
  if(GameGlobal.GCreateBlp[GameGlobal.BPO]==0) return;
  if(GameGlobal.BPO==0){
      rigidBodies = GameGlobal.rigidBodies,physicsWorld = GameGlobal.physicsWorld;
  }else
  {
    rigidBodies = GameGlobal.rigidBodies2,physicsWorld = GameGlobal.physicsWorld2;
  }
  // console.log("GameGlobal.BPO recreateBlps:"+GameGlobal.BPO)
  var GConfig = GameGlobal.GConfig;
  var nr = - 1;
  if(GameGlobal.gameMode==0&&GameGlobal.HjpShowOnce==true)
  {
    nr = Math.floor(Math.random()*10);
    GameGlobal.HjpShowOnce = false;
    var rr = Math.random();
    if(rr<0.5){
      GameGlobal.EftOnce["eftJd"] = true;
      eftJd(GameGlobal.gameUi.sprHjp,"HJP",1);
    }else{
      nr = -1;
    }
  }
  GameGlobal.Hjp = nr;

  var rdr = -1;
  
  for(var i = 0;i<rigidBodies.length;i++){
    var item = rigidBodies[i];
    if(item.userData.blpstate){
      var opos = item.userData.originpos,body = item.userData.physicsBody;
      var ol = item.userData.localInertia;
      item.userData.blpstate = "Stand";
      if(item.meshRenderer.material){
        item.meshRenderer.material.renderMode = 0;
      }
      var tname;if(i==rdr){tname = _r+"tex/blq5.jpg"; }else{tname = i==nr?"res/blp_01.jpg":"res/blp.jpg"; }

      item.meshRenderer.material.albedoTexture = GameGlobal.globalres[tname];
      item.transform.position = new Vector3(opos.x,opos.y,opos.z);
      item.transform.rotation = new Laya.Quaternion(0,0,0,1);
      var transform = new Ammo.btTransform();transform.setIdentity();
      transform.setOrigin(new Ammo.btVector3(opos.x, opos.y, opos.z));
      if(body) {
        physicsWorld.removeRigidBody(body);
      }
      var motionState = new Ammo.btDefaultMotionState(transform);
      var localInertia = new Ammo.btVector3(ol.x, ol.y, ol.z);
      var rbInfo = new Ammo.btRigidBodyConstructionInfo(item.userData.mass, motionState, GConfig.blpshape, localInertia);
      var body = new Ammo.btRigidBody(rbInfo);
      body.name = "blp";
      physicsWorld.addRigidBody(body);
      item.userData.physicsBody = body;
      body.setActivationState(4);
    }
  }
  GameGlobal.GCreateBlp[GameGlobal.BPO] = 0;
  GameGlobal.updatePhy = false;
}

window.createRigidBody = function(object, physicsShape, mass, pos, quat, vel, angVel) {
  var physicsWorld,rigidBodies;
  if(GameGlobal.BPO == 0){
    physicsWorld = GameGlobal.physicsWorld,rigidBodies = GameGlobal.rigidBodies;
  }
  else{
    physicsWorld = GameGlobal.physicsWorld2,rigidBodies = GameGlobal.rigidBodies2;
  }
  
  var GConfig = GameGlobal.GConfig;

  object.userData = {};
  var transform = new Ammo.btTransform();
  transform.setIdentity();
  transform.setOrigin(new Ammo.btVector3(pos.x, pos.y, pos.z));
  transform.setRotation(new Ammo.btQuaternion(0, 0, 0, 1));
  var motionState = new Ammo.btDefaultMotionState(transform);
  var localInertia = new Ammo.btVector3(0, 0, 0);
  physicsShape.calculateLocalInertia(mass, localInertia);

  var rbInfo = new Ammo.btRigidBodyConstructionInfo(mass, motionState, physicsShape, localInertia);
  var body = new Ammo.btRigidBody(rbInfo);
  body.setFriction(GConfig.gf);
  if (vel) {
    body.setLinearVelocity(new Ammo.btVector3(vel.x, vel.y, vel.z));
  }
  if (angVel) {
    body.setAngularVelocity(new Ammo.btVector3(angVel.x, angVel.y, angVel.z));
  }
  object.userData.physicsBody = body;
  object.userData.collided = false;
  // scene.add(object);
  if (mass > 0) {
    object.lact = true;
    object.name = "ball_"+GameGlobal.BPO;
    if(rigidBodies.indexOf(object)==-1)
      rigidBodies.push(object);
    body.setActivationState(4);
  }else{ 
    object.name = "ground";
    object.active = false;
    // body.setFriction(0.2);
  }
  body.name = object.name;
  // console.log("Xgq BPO:"+GameGlobal.BPO+" addRigidBody "+body.name);
  physicsWorld.addRigidBody(body);
  return body;
}
