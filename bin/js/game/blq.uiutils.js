window.refreshMSNum = function(mode,late){
  if(mode=="game"){
    var scene = GameGlobal.gameUi;
    scene.txtFhq.text = GGlobal.sgame.fhq;
    scene.txtHjq.text = GGlobal.sgame.hjq;
    scene.txtJsS1.text = scene.txtJsS.text = GGlobal.maxscoreI;
    scene.txtJsM1.text = scene.txtJsM.text = GGlobal.maxscore;
    var idx = GameGlobal.playerOrder?GameGlobal.playerOrder:0;
    scene.txtHjqTimes.text = (3-GameGlobal.itemsUsed[idx].hjq)+"/3";
    scene.txtFhqTimes.text = (3-GameGlobal.itemsUsed[idx].fhq)+"/3";
  }else{
    var scene = GameGlobal.homeUi;
    scene.txtMNum.text = GGlobal.maxscore;
    scene.txtSNum.text = GGlobal.maxscoreI;
    if(!GGlobal.sgame) return;
    scene.txtFhq.text = GGlobal.sgame.fhq;
    scene.txtHjq.text = GGlobal.sgame.hjq;
    scene.txtJLNum.text = GGlobal.sgame.jlq;
  }
}
window.refreshSign = function(){
  for(var k = 0;k<7;k++){
    if(k<GGlobal.signCount){
      GameGlobal.Days[k].getChildByName("勾").visible = true;
    }else{
      GameGlobal.Days[k].getChildByName("勾").visible = false;
    }
  }
}
window.refreshChou = function(){
  var scene = GameGlobal.homeUi;
  if(GameGlobal.GetItemMode=="chou"){
    if(GGlobal.sgame.chou==1){
      scene.btnChouM.visible = true;
      scene.btnChouFree.visible = false;
    }
  }
}
window.loadSceneLoaded = function(scene){
  // if(GGlobal.SRatio<2.0) GGlobal.SRatio = null;
  
  GameGlobal.loadingUi = scene;
  GameGlobal.loadingUi.iLoadingBar.visible = true;
  adapterpx("loading");
  Laya.SoundManager.stopAll();
  var stime = 0;
  scene.btnSGame.visible = false;
  function loginBack(){
    refreshHomeLevel();
    Laya.SoundManager.setMusicVolume(0.5);
    Laya.SoundManager.playMusic(_s+"bj5.mp3",-1);
    Laya.timer.handlerClear("loadingCircle");
      GameGlobal.homeUi.visible = true;
      if(GGlobal.joinBattle){
        blqInitHome(GameGlobal.homeUi,5);
        GGlobal.WssSocketOpen(GGlobal.WSSURL,
          wssOnMsg.bind(this),function(){
          var data = {command:"room", action:"join", openId:GGlobal.openId, name:GGlobal.nickName, avatar:GGlobal.avatarUrl, roomId:GGlobal.roomId };
          GameGlobal.BattleHomeState = "toready";
          // console.log("Xgq send data::"+JSON.stringify(data));
          GGlobal.WssSocketSend(JSON.stringify(data));
      });
      }else
      blqInitHome(GameGlobal.homeUi);
  }
  LoadingRes();
  GGlobal.WechatGameLogin(loginBack)
  Laya.Scene.open("test/home.scene",false,Laya.Handler.create(null,function(hscene){
      GameGlobal.homeUi = hscene;
      GameGlobal.homeUi.visible = false;
      // scene.btnSGame.visible = true;
    })
  )
  console.log("Xgq scene.btnSGame 11222");
  scene.btnSGame.setOnClick(function(){
    if(!wx.login){
      if(GGlobal.InitSpec1000){
        for(var k = 0;k<1000;k++)
          getUserInfo("123456",null,null);
      }
      else getUserInfo("123456",null,loginBack);
    }
  })
  scene.btnTestSocket.setOnClick(function(){
    GameGlobal.homeUi.visible = true;
    blqInitHome(GameGlobal.homeUi);
    if(GGlobal.button){
      GGlobal.button.hide();
    }
    // window.SDK.share(
    //   {
    //     title:"分享功能",
    //     imageUrl:GGlobal.ShareUrl,
    //     query:{
    //       openId:GGlobal.openId
    //     }
    //   }
    // )
  })
}
window.refreshInvList = function(){
  // iInvUs
  var name,url,s;
  if(!GGlobal.invlist) return;
  for(var k = 0;k<4;k++){
    var item = GameGlobal.iInvDs[k];
    var uitem = GameGlobal.iInvUs[k];
    // GGlobal.invlist.length/3&&k<GameGlobal.iInvDs.length
    name = url = s = null;
    if(k<GGlobal.invlist.length/3&&k<GameGlobal.iInvDs.length){
      url = GGlobal.invlist[k*3];
      if(url&&url.replace) url = url.replace(' ', '');
      name = GGlobal.invlist[k*3+1];
      s = GGlobal.invlist[k*3+2];
    }
    else{
      name = "好友"+(k+1);
    }
    item.getChildByName("名字").text = name;
    if(url) item.getChildByName("玩家").loadImage(url);
    if(s){
      item.getChildByName("领取").visible = s==0;
      item.getChildByName("已领取").visible = s!=0;
      uitem.getChildByName("蒙版").visible = false;
      uitem.getChildByName("锁").visible = false;
      uitem.getChildByName("图标").visible = true;
    }else{
      item.getChildByName("领取").visible = item.getChildByName("已领取").visible = false;
      uitem.getChildByName("蒙版").visible = true;
      uitem.getChildByName("锁").visible = true;
      // uitem.getChildByName("图标").visible = false;
    }
  }

  GameGlobal.homeUi.btnInv.visible = GGlobal.invlist.length!=12;
}
window.bindItemGet = function(arg0){  
  var scene;
  if(GameGlobal.UiMode=="home") scene = GameGlobal.homeUi;
  else scene = GameGlobal.gameUi;
  var sitem = {'hjq':1};
  if(GameGlobal.GetItemMode=="sign") sitem = GameGlobal.SItems[GGlobal.signCount-1];
  else if(GameGlobal.GetItemMode=="chou") sitem = GameGlobal.CItems[GameGlobal.GetItemV];
  for(var sk in sitem){
    if(sk=="m"){
      GGlobal.maxscore+=sitem[sk]*(arg0==2?2:1);
    }else if(sk=="fhq"){
      GGlobal.sgame.fhq+=5*(arg0==2?2:1);
    }else if(sk=="hjq"){
      GGlobal.sgame.hjq+=5*(arg0==2?2:1);
    }else if(sk=="jlq"){
      GGlobal.sgame.jlq+=5*(arg0==2?2:1);
    }
  }
  if(GameGlobal.GetItemMode=="sign") GGlobal.isSignIn = 1;
  else if(GameGlobal.GetItemMode=="chou"){
    if(GameGlobal.chouMode == "free"){  GGlobal.sgame.chou = 1;}
  } 
  if(GameGlobal.GetItemMode=="sign"||GameGlobal.GetItemMode=="chou"){
    //GameConfigToDo
    function GetItem(){
      Laya.SoundManager.playSound(_s+"item.mp3");
      scene.pGetItem.visible = scene.pMask1.visible = scene.pMask2.visible = false;
      scene.pMask1.visible = true;
      refreshMSNum();
      refreshSign();
      refreshChou();
    }
    if(arg0==2){
      if(GGlobal.gameConfig[0]==0){
        GetItem();
      }else{
        //GGlobal.showAdvertisement(GameGlobal.GetItemMode,GetItem);
        GGlobal.Share("签到抽奖",GetItem);
      }
    }else{
      GetItem();
    }
  }else if(GameGlobal.GetItemMode=="game"){
    function GetGameHjp(){
      scene.pGetItem.visible = false;
      scene.pMask.visible = false;
      refreshMSNum("game");
      if(GameGlobal.FnNextStep) GameGlobal.FnNextStep();
      GameGlobal.FnNextStep = null;
    }
    if(GGlobal.gameConfig[0]==0){
      GetGameHjp();
    }else{
      GGlobal.Share("游戏中得到黄金屏",GetGameHjp);
    }
  }
  GGlobal.SaveItem(null,null,true);
}

window.BqOn = function(){
    GameGlobal.gameUi.btnBiaoQin.getChildByName("表情").visible = false;
    GameGlobal.gameUi.btnBiaoQin.getChildByName("关闭").visible = true;
    var c = GameGlobal.gameUi.pBiaoQinC;
    var bqs = getBiaoQin();
    var bqh1 = 110;
    var bqnh = Math.floor((bqs.length-1)/5)+1;
    var bqh = bqnh*bqh1,bqw = 548,bqnw = 105,bqsx = 60,bqsy = 56,bqiw = 95,bqih = 95;
    GameGlobal.gameUi.pBiaoQinC.height = bqh;
    var preRound = Laya.loader.getRes("prefab/游戏聊天表情.prefab");
    GameGlobal.gameUi.pBiaoQinC.removeChildren();
    var imgs = GameGlobal.BiaoQinImgs;
    for(var k = 0;k<bqs.length;k++){
      var item = preRound.create();
      GameGlobal.gameUi.pBiaoQinC.addChild(item);
      item.x = Math.floor(k%5)*bqnw+bqsx;
      item.y = bqsy+Math.floor(k/5)*bqh1;
      item.width = bqiw;item.height = bqih;
      var mc = item.getChildByName("图标");
      mc.skin = _r+"home/bq"+(bqs[k]+1)+".png";
      item.setOnClick((function(){
        if(GGlobal.maxscore<5){
          eftFloat("金币不足","#0",600,40);
        }else{
          eftFloat("消耗5金币","#0",600,40);
          GGlobal.maxscore-=5;GGlobal.SaveItem(null,null,true);
          animBiaoQin(GameGlobal.matchMeOrder,bqs[this]);
          c.visible = false;
          netGameLData([2,bqs[this]]);
        }
        BqOff();
      }).bind(k))
    }
    c.visible = true;c.scaleX = c.scaleY = 0.1;
    Laya.Tween.to(c,{scaleX:1,scaleY:1},500,Laya.Ease.elasticOut,Laya.Handler.create(null,function(){
    }));
    GameGlobal.ShowBq = 0;
}

window.BqOff = function(){
  GameGlobal.gameUi.btnBiaoQin.getChildByName("表情").visible = true;
  GameGlobal.gameUi.btnBiaoQin.getChildByName("关闭").visible = false;
  GameGlobal.gameUi.pBiaoQinC.removeChildren();
  GameGlobal.gameUi.pBiaoQinC.visible = false;
  GameGlobal.ShowBq = 1;
}