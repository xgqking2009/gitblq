window.updatePhysics = function(deltaTime) {
    // console.log("Xgq[restore] updatePhysics start");
    // if(GameGlobal.ShootBallFunc){ GameGlobal.ShootBallFunc();GameGlobal.ShootBallFunc = null;}
    // if(GameGlobal.ClickFhq){ GameGlobal.ClickFhq();GameGlobal.ClickFhq = null;}

    var camera,physicsWorld,rigidBodies;GameGlobal.GCreateBlp[0] = GameGlobal.GCreateBlp[1] = 1;
    var mapw = mapWorld(); physicsWorld = mapw[0];rigidBodies = mapw[1];camera = GameGlobal.camera;
    var e0 = GameGlobal.PEnds[0];var e1 = GameGlobal.PEnds[1];
    var gblpz = GameGlobal.GConfig.blpz;
    
    if(GameGlobal.gameMode==0){
      if(e0==true){
        onGameJs();return;
      }
    }
    else{
      if(e0==true&&e1==true){
        Laya.timer.handlerClear("GMTime");
        // console.log("Xgq updatePhysics onGameJs ");
        onGameJs();return;
      }else if(e0==true||e1==true)
      {
        if(GameGlobal.WaitBattleOnce==true){
          if((e0==true&&GameGlobal.matchMeOrder==0)||(e1==true&&GameGlobal.matchMeOrder==1)){
            GameGlobal.IsWaitMe = false;
            eftFloat("等待对方完成比赛","#b32210",500,72);
          }else{
            GameGlobal.IsWaitMe = true;
            eftFloat("对方已经完成比赛","#b32210",500,72);
          }
          blqWaitBattleTime();
          GameGlobal.WaitBattleOnce = false;
        }
      }
    }
    if(GameGlobal.GameStop==true){return;}
    GameGlobal.turnMe = true;
    if(GameGlobal.gameMode==1&&GameGlobal.turnMe==false){
      if(GameGlobal.IsMachine==true){
        if(GameGlobal.MachineOnce==1&&GameGlobal.thinking==false){
          GameGlobal.useYellowBall[GameGlobal.playerOrder] = thinkingUseHjq();
          if(GameGlobal.useYellowBall[GameGlobal.playerOrder]) GameGlobal.bRoundTip = 3;
          Laya.timer.handlerOnce("monShootBall",Math.random()*2000+500,function(){
            if(GameGlobal.playerOrder!=GameGlobal.matchMeOrder) onShootBall();
          })
          GameGlobal.MachineOnce = 0;
        }
      }else{
        updateHumanOther();
      }
    }
    var transformAux1 = GameGlobal.transformAux1;
    var Gs = GameGlobal.Gs;
    var ball = GameGlobal.ball;
    nTime = performance.now();
    deltaTime = 0.016;
    // console.log("Xgq 1 ballinput:"+GameGlobal.ballinput+" cFollow"+GameGlobal.cFollow);
    if(GameGlobal.ballinput){
      var bp = GameGlobal.ball.transform.position;
      var cpos = camera.transform.position; var oz = cpos.z; var ox = cpos.x;var tx = bp.x; var pz = bp.z; var tz = pz+3*Gs;
      processSkew(pz);   
      if(tz<-23*Gs){  tz = -23*Gs; }   
      if(tx<1500){
        if(GameGlobal.cFollow==1){if(bp.z<-gblpz+3){GameGlobal.cFollow = 2; } }
        if(GameGlobal.cFollow==2||GameGlobal.cFollow==1)
        {
          // console.log("Xgq bp.y::"+bp.y+" bp.z"+bp.z);
          var m3 = false;if(bp.y<-5||bp.z<-gblpz-30){
            Laya.SoundManager.stopSound(_s+"gd.mp3");
            m3 = true;
            if(GameGlobal.turnMe||GameGlobal.IsMachine){
              fadeBall();
            }
          } 
          if(GameGlobal.bPrez!=10000&&pz!=GameGlobal.SBPZ){
            var dpz = pz - GameGlobal.bPrez;
            if(dpz>-0.0001){
              if(GameGlobal.turnMe||GameGlobal.IsMachine){
                fadeBall();
              }
            }
          }
          if(m3==true){
            var pl = GameGlobal.matchMeOrder;
            if(GameGlobal.Once[pl] == true){
              GameGlobal.Once[pl] = false;
              GameGlobal.VPTime = 1500;
              // var stime = 0;
              // Laya.timer.handlerLoop("onViewCurrentBall3",16,function(){
              //   stime+=16;
              // })
              Laya.timer.handlerOnce("onViewCurrentBall3",1500,function(){
                GameGlobal.cFollow = 3;
                onViewCurrentBall();
              });
            }
          }
        }
        GameGlobal.bPrez = pz;
        if(GameGlobal.cFollow==1){
          var s = 0.96;
          if(tx!=-1000)
            camera.transform.position = (new Vector3(ox * s + tx * (1-s), cpos.y, oz * s + tz * (1-s)));
        }
        else if(GameGlobal.cFollow==2){

        }
        else if(GameGlobal.cFollow==3){
          var tx = GConfig.cx,ty = GConfig.cy,tz = GConfig.cz;
          if(GameGlobal.BPO==1){
            tx+=GameGlobal.BOY;
          }
          var ox = cpos.x,oz = cpos.z;
          var dx = ox - tx, dz = oz - tz;
          var slen = dx*dx+dz*dz;
          var s = 0.95;
          camera.transform.position = new Vector3(ox*s+tx*(1-s),cpos.y,oz*s+tz*(1-s));
          if(dx*dx+dz*dz<1){
            Laya.SoundManager.stopSound(_s+"gd.mp3");
            
            processGameHjqGet();
          }
        }
      }
    }
    pTime = nTime;
    
    // console.log("Xgq stepSimulation BPO:"+GameGlobal.BPO)
    physicsWorld.stepSimulation(deltaTime,2);
    var blpf = new Ammo.btVector3(0, 1, 0);
    for (var i = 0, il = rigidBodies.length; i < il; i++) {
      var objThree = rigidBodies[i];
      var objPhys = objThree.userData.physicsBody;
      var ms = objPhys?objPhys.getMotionState():null;
      if(ms==null){
        var st = 1;
      }
      // console.log("Xgq objThree.lact"+objThree.lact+" ms:"+ms+" turnMe:"+GameGlobal.turnMe+" "+GameGlobal.IsMachine);
      if (objThree.lact==true&&ms) {
        ms.getWorldTransform(transformAux1);
        if(!(GameGlobal.gameMode==1&&GameGlobal.turnMe==false&&GameGlobal.IsMachine==false))
        {
          var p = transformAux1.getOrigin();var q = transformAux1.getRotation();
          var x = p.x(),y = p.y(),z = p.z();
          objThree.transform.position = new Vector3(p.x(), p.y(), p.z());
          // console.log("Xgq phy["+objThree.name+"]:"+p.x()+" "+p.y()+" "+p.z()+" deltaTime:"+deltaTime);
          objThree.transform.rotation = new Laya.Quaternion(q.x(), q.y(), q.z(), q.w());
        }
        if(objThree.userData){
          if(objThree.userData.blpstate)
          {
            if(objThree.userData.blpstate=="Stand")
            {
              var ory = objThree.transform.up.y,opy = objThree.transform.position.y;
              if(ory<0.9||opy<0){
                objThree.userData.blpstate = "Down";
                if(GameGlobal.Hjp!=-1&&i==GameGlobal.Hjp){
                  GameGlobal.HjpGet = true;
                }
                if(GameGlobal.BlpBombOnce==true){
                  Laya.SoundManager.playSound(_s+"pz.mp3");
                  GameGlobal.BlpBombOnce = false;
                }
                eftBlpFade(objThree);
                GameGlobal.nBlpDownCount++;
                // console.log("Xgq GameGlobal.nBlpDownCount:"+GameGlobal.nBlpDownCount+" ory:"+ory+" "+opy);
              }
            }
          }
        }
        objThree.userData.collided = false;
      }
    }
    if(GameGlobal.ball){
      var tbp = GameGlobal.ball.transform.position;
      var bp = tbp;
      GameGlobal.TrailError = false;
      // console.log("Xgq bpy:"+bp.x+" "+bp.y+" "+bp.z);
      if(!(bp.y>0&&bp.y<1&&bp.x>-20&&bp.x<20&&bp.z<20)){
        if(GameGlobal.TrailObj){
          GameGlobal.TrailObj.time = 0;
          GameGlobal.TrailError = true;
        } 
      }
      else{
        if(bp.z>-gblpz+6){
          if(GameGlobal.TrailObj){
            GameGlobal.TrailObj.time = 0.3;
            GameGlobal.TrailObj.transform.position = new Laya.Vector3(tbp.x,tbp.y,tbp.z+2);
          }
        }else{
          if(GameGlobal.TrailObj){
            if(GameGlobal.TrailOnce==true){
              GameGlobal.TrailOnce = false;
              var stime = 0;
              var atime = 250;
              Laya.timer.handlerClear("TrailTime");
              Laya.timer.handlerLoop("TrailTime",16,function(){
                if(GameGlobal.TrailError==false){
                  GameGlobal.TrailObj.transform.position = new Laya.Vector3(tbp.x,tbp.y,tbp.z+
                    2+(stime/atime)*2);
                }
                stime+=16;
                GameGlobal.TrailObj.time = 0.3-stime/atime*0.3;
                if(stime>atime){
                  Laya.timer.handlerClear("TrailTime");
                }
              })
            }
          }
        }  
      }
    } 

    // if(GameGlobal.TrailObj){
    //   var dbpos = GameGlobal.TrailObj.transform.position;
    //   console.log("Xgq dbpos :"+dbpos.x+" "+dbpos.y+" "+dbpos.z);
    // }
    // console.log("Xgq stepSimulation End....:"+GameGlobal.BPO)
    if(GameGlobal.ballinput==null&&GameGlobal.ball&&GameGlobal.gameMode==0){
      GameGlobal.ball.transform.position = new Laya.Vector3(0,100,0);
    }
    if(GameGlobal.gameMode==1&&GameGlobal.turnMe==false&&GameGlobal.IsMachine==false){
      return;
    }
    if(GameGlobal.gameMode==1){
      if(GameGlobal.ballinput&&!GameGlobal.GameLoop&&GameGlobal.PollBall==true){
        var bpos = GameGlobal.ball.transform.position;
        var px = bpos.x,py = bpos.y,pz = bpos.z;
        var sdata;
        if(pz>-(GameGlobal.GConfig.blpz-5)&&py!=-100){
          GameGlobal.gameSDataType = 0;
          var eu = GameGlobal.ball.transform.rotationEuler;var ex = eu.x,ey = eu.y,ez = eu.z;
          px = Math.floor(px*100)/100;py= Math.floor(py*100)/100;pz= Math.floor(pz*100)/100;
          ex = Math.floor(ex*100)/100;ey= Math.floor(ey*100)/100;ez= Math.floor(ez*100)/100;
          sdata = GameGlobal.gameSData;
          sdata.push(px);sdata.push(py);sdata.push(pz);
          sdata.push(ex);sdata.push(ey);sdata.push(ey);
          GameGlobal.ballShootCount++
          if(GameGlobal.ballShootCount>=10){
            netGameSData(0);
            GameGlobal.ballShootCount = 0;GameGlobal.gameSData = [];
          }
        }else{
          if(GameGlobal.ballShootCountOnce==1){
            if(GameGlobal.ballShootCount>0){
              netGameSData(0);
            }
            GameGlobal.gameSData = [];
            GameGlobal.ballShootCount = 0;
            GameGlobal.ballShootCountOnce = 0;
          }
          sdata = GameGlobal.gameSData;
          GameGlobal.gameSDataType = 1;
          pumpGData();
          GameGlobal.ballShootCount++;
          if(GameGlobal.ballShootCount>=2){
            netGameSData(1);
            GameGlobal.ballShootCount = 0;GameGlobal.gameSData = [];
          }
        }
      }
    }
    // console.log("Xgq[restore] updatePhysics end");
  }

window.processSkew = function(pz){
  
  if((GameGlobal.turnMe||(!GameGlobal.turnMe&&GameGlobal.IsMachine))){
    var GConfig = GameGlobal.GConfig;
    var blv = GameGlobal.blv;
    
    if(blv==0) return;
    if(!GameGlobal.ball.userData) return;
    var ballbody = GameGlobal.ball.userData.physicsBody;
    // var bvf = GameGlobal.gravityConstant*GConfig.bm;
    // ballbody.applyForce(new Ammo.btVector3(0, bvf, 0), new Ammo.btVector3(0, 0, 0));

    var py = GameGlobal.ball.transform.position.y;
    var pz = GameGlobal.ball.transform.position.z;
    var vl = ballbody.getLinearVelocity();
    var vx = vl.x(),vz = vl.z();var ovx = vx,ovz = vz,ovy = vl.y();
    var vlen = Math.sqrt(vx*vx+vz*vz);
    if(vlen>0){ 
      vx/=vlen;vz/=vlen;
    }
    var nx = -vz*blv*vlen/60,nz = vx*blv*vlen/60;
    // console.log("Xgq nx:"+nx+" nz:"+nz+" blv:"+blv+" ovx:"+ovx+" ovz:"+ovz);
    ballbody.setLinearVelocity(new Ammo.btVector3(nx+ovx,ovy,nz+ovz));
    var nxf = -vx,nzf = -vz;    
    // var s = (vlen*GConfig.Skew1)/(GConfig.Skew2+Math.pow(vlen,GConfig.Sp)*GConfig.Skew3); 
    // if(py>0.8)
    // {
    //   if(pz>-17*GConfig.Gs)
    //     ballbody.applyForce(new Ammo.btVector3(nx*s, 0, nz*s), new Ammo.btVector3(0, 0, 0)); 
    //   else{
    //     s*=0.02;
    //     ballbody.applyForce(new Ammo.btVector3(nx*s, 0, nz*s), new Ammo.btVector3(0, 0, 0)); 
    //   }
    // }
    // var sf = GConfig.bfF;
    // if(vlen<3) sf = 0;
    // if(pz>-10*GConfig.Gs)
    //   ballbody.applyForce(new Ammo.btVector3(nxf*sf, 0, nzf*sf), new Ammo.btVector3(0, 0, 0));
  }  
}

window.pumpGData = function(){
  var sdata = GameGlobal.gameSData;
  var rigidBodies = GameGlobal.BPO==0?GameGlobal.rigidBodies:GameGlobal.rigidBodies2;
  for (var i = 0, il = rigidBodies.length; i < il; i++) {
    var objThree = rigidBodies[i];
    var bpos = objThree.transform.position;
    var px = bpos.x,py = bpos.y,pz = bpos.z;
    var eu = objThree.transform.rotationEuler;var ex = eu.x,ey = eu.y,ez = eu.z;
    px = Math.floor(px*100)/100;py= Math.floor(py*100)/100;pz= Math.floor(pz*100)/100;
    ex = Math.floor(ex*100)/100;ey= Math.floor(ey*100)/100;ez= Math.floor(ez*100)/100;
    sdata.push(px);sdata.push(py);sdata.push(pz);
    sdata.push(ex);sdata.push(ey);sdata.push(ez);
  }
}

window.initPhysics = function(){
  if(Ammo){
    function removeBodys(pworld,bodys){
      for(var k = 0;k<bodys.length;k++){
        var item = bodys[k];
        if(item.userData.physicsBody){
          pworld.removeRigidBody(item.userData.physicsBody);
        }
      }
    }
    if(!GameGlobal.collisionConfiguration){
      GameGlobal.collisionConfiguration = new Ammo.btDefaultCollisionConfiguration();
    }
    GameGlobal.dispatcher = new Ammo.btCollisionDispatcher(GameGlobal.collisionConfiguration);
    GameGlobal.broadphase = new Ammo.btDbvtBroadphase();
    GameGlobal.solver = new Ammo.btSequentialImpulseConstraintSolver();
    GameGlobal.physicsWorld = new Ammo.btDiscreteDynamicsWorld(GameGlobal.dispatcher,GameGlobal.broadphase, GameGlobal.solver, GameGlobal.collisionConfiguration);
    GameGlobal.physicsWorld.setGravity(new Ammo.btVector3(0, -GameGlobal.gravityConstant, 0));
    removeBodys(GameGlobal.physicsWorld,GameGlobal.rigidBodies);
    // if(!GameGlobal.collisionConfiguration2){
    //   GameGlobal.collisionConfiguration2 = new Ammo.btDefaultCollisionConfiguration();
    // }

    // GameGlobal.dispatcher2 = new Ammo.btCollisionDispatcher(GameGlobal.collisionConfiguration);
    // GameGlobal.broadphase2 = new Ammo.btDbvtBroadphase();
    // GameGlobal.solver2 = new Ammo.btSequentialImpulseConstraintSolver();
    // GameGlobal.physicsWorld2 = new Ammo.btDiscreteDynamicsWorld(
    //   GameGlobal.dispatcher2,GameGlobal.broadphase2, GameGlobal.solver2, GameGlobal.collisionConfiguration2);
    // GameGlobal.physicsWorld2.setGravity(new Ammo.btVector3(0, -GameGlobal.gravityConstant, 0));
    // removeBodys(GameGlobal.physicsWorld2,GameGlobal.rigidBodies2);
    GameGlobal.rigidBodies = [];
    // GameGlobal.rigidBodies2 = [];
  }
}
window.intersectsRayAndPlaneRP = function(ray,plane){
  var planeNor=plane.normal; 
  var direction=Vector3.dot(planeNor,ray.direction); 
  var position=Vector3.dot(planeNor,ray.origin); var distance=(-plane.distance-position)/ direction; 
  var nv3 = new Vector3(); 
  Vector3.scale(ray.direction,distance,nv3);
  nv3.x = ray.origin.x+nv3.x;nv3.y = ray.origin.y+nv3.y;nv3.z = ray.origin.z+nv3.z; 
  return nv3; }
var margin = GameGlobal.margin,GConfig = GameGlobal.GConfig;
window.createParalellepipedWithPhysics = function(w,h,r,x,y,z,material) {
  var physicsWorld;
  if(GameGlobal.BPO == 0){ physicsWorld = GameGlobal.physicsWorld; } else {physicsWorld = GameGlobal.physicsWorld2; x+=GameGlobal.BOY; }
  var p = GameGlobal.scene.addChild(new Laya.MeshSprite3D(new Laya.PlaneMesh(w,h,1,1)));
  var pos = new Vector3(x,y,z);p.transform.translate(pos);
  var shape;
  if(r==2){
    shape = new Ammo.btBoxShape(new Ammo.btVector3(w * 0.5, 0, h * 0.5));
  }else if(r==-3){
    p.transform.rotationEuler = new Laya.Vector3(0, 0, 90);
    shape = new Ammo.btBoxShape(new Ammo.btVector3(0 , 100, h * 0.5));
  }else if(r==3){
    p.transform.rotationEuler = new Laya.Vector3(0, 0, -90); 
    shape = new Ammo.btBoxShape(new Ammo.btVector3(0 , 100, h * 0.5));
    // shape = new Ammo.btBoxShape(new Ammo.btVector3(0 , w *0.5, h * 0.5));            
  }
  shape.setMargin(margin);
  createRigidBody(p, shape, 0, new Vector3(pos.x,pos.y,pos.z), null);
  // p.meshRenderer.material = material;
  return p;
}

window.createParalellepiped = function(w,h,r,x,y,z,material) {
  var physicsWorld;
  if(GameGlobal.BPO == 0){ physicsWorld = GameGlobal.physicsWorld; } else {physicsWorld = GameGlobal.physicsWorld2; x+=GameGlobal.BOY; }
  var p = GameGlobal.scene.addChild(new Laya.MeshSprite3D(new Laya.PlaneMesh(w,h,1,1)));
  var pos = new Vector3(x,y,z);p.transform.translate(pos);
  p.meshRenderer.material = material;
  return p;
}

