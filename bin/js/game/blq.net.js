window.onHomeMsg = function(evt){
  var data = JSON.parse(evt.data);
  console.log("onHomeMsg evt.data:"+evt.data);
}

window.wssOnMsg = function(evt){
  var data = JSON.parse(evt.data);
  if(data.code==1){
    eftFloat("网络出错了","#b3b3b3",600,60);
    return;
  }
  var msg = data.data;var cmd = msg.cmd;
  if(!msg.cmd){  cmd = msg.event;}
  // console.log("wssOnMsg ---["+cmd+"] openId:"+GGlobal.openId+" evt.data:"+evt.data);
  if(cmd=="gamed"){
    v = msg.v;
    var p = 1-GameGlobal.matchMeOrder;
    if(v[0]==0){
      saveGame(p);
      var lmap = processLData(p,v[1]);
      GameGlobal.gameLevel[p] = lmap.nlevel;
      refreshItemScore();
    }else if(v[0]==1){
      GameGlobal.BpData[p] = GameGlobal.BpData[p].slice(0,GameGlobal.BpData[p].length-1);
      restoreGame(p);
      var lmap = processLDataMap(p,0);
      GameGlobal.gameLevel[p] = lmap.nlevel;
      refreshItemScore();
    }else if(v[0]==2){
      animBiaoQin(1-GameGlobal.matchMeOrder,v[1]);
    }else if(v[0]==3){
      GameGlobal.PEnds[1-GameGlobal.matchMeOrder] = true;
    }
  }
  else if(cmd=="ming"){
    if(msg.state==1){
      var oimg,otxt;
      for(var k = 0;k<msg.ms.length;k++)
      {
        if(msg.ms[k].avatar!=GGlobal.avatarUrl){
          GGlobal.imgAv2 = msg.ms[k].avatar;
          GGlobal.txtAv2= msg.ms[k].name; 
        }else{
          GGlobal.cIdO = msg.ms[k].cIdO;
        }
        if(GGlobal.openId==msg.ms[k].openId){
          if(k==0){
            GameGlobal.matchMeOrder = 0;
          }else{
            GameGlobal.matchMeOrder = 1;
          }
        }
      }
      if(GameGlobal.matchMeOrder==1){
        var s = ['','img','txt'];
        for(var k = 1;k<=2;k++){
          var tmp = GGlobal[s[k]+'Av1'];
          GGlobal[s[k]+'Av1'] = GGlobal[s[k]+'Av2']
          GGlobal[s[k]+'Av2'] = tmp;
        } 
      }
      GGlobal.imgAv3 = GGlobal.imgAv1;GGlobal.txtAv3 = GGlobal.txtAv1;
      GGlobal.imgAv4 = GGlobal.imgAv2;GGlobal.txtAv4 = GGlobal.txtAv2;
      Laya.timer.handlerClear("matchingLoop");
      GameGlobal.matched = 1;
    }
  }else if(cmd=="turn"){
    Laya.timer.handlerClear("pwTimeLoop");
    blqScene3DChange();
    resetOneBall();
  }else if(cmd=="roomerror"){
    var state = msg.state;
    // "noroom";"playing";"maxplayer";
    if(state=="noroom"){
      eftFloat("房间已解散","#b3b3b3",600,60);
    }else if(state=="playing"||state=="maxplayer"){
      eftFloat("房间游戏已开始","#b3b3b3",600,60);
    }
    GameGlobal.BtnPanelClick[""+3]();
    // blqInitHome(GameGlobal.homeUi,3);
  }
  else if(cmd=="gdata"){
    var kt = msg.t;var t;
    if(kt<100){ t = kt;} else {
      var kv = Math.floor(kt/100);
      GameGlobal.useYellowBall[GameGlobal.playerOrder] = (kv==1?false:true);
    }
    if(t==3){
      GameGlobal.battleTurnThinking = true;
    }
    GameGlobal.gameSData.push.apply(GameGlobal.gameSData,msg.v);
    // console.log("Xgq gdata[R]-------:"+GameGlobal.gameSData.length);
    if(t==0){GameGlobal.ballSkSize1 = GameGlobal.gameSData.length;}
    GameGlobal.battlePack = t;GameGlobal.dbgBattle = 1;
  }else if(cmd=="end"){
    console.log("Xgq cmd end onGameJs ");
    onGameJs();
  }
  else if(cmd=="item"){
    GameGlobal.bRoundTip = 2+msg.t;
  }
  else if(cmd=="create_room"){
    GameGlobal.OPLeave = false;
    GGlobal.roomId = msg.roomId;
    GGlobal.playersInfo = msg.playersInfo;
    if(!wx.login){
      var pc = 0;for(var key in GGlobal.playersInfo) pc++;
      if(pc==1){
        var battleInfo = {roomId:GGlobal.roomId,openId:GGlobal.openId,mode:'BF'};
        localStorage.setItem("battleInfo",JSON.stringify(battleInfo));
      }
    }
    battleShare();
    console.log("create_room back.....");
  }else if(cmd=="join_room"){
    GameGlobal.OPLeave = false;
    GGlobal.playersInfo = msg.playersInfo;
    refreshBattleInfo("update");
  }else if(cmd=="ready"){
    GGlobal.playersInfo = msg.playersInfo;
    refreshBattleInfo("update");
  }
  else if(cmd=="game_start"){
    if(GameGlobal.BattleHomeState!="battlegame"){
      
      var ourl,otxt;
      for(var key in GGlobal.playersInfo ){
        GGlobal.playersInfo[key].isReady = true;
        if(key==GGlobal.openId){
          GGlobal.cIdO = msg.playersInfo[key].cIdO;
          GameGlobal.matchMeOrder = msg.playersInfo[key].order;
        }else{ourl = msg.playersInfo[key].avatar;otxt = msg.playersInfo[key].name;}
      }

      var data = {ourl:ourl,txtAv:otxt};

      if(GameGlobal.matchMeOrder==0){
        GGlobal.imgAv1 = GGlobal.avatarUrl; GGlobal.txtAv1 = GGlobal.nickName;
        GGlobal.imgAv2 = data.ourl; GGlobal.txtAv2 = data.txtAv;
      }else{
        GGlobal.imgAv2 = GGlobal.avatarUrl; GGlobal.txtAv2 = GGlobal.nickName;
        GGlobal.imgAv1 = data.ourl; GGlobal.txtAv1 = data.txtAv;
      }
      GGlobal.avatarUrl2 = data.ourl;
      GGlobal.imgAv3 = GGlobal.imgAv1;GGlobal.txtAv3 = GGlobal.txtAv1;
      GGlobal.imgAv4 = GGlobal.imgAv2;GGlobal.txtAv4 = GGlobal.txtAv2;
      GameGlobal.IsMachine = false;
      matchedDoneOp();
      refreshBattleInfo("update");
      GameGlobal.BattleHomeState = "battlegame";
      var stime = 0,satime = 3000;
      GameGlobal.IsFriendBattle = true;
      GameGlobal.homeUi.txtBFTime.visible = true;
      Laya.timer.handlerLoop("BattleTimeLoop",20,function(){
        stime+=20;
        GameGlobal.homeUi.txtBFTime.text = Math.floor((satime-stime)/1000);
        if(GameGlobal.OPLeave==true){
          Laya.timer.handlerClear("BattleTimeLoop");
        }else{
          if(stime>=satime){
            GameGlobal.homeUi.txtBFTime.text = "";
            Laya.timer.handlerClear("BattleTimeLoop");
            localStorage.removeItem("battleInfo");
            blqGoGame.bind(1)();
            GameGlobal.turnMe = !GGlobal.joinBattle;
          }
        }
      })
    }
  }
  else if(cmd=="leave_room"){
    GameGlobal.OPLeave = true;
    if(GameGlobal.InGame == true){
      var op = 1-GameGlobal.matchMeOrder;
      GameGlobal.LeaveGame[op] = 1;
      var gd = [];
      for(var k = 0;k<GameGlobal.TotalLevel;k++){
        gd.push({});
      }
      GameGlobal.RoundsData[op] = gd;
      //function(txt,color,py,fontSize)
      if(GameGlobal.gameOver!=true) eftFloat("对手已离线","#b32210",500,72);
      Laya.timer.handlerOnce("playerleave",1000,function(){onGameJs();})
    }else{
      if(GGlobal.joinBattle==true){
        eftFloat("房间已解散","#b32323",GGlobal.EftTextY,48);
        Laya.timer.handlerOnce("roomout",1000,function(){
          GameGlobal.btnLogicSelPre = 3;
          GameGlobal.BtnPanelClick[""+3]();})
      }else {
        if(GameGlobal.gameOver!=true) eftFloat("对手离开游戏,请重新发起挑战","#b32210",GGlobal.EftTextY,48);
        GGlobal.WssSocketClose(function(){
          localStorage.removeItem("battleInfo");
          refreshBattleInfo("init","toready");
        })
      }
    }
  }
}
window.netMatchingPlayer = function(){
    GameGlobal.matched = 0;
    GGlobal.WssSocketOpen(GGlobal.WSSURL,
        wssOnMsg.bind(this),function(){
          var stime = 0;
          Laya.timer.handlerLoop("matchingLoop",100,function(){
          var data = {
              command:"room",
              action:"matching",
              openId:GGlobal.openId,
              name:GGlobal.nickName,
              avatar:GGlobal.avatarUrl,
              stime:stime
            };
              stime+=100;
              
              if(stime>=(GGlobal.gmachine?0:GGlobal.gmachinetime)){
                Laya.timer.handlerClear("matchingLoop");
                GGlobal.WssSocketClose(function(){
                  function matchAuto(jsData){
                    if(jsData.code==-1){
                      eftFloat("网络出错了","#b3b3b3",600,60);
                      return;
                    }
                    if(jsData){
                      // jsData = JSON.parse(info);
                    }else{
                      jsData = {};jsData.code = 0;
                      jsData.data = {};
                      var rx =  Math.floor(Math.random()*(GGlobal.GMachines.length/2));
                      jsData.data.ourl = GGlobal.GMachines[rx*2];
                      jsData.data.txtAv = GGlobal.GMachines[rx*2+1];
                    }
                    var code = jsData['code'];if(code==1){  
                      GoToHome.bind(4)();
                      return;
                    }
                    var data = jsData.data;
                    var r =  Math.random();
                    if(GGlobal.gmachineleft) r = 0.7;
                    if(r<0.5){
                      GGlobal.imgAv1 = GGlobal.avatarUrl; GGlobal.txtAv1 = GGlobal.nickName;
                      GGlobal.imgAv2 = data.ourl; GGlobal.txtAv2 = data.txtAv;
                      GameGlobal.matchMeOrder = 0;
                    }else{
                      GGlobal.imgAv2 = GGlobal.avatarUrl; GGlobal.txtAv2 = GGlobal.nickName;
                      GGlobal.imgAv1 = data.ourl; GGlobal.txtAv1 = data.txtAv;
                      GameGlobal.matchMeOrder = 1;
                    }
                    GGlobal.avatarUrl2 = data.ourl;
                    GGlobal.imgAv3 = GGlobal.imgAv1;GGlobal.txtAv3 = GGlobal.txtAv1;
                    GGlobal.imgAv4 = GGlobal.imgAv2;GGlobal.txtAv4 = GGlobal.txtAv2;
                    matchedDone();
                    GameGlobal.IsMachine = true;
                  }
                  GGlobal.RequestPost(GGlobal.REMOTE_SERVER_URL+'matchingMachine',
                      {avatarUrl:GGlobal.avatarUrl},
                      function(info,jsData){
                        matchAuto(jsData);
                      },
                      function(info,jsData){ 
                        // GGlobal.FloatingText("网络连接中断",200,600,300);
                      }
                  )
                  // matchAuto();
                  });
              }else{
                GGlobal.WssSocketSend(JSON.stringify(data));
              } 
          });
        }
    ); 
}
window.netCreateRoom = function(){
  GGlobal.WssSocketOpen(GGlobal.WSSURL,
    wssOnMsg.bind(this),function(){
    var data = {
        command:"room",
        action:"create",
        openId:GGlobal.openId,
        name:GGlobal.nickName,
        avatar:GGlobal.avatarUrl
    };
    GGlobal.WssSocketSend(JSON.stringify(data));
  });
}

window.netJoinRoom = function(){
  // console.log("Xgq btnOnOpen.........:"+GGlobal.avatarUrl);
  var data = {
      command:"room",
      action:"join",
      openId:GGlobal.openId,
      name:GGlobal.nickName,
      avatar:GGlobal.avatarUrl,
      roomId:GGlobal.roomId
  };
  GameGlobal.WssSocketSend(JSON.stringify(data));
}
window.netGameLData = function(v){
  if(GameGlobal.IsMachine == true||GameGlobal.gameMode==0) return;
  var data = {
    command:"room",
    action:"gamed",
    o:GGlobal.cIdO,
    v:v
  };
  GGlobal.WssSocketSend(JSON.stringify(data));
}
window.netGameItem = function(t){}
window.netGameEnd = function(){}
window.netGameSData = function(t){}
window.netTurnPlayer = function(){}
window.netSendBiaoQin = function(v){}


