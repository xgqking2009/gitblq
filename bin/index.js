window.screenOrientation = "sensor_landscape";
if(!window.wx){
    window.wx = {
        loadSubpackage:function(obj){
          if(obj.name!="respng"){
            loadLib("libs/"+obj.name+".js");
          }
          obj.success(null);
        },
        getSystemInfoSync:function(){
          return {
            windowWidth:720,
            windowHeight:1280
          }
        },
        getLaunchOptionsSync:function(){
          return {query:{key:null}};
        }
    }
    window.canvas = {addEventListener:function(event,fn){window.addEventListener(event,fn);}},
    window.performance = {  now: function now() {    return Date.now() / 1000}}
}

window.loadSubPkgs = function(pkgs,done){
    if(pkgs.length>0){
      wx.loadSubpackage({
        name: pkgs[0],
        success: function (res){
          loadSubPkgs(pkgs.slice(1,pkgs.length),done);
        },
        fail: function (res){
        }
      })
    }else{
      done();
    }
}

function endloadlib(){
  loadLib("js/game/CryptoJS.js")
  loadLib("js/game/Utils.js")
  loadLib("js/game/UtilsBAv.js")
  loadLib("js/game/blq.utils.js")
  loadLib("js/game/blq.uiutils.js")
  loadLib("js/game/blq.graphic.js")
  loadLib("js/game/blq.phyhuman.js")
  loadLib("js/game/blq.phy.js")
  loadLib("js/game/blq.input.js")
  loadLib("js/game/blq.fg.js")
  loadLib("js/game/blq.logic.js")
  loadLib("js/game/blq.ui.js")
  loadLib("js/game/blq.game3D.js")
  loadLib("js/game/blq.net.js")
  loadLib("js/game/blq.ntp.js")
  loadLib("js/game/blq.adaptpx.js")
  loadLib("js/game/blq.netgame.js")
  loadLib("js/bundle.js");
}
loadLib("libs/ammo.js");
loadLib("libs/laya.core.js");
loadLib("libs/laya.webgl.js");
loadLib("libs/laya.ui.js");
loadLib("libs/laya.d3.js");
endloadlib();