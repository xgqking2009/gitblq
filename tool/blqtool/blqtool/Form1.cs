﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;

namespace blqtool
{

    public partial class Form1 : Form
    {
        enum NetState
        {
            ELocal,
            ETest,
            EOffical
        };
        private NetState mNetState = NetState.ELocal;
        private string mApiUrl;
        private JObject mConfig;

        public Form1()
        {
            string jsonstr = System.IO.File.ReadAllText(@"config.json");
            mConfig = JObject.Parse(jsonstr);
            string ns = mConfig["NetState"].ToString();
            if(ns == "0")
            {
                mApiUrl = "http://192.168.1.174/index.php?s=/mzblq/Back/";
            }else if(ns == "1")
            {
                mApiUrl = "https://test.api.sg.jiawanhd.com/index.php?s=/mzblq/Back/";
            }else if(ns == "2")
            {
                mApiUrl = "https://api.sg.jiawanhd.com/index.php?s=/mzblq/Back/";
            }
            InitializeComponent();
        }

        private string PostHttp(string url, string body, string contentType)
        {
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);

            httpWebRequest.ContentType = contentType;
            httpWebRequest.Method = "POST";
            httpWebRequest.Timeout = 20000;

            byte[] btBodys = Encoding.UTF8.GetBytes(body);
            httpWebRequest.ContentLength = btBodys.Length;
            httpWebRequest.GetRequestStream().Write(btBodys, 0, btBodys.Length);

            HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream());
            string responseContent = streamReader.ReadToEnd();

            httpWebResponse.Close();
            streamReader.Close();
            httpWebRequest.Abort();
            httpWebResponse.Close();
            return responseContent;
        }

        private void btnConfig_Click(object sender, EventArgs e)
        {
            PostHttp(mApiUrl+ "config", "data="+ mConfig.ToString(), "application/x-www-form-urlencoded;charset=utf-8");
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
