<?php

namespace app\mzblq\controller;

use think\worker\Server;

class Worker extends Server
{
    protected $socket = 'websocket://0.0.0.0:8347';

    //protected $context = [
    //    'ssl' => [
    //    'local_cert' => '/usr/local/nginx/conf/api.sg.jiawanhd.com.crt',
    //    'local_pk' => '/usr/local/nginx/conf/api.sg.jiawanhd.com.key',
    //    'verify_peer' => false,
    //    ],
    //];
    //protected $transport = 'ssl';
    //protected $processes = 1;

    /**
     * 每个进程启动
     * @param $worker
     */
    public function onWorkerStart($worker)
    {
        $myfile = fopen("logws.txt", "w");
        fclose($myfile);
        GameServer::getInstance()->startTimer();
    }

    /**
     * 当连接建立时触发的回调函数
     * @param $connection
     */
    public function onConnect($connection)
    {
        GameServer::getInstance()->onConnect($connection);
    }

    /**
     * 收到信息
     * @param $connection
     * @param $data
     */
    public function onMessage($connection, $data)
    {
        GameServer::getInstance()->onMessage($connection, $data);
    }

    /**
     * 当连接断开时触发的回调函数
     * @param $connection
     */
    public function onClose($connection)
    {
        GameServer::getInstance()->onClose($connection);
    }

    /**
     * 当客户端的连接上发生错误时触发
     * @param $connection
     * @param $code
     * @param $msg
     */
    public function onError($connection, $code, $msg)
    {
    }
}