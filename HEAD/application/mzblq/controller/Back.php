<?php

namespace app\mzblq\controller;

use WXBizDataCrypt\WXBizDataCrypt;

// $trace = debug_backtrace(); $tracestr = json_encode($trace,JSON_FORCE_OBJECT); 
// file_put_contents("log.txt", "test：$city\r\n".$tracestr."\r\n",FILE_APPEND); 
function debug($msg){
    file_put_contents("logback.txt", "$msg\r\n",FILE_APPEND);
}
class Back extends Base {

    public function __construct()
    {
        parent::__construct();
        $this->configKey = $this->redisCachePrefix.'config';
    }

    public static $clientCount = 0;
    public $configKey;

    public function config(){
        session_start();
        $data = input("post.data");
        $configKey = $this->configKey;
        $data = json_decode($data, true);
        $config = $data['config'];
        $strconfig = json_encode($config);
        $this->redis->hset($configKey,'config',$strconfig);
    }
}