<?php
namespace app\nxfxj\controller;

class Player extends Command
{

    public function __construct($connection, $data)
    {
        parent::__construct($connection, $data);

    }

    /**
     * 提交分数
     */
    public function submit_score()
    {
        $roomId = $this->getRoomId();
        $openId = $this->getOpenId();
        $score  = empty($this->data['score']) ? 0 : intval($this->data['score']);
        Room::$roomList[$roomId]['updateTime'] = time();
        if($score >= 0)
            Room::$roomList[$roomId]['playersInfo'][$openId]['score'] = $score;
        $message = [
            'event' => 'update_score',
            'openId' => $openId,
            'score' => $score
        ];
        return $this->reply($message, Room::getConnections($roomId));
    }

    /**
     * 死掉
     */
    public function dead()
    {
        $roomId = $this->getRoomId();
        $openId = $this->getOpenId();
        Room::$roomList[$roomId]['updateTime'] = time();
        Room::$roomList[$roomId]['playersInfo'][$openId]['isDead'] = 1;
        Room::$roomList[$roomId]['deadStatus'][$openId] = $this->getConnectionId();
        $message = [
            'event' => 'dead',
            'openId' => $openId
        ];
        return $this->reply($message, Room::getConnections($roomId));
    }

    /**
     * 获取roomId
     */
    private function getRoomId()
    {
        $connectionId = $this->getConnectionId();
        if(!isset(Room::$connectionIdToRoomId[$connectionId]))
            throw new \Exception("player is not in room");

        $roomId = Room::$connectionIdToRoomId[$connectionId];
        //房间不存在
        if(!isset(Room::$roomList[$roomId]))
            throw new \Exception("room is not exists");

        return $roomId;
    }

}