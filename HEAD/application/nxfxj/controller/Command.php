<?php
namespace app\nxfxj\controller;

class Command
{
    protected $connection = null;
    protected $data = [];

    public function __construct($connection, $data)
    {
        $this->connection = $connection;
        $this->data = $data;
    }

    public function getConnectionId()
    {
        return $this->connection->id;
    }

    public function getOpenId()
    {
        if(!isset($this->data['openId']))
            throw new \Exception('parameter[openId] error');

        return $this->data['openId'];
    }

    public function reply($return, $connectionIds = null)
    {
        return [$return, $connectionIds];
    }
}