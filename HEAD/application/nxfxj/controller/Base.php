<?php

namespace app\nxfxj\controller;

use think\Controller;
use think\Cache;

class Base extends Controller
{
    protected $redisCache;
    protected $redisCachePrefix;
    protected $redis;
    protected $aesKey;
    protected $aesIv;
    protected $appID;
    protected $appSecret;
    protected $requestLimit;
    protected $gameConfig;

    public function __construct()
    {
        $this->aesKey = config('nxfxj.aes_key');
        $this->aesIv = config('nxfxj.aes_iv');
        $this->requestLimit = config('nxfxj.request_limit');
        $this->redisCache = config('nxfxj.redis_cache');
        $this->redisCachePrefix = config('nxfxj.redis_cache_prefix');
        $this->appID = config('nxfxj.mp_appid');
        $this->appSecret = config('nxfxj.mp_appsecret');
        $this->gameConfig = config('nxfxj.game_config');

        try {
            $this->redis = Cache::store($this->redisCache);
            $this->redis = $this->redis->handler();
        }catch (\Exception $e) {
            $this->_error(['code' => -1, 'msg' => 'server went away']);
        }

        $ip = request()->ip();
        $key = $this->redisCachePrefix.'ip:'.$ip;
        $count = $this->redis->incr($key);
        if($count == 1)
            $this->redis->expire($key, 5);//设置过期时间
        if($count > $this->requestLimit)
            $this->_error(['code' => -2, 'msg' => '请求太频繁']);

    }

    public function _error($data)
    {
        header('content-type:application/json;charset=utf-8');
        echo json_encode($data);
        exit();
    }

    public function decrypt($data)
    {
        //补0
        if (strlen($data) % 16) {
            $data = str_pad($data,strlen($data) + 16 - strlen($data) % 16, "\0");
        }

        //解密
        $data = openssl_decrypt($data, "AES-128-CBC", $this->aesKey, OPENSSL_NO_PADDING, $this->aesIv);
        //去掉补0
        return rtrim( $data,chr(0));
    }
    
}