<?php

namespace app\api\controller;

use think\Controller;
use think\Cache;

class ApiBase extends Controller
{
    protected $redis;
    protected $aesKey;
    protected $aesIv;
    protected $requestLimit;

    public function __construct()
    {
        $this->aesKey = config('api.aes_key');
        $this->aesIv = config('api.aes_iv');
        $this->requestLimit = config('api.request_limit');

        try {
            $this->redis = Cache::store('redis');
            $this->redis = $this->redis->handler();
        }catch (\Exception $e) {
            $this->_error(['code' => -1, 'msg' => 'server went away']);
        }

        $ip = request()->ip();
        $key = 'ip:'.$ip;
        $count = $this->redis->incr($key);
        if($count == 1)
            $this->redis->expire($key, 5);//设置过期时间
        // if($count > $this->requestLimit)
        //     $this->_error(['code' => -2, 'msg' => '请求太频繁']);

    }

    public function _error($data)
    {
        header('content-type:application/json;charset=utf-8');
        echo json_encode($data);
        exit();
    }

    public function decrypt($data)
    {
        //补0
        if (strlen($data) % 16) {
            $data = str_pad($data,strlen($data) + 16 - strlen($data) % 16, "\0");
        }

        //解密
        $data = openssl_decrypt($data, "AES-128-CBC", $this->aesKey, OPENSSL_NO_PADDING, $this->aesIv);
        //去掉补0
        return rtrim( $data,chr(0));
    }
    
}