<?php

namespace app\api\controller;

use WXBizDataCrypt\WXBizDataCrypt;


class User extends ApiBase {

    public function __construct()
    {
        parent::__construct();
    }

    //登录 
    public function login()
    {

        $appID = config('wechat.mp_appid');
        $appSecret = config('wechat.mp_appsecret');


        $data = input("post.data");
        $data = json_decode($data, true);

        if(empty($data) || empty($data['gameData']) || empty($data['code']) || empty($data['encryptedData']) || empty($data['iv']))
            return json(['code' => 1, 'msg' => '参数错误']);
        $code = $data['code'];
        $encryptedData = $data['encryptedData'];
        $gameData = $data['gameData'];
        $iv = $data['iv'];
        $gameData = base64_decode($gameData);
        //解密游戏数据
        $gameData = $this->decrypt($gameData);
        $param = array( 
            'appid' => $appID, 
            'secret' => $appSecret, 
            'js_code' => $code, 
            'grant_type' => 'authorization_code'
        );
        //用户登录凭证（有效期五分钟）
        $res = http("https://api.weixin.qq.com/sns/jscode2session", $param, 'post'); 
        $arr = json_decode($res, true); 
        // exit();
        if(isset($arr['errcode']) && $arr['errcode'] != 0)
        {
            return json(['code' => $arr['errcode'], 'msg' => $arr['errmsg']]);
        }
        /*$arr['session_key'] = 'tiihtNczf5v6AKRyjwEUhQ==';
        $encryptedData="CiyLU1Aw2KjvrjMdj8YKliAjtP4gsMZM
                QmRzooG2xrDcvSnxIMXFufNstNGTyaGS
                9uT5geRa0W4oTOb1WT7fJlAC+oNPdbB+
                3hVbJSRgv+4lGOETKUQz6OYStslQ142d
                NCuabNPGBzlooOmB231qMM85d2/fV6Ch
                evvXvQP8Hkue1poOFtnEtpyxVLW1zAo6
                /1Xx1COxFvrc2d7UL/lmHInNlxuacJXw
                u0fjpXfz/YqYzBIBzD6WUfTIF9GRHpOn
                /Hz7saL8xz+W//FRAUid1OksQaQx4CMs
                8LOddcQhULW4ucetDf96JcR3g0gfRK4P
                C7E/r7Z6xNrXd2UIeorGj5Ef7b1pJAYB
                6Y5anaHqZ9J6nKEBvB4DnNLIVWSgARns
                /8wR2SiRS7MNACwTyrGvt9ts8p12PKFd
                lqYTopNHR1Vf7XjfhQlVsAJdNiKdYmYV
                oKlaRv85IfVunYzO0IKXsyl7JCUjCpoG
                20f0a04COwfneQAGGwd5oa+T8yO5hzuy
                Db/XcxxmK01EpqOyuxINew==";

        $iv = 'r7BXXKkLb8qrSNn05n0qiA==';*/

        // echo " 2==".$gameData;
        $crypt = new WXBizDataCrypt($appID, $arr['session_key']);

        $userData = [];

        $errCode = $crypt->decryptData($encryptedData, $iv, $userData);
        
        if ($errCode == 0)
        {

            //解密成功
            $datas = json_decode($userData, true);
 
            $avatarUrl = $datas['avatarUrl'];
            $city = $datas['city'];
            $province = $datas['province'];
            $country = $datas['country'];
            $nickName = $this->filterNickname($datas['nickName']);
            $openId = $datas['openId'];

            $key = 'user:'.$openId;
            $info = $this->redis->hgetall($key);
            $data = [];
            $data['openId'] = $openId;
            $data['avatarUrl'] = $avatarUrl;
            $data['nickName'] = $nickName;
            $data['city'] = $city;
            $data['province'] = $province;
            $data['country'] = $country;

            if(empty($info)){
                $data['gameData'] = $gameData;
                $data['createdAt'] = time();
                $data['updatedAt'] = 0;
                $data['flag'] = 0;
            }
            else
            {
                $data['updatedAt'] = time();
            }

            $ret = $this->redis->hmset($key, $data);

            if(!$ret)
                return json(['code' => 1, 'msg' => '保存用户信息失败']);

            $data['gameData'] = json_decode($gameData);
            
            return json(['code' => 0, 'msg' => '用户登录成功', 'data' => $data]);
        }
        else
        {
            return json(['code' => 1, 'msg' => '解密用户信息失败']);
        }
 
    }
 
    //过滤nickname 标签
    public function filterNickname($nickname) {
        $nickname = preg_replace('/[\x{1F600}-\x{1F64F}]/u', '', $nickname);
        $nickname = preg_replace('/[\x{1F300}-\x{1F5FF}]/u', '', $nickname);
        $nickname = preg_replace('/[\x{1F680}-\x{1F6FF}]/u', '', $nickname);
        $nickname = preg_replace('/[\x{2600}-\x{26FF}]/u', '', $nickname);
        $nickname = preg_replace('/[\x{2700}-\x{27BF}]/u', '', $nickname);
        $nickname = str_replace(array('"', '\''), '', $nickname);
 
        return trim($nickname);
    }
 
    //提交分数
    public function submitData()
    {
        $data = input("post.data");
        $data = base64_decode($data);
        //解密
        $data = $this->decrypt($data);
        $arr = json_decode($data, true);
        if(empty($arr) || empty($arr['openId']) || empty($arr['score']))
            return json(['code' => 1, 'msg' => '参数错误']);

        $openId = $arr['openId'];
        $score = abs($arr['score']);
        $key = 'user:'.$openId;
        $info = $this->redis->hgetall($key);
        if(empty($info))
            return json(['code' => 1, 'msg' => '用户信息不存在']);

        $total = $info['score'] + $score;
        $data = [];
        $data['score'] = $total;
        $data['updatedAt'] = time();

        $ret = $this->redis->hmset($key, $data);

        if(!$ret)
            return json(['code' => 1, 'msg' => '提交信息失败']);

        //更新排行榜
        $data = [];
        $data['avatarUrl'] = $info['avatarUrl'];
        $data['nickName'] = $info['nickName'];

        $this->redis->zadd('rank', $total, json_encode($data));

        return json(['code' => 0, 'msg' => '提交信息成功']);
 
    }

    //获取排行榜
    public function getRank()
    {
        $top = input("post.top", 100, 'intval');
        $top = $top - 1;
        $top = abs($top) > 100 ? 100 : abs($top);

        $lists = $this->redis->zrevrange('rank', 0, $top, true);
        $rank = [];
        foreach($lists as $key => $score)
        {
            $info = [];
            $info = json_decode($key);
            $info->score = $score;
            $rank[] = $info;
        }
        if(empty($rank))
            return json(['code' => 1, 'msg' => '排行榜为空']);

        return json(['code' => 0, 'msg' => '获取排行榜成功', 'data' => $rank]);
    }

    // public function logFile()
    // {
    //     $data = input("post.data");
    //     $file  = 'log.txt';//要写入文件的文件名（可以是任意文件名），如果文件不存在，将会创建一个
    //     $content = $data.'\n';  
    //     if($f  = file_put_contents($file, $content,FILE_APPEND)){// 这个函数支持版本(PHP 5) 
    //     }
    // }
}
