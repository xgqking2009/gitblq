<?php
namespace app\mzblq\controller;

class Room extends Command
{
    const MIN_ROOM_PLAYER = 2;
    const MAX_ROOM_PLAYER = 32;

    public static $roomList = [];
    public static $playerMap = [];
    public static $mCMap = [];
    public static $mIds = [];
    public static $mCList = [];
    public static $connectionIdToRoomId = [];
    public static $roomCls = [];
    public static $playerMapWx = [];
    public static $mCMapWx = [];
    public static $isEcho = true;
    public static function debug($msg){
        file_put_contents("logws.txt", "$msg\r\n",FILE_APPEND);
    }

    public function __construct($connection, $data)
    {
        parent::__construct($connection, $data);

    }


    public function matching(){
        $openId = $this->data['openId'];
        if(!isset(self::$playerMap[$openId])){
            $mv = array("openId"=>$openId,"name"=>$this->data['name'],"avatar"=>$this->data['avatar'],
                "stime"=>$this->data['stime'],"cId"=>$this->getConnectionId());
            self::$mCMap[$mv['cId']] = $mv;
            self::$mCList[] = $mv;
            self::$playerMap[$openId] = $mv;

        }else{
            self::$playerMap[$openId]['stime'] = $this->data['stime'];
        }
        $pcount = count(self::$mCList);
        $me = self::$playerMap[$openId];
        // echo "matching pcount:".$pcount."\r\n";
        // var_dump(self::$mCList);
        $tcount = 0; $imOid = 'null';$midx = -1;
        if($pcount>$tcount){
            for($k = 0;$k<10;$k++){
                $v = rand(0,$pcount-1);
                $imOid = self::$mCList[$v]['openId'];
                if($imOid!=$openId){
                    $midx = $v;
                    break;
                }
            }
        }
        $cls = [];
        if($midx!=-1){
            $cls[self::$playerMap[$openId]['cId']] = 1;
            $cls[self::$playerMap[$imOid]['cId']] = 1;
            $mp = self::$playerMap[$openId];
            $mv = self::$playerMap[$imOid];
            $mp['cIdO'] = self::$playerMap[$imOid]['cId'];
            $mv['cIdO'] = self::$playerMap[$openId]['cId'];

            if($mp['name']>$mv['name']){
                $tmp = $mp;
                $mp = $mv;
                $mv = $tmp;
            }
            $mds = [$mp['cIdO'],$mv['cIdO']];
            $msg = array("cmd"=>"ming","state"=>1,"ms"=>[$mp,$mv]);
            self::$mIds[$mp['cIdO']] = $mds;
            self::$mIds[$mv['cIdO']] = $mds;
            array_splice(self::$mCList,$v,1);
            $pv = array_search(self::$mCList, $mp);
            array_splice(self::$mCList,$pv,1);
        }else{
            $msg = array("cmd"=>"ming","state"=>0);
        }
        // var_dump($cls);
        return $this->reply($msg,$cls);
    }

    public function turn(){
        $cls = [];$cls[$this->data['cIdO']] = 1;
        $msg = array("cmd"=>"turn");
        return $this->reply($msg,$cls);
    }

    public function bq(){
        $cls = [];$cls[$this->data['cIdO']] = 1;
        $msg = array("cmd"=>"bq","v"=>$this->data['v']);
        return $this->reply($msg,$cls);
    }

    public function end(){
        $cls = [];$cls[$this->data['cIdO']] = 1;
        $msg = array("cmd"=>"end");
        return $this->reply($msg,$cls);
    }

    public function item(){
        $cls = [];$cls[$this->data['cIdO']] = 1;
        $msg = array("cmd"=>"item","t"=>$this->data['t']);
        return $this->reply($msg,$cls);
    }

    public function gamed(){
        $cls = [];$cls[$this->data['o']] = 1;
        $msg = array("cmd"=>"gamed","v"=>$this->data['v']);
        return $this->reply($msg,$cls);
    }

    public function gdata(){
        $msg = array("cmd"=>"gdata","t"=>$this->data['t'],"v"=>$this->data['v']);
        $cls = [];$cls[$this->data['o']] = 1;
        return $this->reply($msg,$cls);
    }
    public static function deleteMatching($connectionId){
        if(isset(self::$mCMap[$connectionId])){
            $index = array_search(self::$mCMap[$connectionId], self::$mCList);
            array_splice(self::$mCList,$index,1);
            unset(self::$playerMap[self::$mCMap[$connectionId]['openId']]);
            unset(self::$mCMap[$connectionId]);
        }
        $tmpo = [];
        if(isset(self::$mIds[$connectionId]))
        {
            $tmpa = [];
            foreach(self::$mIds[$connectionId] as $value) 
            {
                if($value!=$connectionId)
                    $tmpo[] = $value;
                $tmp[] = $value;
            }
            foreach ($tmpo as $value) {
                self::$mIds[$value] = $tmpo;
            }
            unset(self::$mIds[$connectionId]);
            return $tmpo;
        }
    }
    public static function deleteWxMatch($connectionId){
        if(isset(self::$mCMapWx[$connectionId])){
            unset(self::$playerMapWx[self::$mCMapWx[$connectionId]['openId']]);
            unset(self::$mCMapWx[$connectionId]);
        }
    }

    public static function deleteRoom($roomId,$connectionId){
        if(isset(self::$roomList[$roomId])){
            $clen = count(self::$roomList[$roomId]['connectionIds']);
            if($clen==0){
                unset(self::$roomList[$roomId]);
            }
            if(isset(self::$roomList[$roomId])) 
                $rlen = count(self::$roomList);
            else
                $rlen = 0;
            if(self::$isEcho){
                echo "room length:$rlen\r\n";
                foreach (self::$roomList as $key => $value) {
                    echo "room[$key]:=========\r\n";
                }
            }
        }
    }
    /**
     * 创建房间
     */
    public function create()
    {
        $createRoom = false;
        for($k = 0;$k<10;$k++){
            $roomId = uniqid();
            // echo "".$roomId."\r\n";
            if(!isset(self::$roomList[$roomId]))
            {
                $createRoom = true;
                break;
            }
        }
        if($createRoom==false){
            $cls = [];$cls[$this->getConnectionId()] = 1;
            $message = ['event' => 'roomerror','state' => 'roomexist'];
            return $this->reply($message, $cls);
            // throw new \Exception('room already exists');
        }

        //最大人数
        $maxPlayer = isset($this->data['maxPlayer']) && intval($this->data['maxPlayer']) > self::MIN_ROOM_PLAYER ? intval($this->data['maxPlayer']) : self::MIN_ROOM_PLAYER;
        $maxPlayer = $maxPlayer > self::MAX_ROOM_PLAYER ? self::MAX_ROOM_PLAYER : $maxPlayer;
        //初始化房间
        if(self::$isEcho) echo "initRoom $roomId\r\n";
        $this->initRoom($roomId, $maxPlayer);
        // var_dump(self::$roomList[$roomId]);
        //进入房间
        $state = $this->enterRoom($roomId);
        if($state!="ok"){
            $cls = [];$cls[$this->getConnectionId()] = 1;
            $message = ['event' => 'roomerror','state' => $state];
            return $this->reply($message, $cls);
        }else{
            return $this->reply(self::broadcast($roomId, 'create_room'), self::getConnections($roomId));
        }
        //增加定时器回调
        // GameServer::getInstance()->addTimer($roomId, function() use ($roomId) { return self::gameStatus($roomId);});
// self::broadcast($roomId, 'create_room')
        // var_dump(self::getConnections($roomId));
        
    }

    public function match()
    {
        $roomId = self::getRandomRoom(1);

        //如果房间不存在，则初始化它
        if(!isset(self::$roomList[$roomId]))
        {
            //最大人数
            $maxPlayer = isset($this->data['maxPlayer']) && intval($this->data['maxPlayer']) > self::MIN_ROOM_PLAYER ? intval($this->data['maxPlayer']) : self::MIN_ROOM_PLAYER;
            $maxPlayer = $maxPlayer > self::MAX_ROOM_PLAYER ? self::MAX_ROOM_PLAYER : $maxPlayer;
            //初始化房间
            $this->initRoom($roomId, $maxPlayer, 1);
            //进入房间
            $state = $this->enterRoom($roomId);
            //增加定时器回调
            // GameServer::getInstance()->addTimer($roomId, function() use ($roomId) { return self::gameStatus($roomId);});
            //return $this->reply(self::broadcast($roomId, 'create_room'), self::getConnections($roomId));
        }
        else
        {
            //进入房间
            $state = $this->enterRoom($roomId);
        }
        if($state!="ok"){
            $cls = [];$cls[$this->getConnectionId()] = 1;
            $message = ['event' => 'roomerror','state' => $state];
            return $this->reply($message, $cls);
        }else{
            return $this->reply(self::broadcast($roomId, 'create_room'), self::getConnections($roomId));
        }
        // return $this->reply(self::broadcast($roomId, 'join_room'), self::getConnections($roomId));
    }

    /**
     * 加入房间
     */
    public function join()
    {
        $roomId  = empty($this->data['roomId']) ? '' : $this->data['roomId'];
        //进入房间
        $state = $this->enterRoom($roomId);
        if($state!="ok"){
            $cls = [];$cls[$this->getConnectionId()] = 1;
            $message = ['event' => 'roomerror','state' => $state];
            return $this->reply($message, $cls);
        }else{
            return $this->reply(self::broadcast($roomId, 'join_room'), self::getConnections($roomId));
        }
        // return $this->reply(self::broadcast($roomId, 'join_room'), self::getConnections($roomId));
    }

    /**
     * 离开房间
     */
    public function leave()
    {
        $connectionId = $this->getConnectionId();
        self::deleteMatching($connectionId);
        $openId = $this->getOpenId();
        $roomId = $this->getRoomId();
        //离开房间
        if(isset(self::$roomList[$roomId]))
        {
            if(isset(self::$roomList[$roomId]['connectionIds'][$openId]))
                unset(self::$roomList[$roomId]['connectionIds'][$openId]);
            if(self::$roomList[$roomId]['status'] == 0 && isset(self::$roomList[$roomId]['playersInfo'][$openId]))
                unset(self::$roomList[$roomId]['playersInfo'][$openId]);
            if(self::$roomList[$roomId]['status'] == 0 && isset(self::$roomList[$roomId]['readyStatus'][$openId]))
                unset(self::$roomList[$roomId]['readyStatus'][$openId]);

        }

        if(isset(self::$connectionIdToRoomId[$connectionId]))
            unset(self::$connectionIdToRoomId[$connectionId]);

        self::deleteRoom($roomId,$connectionId);

        $message = [
            'event' => 'leave_room',
            'openId' => $openId
        ];
        return $this->reply($message, self::getConnections($roomId));
    }

    public function status()
    {
        $message = [];
        if(isset($this->data['status']) && $this->data['status'] == 112)
        {
            $total = count(self::$roomList);
            $list = [];
            if($total > 0)
                $list = array_keys(self::$roomList);
            $message = ['event' => 'status_112', 'count' => $total, 'list' => $list];

        }
        if(isset($this->data['status']) && $this->data['status'] == 113 && isset($this->data['roomId']))
        {
            $roomId = $this->data['roomId'];

            if(isset(self::$roomList[$roomId]))
                $message = ['event' => 'status_113', 'info' => self::$roomList[$roomId]];

        }
        return $this->reply($message, []);
    }

    /**
     * 进入房间
     */
    private function enterRoom($roomId)
    {
        if(empty($roomId)){
            return false;
            // throw new \Exception("parameter[roomId] error");
        }

        //房间不存在
        if(!isset(self::$roomList[$roomId])) return "noroom";
            // throw new \Exception("room is not exists");

        if(self::$roomList[$roomId]['status'] != 0) return "playing";
            // throw new \Exception("game is already started");

        //已有人数
        $total = count(self::$roomList[$roomId]['connectionIds']);
        //房间已满
        if($total >= self::$roomList[$roomId]['maxPlayer']) return "maxplayer";
            // throw new \Exception("room is full");

        $connectionId = $this->getConnectionId();

        $openId = $this->getOpenId();

        self::$roomList[$roomId]['connectionIds'][$openId] = $connectionId;
        self::$roomList[$roomId]['playersInfo'][$openId] = [
            'name' => empty($this->data['name']) ? '' : $this->data['name'],
            'avatar' => empty($this->data['avatar']) ? '' : $this->data['avatar'],
            'order' => $total,
            'isDead' => 0,
            'isReady' => 0,
            'score' => 0
        ];
        self::$roomList[$roomId]['updateTime'] = time();
        self::$connectionIdToRoomId[$connectionId] = $roomId;

        echo "roomId:".$roomId." connectionId:".$connectionId."\r\n";

        $openId = $this->getOpenId();
        if(!isset(self::$playerMapWx[$openId])){
            $mv = array("openId"=>$openId,"name"=>$this->data['name'],"avatar"=>$this->data['avatar'],
                "cId"=>$this->getConnectionId());
            self::$mCMapWx[$mv['cId']] = $mv;
            self::$playerMapWx[$openId] = $mv;

        }else{
            self::$playerMapWx[$openId]['stime'] = $this->data['stime'];
        }
        self::$roomCls[$roomId][$openId] = 1;
        $rplen = count(self::$roomCls[$roomId]);
        if($rplen==2){
            $mid = [];$mvs = [];
            foreach(self::$roomCls[$roomId] as $roid=>$rvalue){
                $mid[] = $roid;
            }
            $mv = self::$playerMapWx[$mid[0]];
            $mp = self::$playerMapWx[$mid[1]];
            $mv['cIdO'] = $mp['cId'];
            $mp['cIdO'] = $mv['cId'];
        }
        return "ok";
    }

    /**
     * 准备
     */
    public function ready()
    {
        $roomId = $this->getRoomId();
        $openId = $this->getOpenId();

        //玩家准备
        if(self::$roomList[$roomId]['status'] > 0)
            throw new \Exception("game is already started");

        if(count(self::$roomList[$roomId]['readyStatus']) >= self::$roomList[$roomId]['maxPlayer'])
            throw new \Exception("room is full");

        self::$roomList[$roomId]['readyStatus'][$openId] = $this->getConnectionId();
        self::$roomList[$roomId]['updateTime'] = time();
        self::$roomList[$roomId]['playersInfo'][$openId]['isReady'] = 1;

        $rplen = count(self::$roomCls[$roomId]);
        self::$roomCls[$roomId][$openId] = 2;//开始
        // echo "Php Ready openId:$openId \r\n";
        $readc = 0;
        foreach(self::$roomCls[$roomId] as $roid=>$rvalue){
            // echo "Php Ready roid:$roid rvalue:$rvalue\r\n";
            if($rvalue==2){$readc++;}
        }
        // echo "Php Ready $rvalue \r\n";
        if($readc==2){
            self::$roomList[$roomId]['updateTime'] = time();
            $msg = self::broadcast($roomId, 'game_start');
            $keys = [];$rcls = self::$roomCls[$roomId];
            foreach($rcls as $key=>$value){$keys[] = $key;}
            $mv = self::$playerMapWx[$keys[0]];
            $mp = self::$playerMapWx[$keys[1]];
            
            $msg["playersInfo"][$keys[0]]['cIdO'] = $mp['cId'];
            $msg["playersInfo"][$keys[1]]['cIdO'] = $mv['cId'];
            if(self::$isEcho){
                echo "xgq pIO---:".$mp['cId']." ".$mv['cId']." \r\n";
                echo "Xgq game_start 1.....\r\n";
                var_dump($msg);
            }
            return $this->reply($msg, self::getConnections($roomId));
        }
        return $this->reply(self::broadcast($roomId, 'ready'), self::getConnections($roomId));
    }

    /**
     * 取消准备
     */
    public function no_ready()
    {
        $roomId = $this->getRoomId();
        $openId = $this->getOpenId();

        //玩家取消准备
        if(self::$roomList[$roomId]['status'] > 0)
            throw new \Exception("game is already started");

        unset(self::$roomList[$roomId]['readyStatus'][$openId]);
        self::$roomList[$roomId]['updateTime'] = time();
        self::$roomList[$roomId]['playersInfo'][$openId]['isReady'] = 0;

        return $this->reply(self::broadcast($roomId, 'set_ready_status'), self::getConnections($roomId));
    }

    /**
     * 获取roomId
     */
    private function getRoomId()
    {
        $connectionId = $this->getConnectionId();
        if(!isset(self::$connectionIdToRoomId[$connectionId]))
            throw new \Exception("player is not in room");

        $roomId = self::$connectionIdToRoomId[$connectionId];
        //房间不存在
        if(!isset(self::$roomList[$roomId]))
            throw new \Exception("room is not exists");

        return $roomId;
    }

    private static function getRoomIdFromConnectionId($connectionId)
    {
        // $roomId = self::$connectionIdToRoomId[$connectionId];
        if(!isset(self::$connectionIdToRoomId[$connectionId]))
        {
            return -1;
            // throw new \Exception("player is not in room");
        }

        $roomId = self::$connectionIdToRoomId[$connectionId];
        //房间不存在
        if(!isset(self::$roomList[$roomId]))
            throw new \Exception("room is not exists");

        return $roomId;
    }

    /**
     * 初始房间数据
     */
    private function initRoom($roomId, $maxPlayer, $type = 0)
    {
        self::$roomList[$roomId] = [
            'type' => $type, //房间类型 0 邀请对战 1 匹配对战
            'status' => 0, //房间状态 0 未开始 1 开始 2 结束
            'maxPlayer' => $maxPlayer, 
            'connectionIds' => [], 
            'playersInfo' => [], //玩家数据
            'readyStatus' => [], //准备好的玩家
            'deadStatus' => [], //死掉的玩家
            'createTime' => time(),
            'updateTime' => time(),
        ];
    }

    /**
     * 随机获取房间
     */
    public static function getRandomRoom($type = 0)
    {
        $roomId = '';
        //房间是不是有空位
        foreach(self::$roomList as $id => $info)
        {
            if($info['type'] != $type || $info['status'] != 0)
                continue;
            if(count($info['connectionIds']) < $info['maxPlayer'])
                $roomId = $id;
        }
        //没空房间，返回新的房间ID
        if($roomId == '')
            $roomId = uniqid();
        return $roomId;
    }

    /**
     * 返回房间内的所有用户连接
     */
    public static function getConnections($roomId)
    {
        if(!isset(self::$roomList[$roomId]))
            return [];
        else{
            $map = [];
            foreach (self::$roomList[$roomId]['connectionIds'] as $key => $value) {
                $map[$value] = 1;
            }
            return $map;
            // return self::$roomList[$roomId]['connectionIds'];
        }
    }

    /**
     * 删除房间内的用户连接
     */
    public static function deleteConnection($connectionId)
    {
        echo "==================deleteConnectionx $connectionId\r\n";
        $other = self::deleteMatching($connectionId);
        self::deleteWxMatch($connectionId);
        $ocount = count($other);
        if($ocount>0){
            echo "deleteConnection $ocount\r\n";
            $msg = [
                'event' => 'leave_room'
            ];
            foreach ($other as $value) {
                $cls[$value] = 1;
            }
            return [$msg, $cls];
        }
        $roomId = self::getRoomIdFromConnectionId($connectionId);
        if($roomId==-1) return;
        $msg = null;$cls = null;
        if(isset(self::$connectionIdToRoomId[$connectionId]))
        {
            $roomId = self::$connectionIdToRoomId[$connectionId];
            unset(self::$connectionIdToRoomId[$connectionId]);
            //如果房间还没被强制回收
            if(isset(self::$roomList[$roomId]))
            {
                $openId = array_search($connectionId, self::$roomList[$roomId]['connectionIds']);
                if($openId !== false)
                {
                    if(isset(self::$roomCls[$roomId][$openId])){
                        unset(self::$roomCls[$roomId][$openId]);
                    }
                    if(isset(self::$roomList[$roomId]['connectionIds'][$openId]))
                        unset(self::$roomList[$roomId]['connectionIds'][$openId]);
                    if(self::$isEcho) echo "onClose roomList $roomId:".self::$roomList[$roomId]['status']."\r\n";
                    // if(self::$roomList[$roomId]['status'] == 0)
                    {
                        if(isset(self::$roomList[$roomId]['playersInfo'][$openId]))
                            unset(self::$roomList[$roomId]['playersInfo'][$openId]);
                        if(isset(self::$roomList[$roomId]['readyStatus'][$openId]))
                            unset(self::$roomList[$roomId]['readyStatus'][$openId]);
                        $message = [
                            'event' => 'leave_room',
                            'openId' => $openId
                        ];
                        $msg = $message;$cls = self::getConnections($roomId);
                        
                    }
                }
            }
        }
        self::deleteRoom($roomId,$connectionId);
        return [$msg, $cls];
    }

    public static function broadcast($roomId, $event)
    {
        $data = self::$roomList[$roomId];
        return [
            'event' => $event,
            'roomId' => $roomId,
            'gameStatus' => $data['status'],
            'playersInfo' => $data['playersInfo'],
            'readyOpenId' => array_keys($data['readyStatus']),
        ];
    }

    /**
     * 定时器回调方法
     */
    public static function gameStatus($roomId)
    {
        $status = self::$roomList[$roomId]['status'];
        switch($status)
        {
            case 0:
                //准备游戏，如果准备玩家大于等于最少开始数量，游戏开始
                if(count(self::$roomList[$roomId]['readyStatus']) >= self::MIN_ROOM_PLAYER)
                {
                    if(!isset(self::$roomList[$roomId]['readyStartTime']))
                        self::$roomList[$roomId]['readyStartTime'] = time();

                    if(time() - self::$roomList[$roomId]['readyStartTime'] > 2)
                    {
                        self::$roomList[$roomId]['status'] = 1;//开始
                        self::$roomList[$roomId]['updateTime'] = time();
                        return [self::broadcast($roomId, 'game_start'), self::getConnections($roomId)];
                    }
                }
                else
                {
                    //如果用户都退出或者超过1分钟没消息，回收
                    if(count(self::$roomList[$roomId]['connectionIds']) == 0 || time() - self::$roomList[$roomId]['updateTime'] > 120)
                    {
                        GameServer::getInstance()->removeTimer($roomId);
                        unset(self::$roomList[$roomId]);
                    }
                    return [null, null];
                }
                break;
            case 1:
                //游戏中，如果玩家都死掉了
                if(count(self::$roomList[$roomId]['readyStatus']) == count(self::$roomList[$roomId]['deadStatus']) || count(array_diff(self::$roomList[$roomId]['connectionIds'], self::$roomList[$roomId]['deadStatus'])) == 0)
                {
                    self::$roomList[$roomId]['status'] = 2;//结束
                    self::$roomList[$roomId]['updateTime'] = time();
                    return [self::broadcast($roomId, 'game_over'), self::getConnections($roomId)];
                }
                else
                {
                    //如果用户都退出或者超过1分钟没消息，回收
                    if(count(self::$roomList[$roomId]['connectionIds']) == 0 || time() - self::$roomList[$roomId]['updateTime'] > 60)
                    {
                        GameServer::getInstance()->removeTimer($roomId);
                        unset(self::$roomList[$roomId]);
                    }
                    return [null, null];
                }
                break;
            case 2:
                //游戏结束，如果无连接或者结束时间超过1分钟，回收
                if(count(self::$roomList[$roomId]['connectionIds']) == 0 || time() - self::$roomList[$roomId]['updateTime'] > 60)
                {
                    GameServer::getInstance()->removeTimer($roomId);
                    unset(self::$roomList[$roomId]);
                }
                return [null, null];
                break;
            default:
                return [null, null];
        }
    }
}