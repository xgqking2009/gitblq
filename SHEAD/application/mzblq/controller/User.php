<?php

namespace app\mzblq\controller;

use WXBizDataCrypt\WXBizDataCrypt;

// $trace = debug_backtrace(); $tracestr = json_encode($trace,JSON_FORCE_OBJECT); 
// file_put_contents("log.txt", "test：$city\r\n".$tracestr."\r\n",FILE_APPEND); 
function debug($msg){
    // file_put_contents("log.txt", "$msg\r\n",FILE_APPEND);
}

class User extends Base {

    public function __construct()
    {
        parent::__construct();
    }

    public static $clientCount = 0;
    //登录 
    public function login()
    {
        session_start();
        $data = input("post.data");
        $data = json_decode($data, true);

        if(empty($data) || empty($data['gameData']) || empty($data['code']) || empty($data['encryptedData']) || empty($data['iv']))
            return json(['code' => 1, 'msg' => '参数错误']);
        $code = $data['code'];
        if($code=='123456'){
            $gameData = $data['gameData'];
        }else{
            $encryptedData = $data['encryptedData'];
            $gameData = $data['gameData'];
            $gameData = base64_decode($gameData);
            //解密游戏数据
            $gameData = $this->decrypt($gameData);
        }
        $maxscoretimekey = $this->redisCachePrefix.'maxscoretime';
        $oldd = $this->redis->get($maxscoretimekey);
        $nowd = intval(time()/604800);//604800
        if(!isset($oldd)||$nowd>$oldd){
            $this->redis->set($maxscoretimekey,$nowd);
            $this->redis->del($this->redisCachePrefix.'score');
        }
        // var_dump($gameData);

        if(empty($gameData))
            return json(['code' => 1, 'msg' => '提交数据失败']);
        $gameDataArr = json_decode($gameData, true);
        $score = isset($gameDataArr['maxscore'])?$gameDataArr['maxscore']:0;
        $scoreEndless = isset($gameDataArr['maxscoreI']) ? $gameDataArr['maxscoreI'] : 0;

        $iv = $data['iv'];
        $maxcity = 20;
        $city = rand(0, $maxcity-1);
        if($code=='123456'){
            $luser = json_decode($iv,true);
            $openId = $luser[0];
            $nickName = $luser[1];
            $avatarUrl = $luser[2];
            // $city = intval(substr($luser[1],4));
            // $city = $city%$maxcity;
            $province = '福建';
            $country = '';
        }else{
            $param = array( 
            'appid' => $this->appID, 
            'secret' => $this->appSecret, 
            'js_code' => $code, 
            'grant_type' => 'authorization_code'
            ); 
            //用户登录凭证（有效期五分钟）
            $res = http("https://api.weixin.qq.com/sns/jscode2session", $param, 'post'); 
            $arr = json_decode($res, true); 
            // var_dump($arr);

            if(isset($arr['errcode']) && $arr['errcode'] != 0)
                return json(['code' => $arr['errcode'], 'msg' => $arr['errmsg']]);

            $crypt = new WXBizDataCrypt($this->appID, $arr['session_key']);
            $userData = [];
            $errCode = $crypt->decryptData($encryptedData, $iv, $userData);
            if ($errCode != 0)
                return json(['code' => 1, 'msg' => '解密用户信息失败']);
            //用户信息解密成功
            $datas = json_decode($userData, true);
            $avatarUrl = $datas['avatarUrl'];
            // $city = $datas['city'];
            $province = $datas['province'];
            $country = $datas['country'];
            $nickName = $this->filterNickname($datas['nickName']);
            $openId = $datas['openId'];
        }
        //是否更新排行榜
        $updateRank = 0;
        $data = [];

        $key = $this->redisCachePrefix.'user:'.$openId;
        $info = $this->redis->hmget($key, ['city','nickName', 'openId', 'avatarUrl', 'gameData', 'signTime', 'signCount','chouTime','jsendTime']);

        if(empty($info['openId']))
        {
            $updateRank = 1;
            $data['gameData'] = $gameData;
            $data['createdAt'] = time();
            $data['updatedAt'] = 0;
            $data['flag'] = 0;
            $data['signTime'] = 0;
            $data['signCount'] = 0;
            $data['new'] = 1;
            $data['invlist'] = [];
            $data['chouTime'] = 0;
        }
        else
        {
            //头像和昵称是否更改，移除旧的排行榜
            if($info['avatarUrl'] != $avatarUrl || $info['nickName'] != $nickName)
            {
                $this->redis->zrem($this->redisCachePrefix.'rank', json_encode(['avatarUrl' => $info['avatarUrl'], 'nickName' => $info['nickName']]));
                $this->redis->zrem($this->redisCachePrefix.'rank_endless', json_encode(['avatarUrl' => $info['avatarUrl'], 'nickName' => $info['nickName']]));
            }
            $serverGameDataArr = json_decode($info['gameData'], true);
            if(!empty($info['city'])) $city = $info['city'];
            $serverGameDataArr['maxscore'] = isset($serverGameDataArr['maxscore'])?$serverGameDataArr['maxscore']:0;
            $serverGameDataArr['maxscoreI'] = isset($serverGameDataArr['maxscoreI']) ? $serverGameDataArr['maxscoreI'] : 0;
            //是否存在未提交的数据，总分是否大于服务器的分数
            if($score > $serverGameDataArr['maxscore'] || $scoreEndless > $serverGameDataArr['maxscoreI'])
            {
                $data['gameData'] = $gameData;
                // debug("up...:".json_encode($data['gameData']));
                $updateRank = 1;
            }
            else{
                // echo 'Down.....';
                $score = $serverGameDataArr['maxscore'];
                $scoreEndless = $serverGameDataArr['maxscoreI'];
                $updateRank = 1;
                $data['gameData'] = $info['gameData'];
                // debug("down...:".json_encode($data['gameData']));
            }
            $data['updatedAt'] = time();
            $data['new'] = 0;
            $invlistkey = $this->redisCachePrefix.'userinv:'.$openId;
            $invlist = $this->redis->lrange($invlistkey,0,-1);
            $data['invlist'] = $invlist;
            if(empty($avatarUrl))
            {
                $avatarUrl = $info['avatarUrl'];
            }
        }
        $data['openId'] = $openId; $data['avatarUrl'] = $avatarUrl; $data['nickName'] = $nickName;
        $data['province'] = $province; $data['country'] = $country;  
        $data['time'] = 1540224000;
        $data['city'] = $city;
        $ret = $this->redis->hmset($key, $data);
        if(!$ret)
            return json(['code' => 1, 'msg' => '保存用户信息失败']);
        //更新排行榜
        if($updateRank)
        {
            // echo 'updateRank $score $scoreEndless';
            $ukey = json_encode(['avatarUrl' => $avatarUrl, 'nickName' => $nickName]);
            $this->rankZAdd($score, $scoreEndless,$ukey);
            $this->rankZAddCity($city,$score, $scoreEndless,$ukey);
        }

        //用户游戏数据
        // echo '----start-----'.$updateRank;
        // var_dump($data);
        // echo '----end-----';
        $data['gameData'] = json_decode($data['gameData'],true);
        //全局游戏功能设置
        $data['gameConfig'] = $this->gameConfig;
        //今天是否签到
        // echo $info['signTime'];
        $data['isSignIn'] = !empty($info['signTime']) && date('Ymd', $info['signTime']) == date('Ymd') ? 1 : 0;
        //$data['isSignIn'] = !empty($info['signTime']) && $info['signTime'] > time() - 60 ? 1 : 0;
        $chou = 0;$chouTime = 0;
        if(!isset($data['gameData'])
            ||!isset($data['gameData']['sgame'])
            ||!isset($data['gameData']['sgame']['chou'])||!isset($info['chouTime'])){
            $chou = 0;
        }else{
            $chou = date('Ymd', $info['chouTime']) == date('Ymd');
            $chouTime = $info['chouTime'];
        }
        
        $data['gameData']['sgame']['chou'] = $chou;
        $data['chouTime'] = $chouTime;
        $jsend = 0;$jsendTime = 0;
        if(!isset($data['gameData'])
            ||!isset($data['gameData']['sgame'])
            ||!isset($data['gameData']['sgame']['jsend'])||!isset($info['jsendTime'])){
            $jsend = 0;
        }else{
            if(date('Ymd', $info['jsendTime']) == date('Ymd'))
                $jsend = 1;
            else 
                $jsend = 0;
            $jsendTime = $info['jsendTime'];
        }
        $data['gameData']['sgame']['jsend'] = $jsend;
        $data['jsendTime'] = $jsendTime;
        // echo $data['isSignIn'];
        $data['signCount'] = 0;
        $data['city'] = $city;

        if($data['isSignIn'] == 1 || date('Ymd', $info['signTime']) == date("Ymd", strtotime("-1 day")))
            $data['signCount'] = $info['signCount'];

        return json(['code' => 0, 'msg' => '用户登录成功', 'data' => $data]);
    }

    public function choufree(){
        $data = input("post.data");
        $arr = json_decode($data, true);
        $openId = $arr['openId'];
        $key = $this->redisCachePrefix.'user:'.$openId;

        $info = $this->redis->hmget($key, ['gameData']);
        if(!empty($info['gameData'])){
            $gameData = json_decode($info['gameData'],true);
            $gameData['sgame']['chou'] = 0;
            $this->redis->hset($key,'gameData',json_encode($gameData));
            $this->redis->hset($key,'chouTime',time());
        }
    }
 
    private function getRankScore($rankKey,$openId){
        $key = $this->redisCachePrefix.'user:'.$openId;
        $userInfo = $this->redis->hmget($key, ['avatarUrl', 'nickName']);
        if(empty($userInfo['avatarUrl']) || empty($userInfo['nickName']))
            return array('rank'=>-1,'score'=>0);
        $userRank = $this->redis->zrevrank($this->redisCachePrefix.$rankKey, json_encode($userInfo));
        debug("$userRank");
        $userScore = $this->redis->zscore($this->redisCachePrefix.$rankKey, json_encode($userInfo));
        return array('rank'=>$userRank,'score'=>$userScore);
    }

    public function getRankPW(){
        debug("getRankPW");
        $data = input("post.data");
        $arr = json_decode($data, true);
        $openId = isset($arr['openId']) ? $arr['openId'] : '';
        $rdata['mrank'] = $this->getRankScore('rank',$openId);
        $rdata['rank12'] = $this->getRankScore12('rank');
        return json(['code' => 0, 'msg' => '获取排位', 'data' => $rdata]);
    }
    private function getRankScore12($rankKey){
        $lists = $this->redis->zrevrange($this->redisCachePrefix.$rankKey, 0, 2, true);
        $num = 0;$rank = [];
        foreach($lists as $key => $score){
            $num++; $info = []; $info = json_decode($key, true); $info['score'] = $score; $info['rank'] = $num; $rank[] = $info; 
        }
        return $rank;
    }
    private function rankZAdd($score,$star,$key){
        $this->redis->zadd($this->redisCachePrefix.'rank', $star*100000000+$score, $key);
    }

    private function rankZAddCity($city,$score,$star,$key){
        $this->redis->zadd($this->redisCachePrefix.'city:'.$city, $star*100000000+$score, $key);
    }

    private function rankZAddScore($score,$star,$key){
        $this->redis->zadd($this->redisCachePrefix.'score', $score*100000000+$star, $key);
    }
    //过滤nickname 标签
    public function filterNickname($nickname) {
        $nickname = preg_replace('/[\x{1F600}-\x{1F64F}]/u', '', $nickname);
        $nickname = preg_replace('/[\x{1F300}-\x{1F5FF}]/u', '', $nickname);
        $nickname = preg_replace('/[\x{1F680}-\x{1F6FF}]/u', '', $nickname);
        $nickname = preg_replace('/[\x{2600}-\x{26FF}]/u', '', $nickname);
        $nickname = preg_replace('/[\x{2700}-\x{27BF}]/u', '', $nickname);
        $nickname = str_replace(array('"', '\''), '', $nickname);
        return trim($nickname);
    }
    //提交
    public function submitData()
    {

        $data = input("post.data");
        $arr = json_decode($data, true);

        if(empty($arr) || empty($arr['gameData']) || empty($arr['openId']))
            return json(['code' => 1, 'msg' => '参数错误']);

        $openId = $arr['openId'];
        $gameData = $arr['gameData'];
        $gameData = base64_decode($gameData);
        //解密游戏数据
        $gameData = $this->decrypt($gameData);
        if(empty($gameData))
            return json(['code' => 1, 'msg' => '提交数据失败']);

        $gameDataArr = json_decode($gameData, true);

        $score = isset($gameDataArr['maxscore'])?$gameDataArr['maxscore']: 0;
        $scoreEndless = isset($gameDataArr['maxscoreI']) ? $gameDataArr['maxscoreI'] : 0;
        $roundscore = isset($gameDataArr['roundscore']) ? $gameDataArr['roundscore'] : -1;
        $star = $scoreEndless;

        $key = $this->redisCachePrefix.'user:'.$openId;
        $info = $this->redis->hmget($key, ['city','avatarUrl', 'nickName']);
        if(empty($info['avatarUrl']) || empty($info['nickName']))
            return json(['code' => 1, 'msg' => '用户信息不存在']);
        $city = isset($info['city'])?$info['city']:0;
        $data = [];
        $data['gameData'] = $gameData;
        $data['updatedAt'] = time();

        $ret = $this->redis->hmset($key, $data);

        if(!$ret)
            return json(['code' => 1, 'msg' => '提交信息失败']);
        //更新排行榜
        $data = [];
        $data['avatarUrl'] = $info['avatarUrl'];
        $data['nickName'] = $info['nickName'];
        $ukey = json_encode($data);
        $this->rankZAdd($score, $scoreEndless,$ukey);
        $this->rankZAddCity($city, $score, $scoreEndless,$ukey);
        $skey = $this->redisCachePrefix.'score';
        $vold = $this->redis->zscore($skey,$ukey);
        if($roundscore!=-1){
            if(isset($vold)){
                $vnow = $roundscore*100000000+$star;
                if($vnow>$vold){
                    $this->rankZAddScore($roundscore,$star,$ukey);
                }
            }else{
                $this->rankZAddScore($roundscore,$star,$ukey);
            } 
        }
        return json(['code' => 0, 'msg' => '提交信息成功']);
    }
    public function heart(){
        $data = input("post.data"); $arr = json_decode($data, true);
        $openId = isset($arr['openId']) ? $arr['openId'] : '';
        $rdata = [];
        $invkey = $this->redisCachePrefix.'invlist';
        $invedId = $this->redis->hget($invkey,$openId);
        if(!empty($invedId)){
            debug("no empty($invedId)");
            $rdata['state'] = 'inved';
            $invedkey = $this->redisCachePrefix.'user:'.$invedId;
            $info = $this->redis->hmget($invedkey,['nickName' , 'avatarUrl']);
            if(!empty($info)){
                $rdata['avatarUrl'] = $info['avatarUrl'];
                $rdata['nickName'] = $info['nickName'];
                $invlistkey = $this->redisCachePrefix.'userinv:'.$openId;
                $avatarUrl = $info['avatarUrl'];$name = $info['nickName'];
                debug("$avatarUrl $name");
                // $this->redis->lpush($invlistkey,$info['avatarUrl'],$info['nickName'],0);
                $this->redis->lpush($invlistkey,0,$info['nickName'],$info['avatarUrl']);
                $rdata['state'] = 'invlist';
                $invlist = $this->redis->lrange($invlistkey,0,-1);
                foreach ($invlist as $value) {
                    debug("value-----".$value);
                }
                $rdata['invlist'] = $invlist;
            }
            $this->redis->hdel($invkey,$openId);
        }
        return json(['code' => 0, 'msg' => 'heart', 'data' => $rdata]);
    }
    public function matchingMachine(){
        $data = input("post.data"); $arr = json_decode($data, true);
        $avatarUrl = isset($arr['avatarUrl']) ? $arr['avatarUrl'] : '';
        $rkey = $this->redisCachePrefix.'rank';
        $count = $this->redis->zcard($rkey);
        $m = 0;
        // return json(['code' => 1, 'msg' => '']);
        for($k = 0;$k<10;$k++){
            $rd = rand(0, $count-1);
            $lists = $this->redis->zrange($rkey, $rd, $rd, true);
            foreach ($lists as $key => $value) {
                $info = json_decode($key, true);
                if($info['avatarUrl']!=$avatarUrl){
                    return json(['code' => 0, 'msg' => '','data'=>array('ourl' =>$info['avatarUrl'],'txtAv'=>$info['nickName'])]);
                }
            }
        }
        return json(['code' => 1, 'msg' => '']);
    }
    public function invited(){
        
        $data = input("post.data"); $arr = json_decode($data, true);
        $invId = isset($arr['invId']) ? $arr['invId'] : '';
        $invedId = isset($arr['invedId']) ? $arr['invedId'] : '';
        debug("invited $invId $invedId");
        $new = isset($arr['new']) ? $arr['new'] : '';
        $userkey = $this->redisCachePrefix.'user:'.$invedId;
        $invkey = $this->redisCachePrefix.'invlist';
        if($new){
            $invlistkey = $this->redisCachePrefix.'userinv:'.$invId;
            $invlen = $this->redis->llen($invlistkey);
            if($invlen<12){
                $this->redis->hset($invkey,$invId,$invedId);
            }
        }
    }
    public function invget(){
        $data = input("post.data"); $arr = json_decode($data, true);
        $openId = isset($arr['openId']) ? $arr['openId'] : '';
        $v = isset($arr['v']) ? $arr['v'] : '';
        $invlistkey = $this->redisCachePrefix.'userinv:'.$openId;
        $this->redis->lset($invlistkey,$v*3+2,1);
    }
    //获取排行榜
    public function getRank()
    {
        $data = input("post.data"); $arr = json_decode($data, true); $top = 100; $openId = isset($arr['openId']) ? $arr['openId'] : '';
        $type = isset($arr['type']) ? intval($arr['type']) : 0; //-1 世界 0-？城市
        $rank = [];
        if($type==-1)
            $rankKey = 'rank';
        else
            $rankKey = 'score';
        // $rankKey = $type == -1 ? 'rank' : 'city:'.$type;
        //查询自己的名次
        if(!empty($openId))
        {
            $rank[] = $this->getRankScore($rankKey,$openId);
        }
        $rkey = $this->redisCachePrefix.$rankKey;
        $lists = $this->redis->zrevrange($rkey, 0, $top, true);
        $num = 0;
        foreach($lists as $key => $score)
        {
            $num++;
            $info = [];
            $info = json_decode($key, true);
            $info['score'] = $score;
            $info['rank'] = $num;
            $rank[] = $info;
        }
        return json(['code' => 0, 'msg' => '获取排行榜成功', 'data' => $rank]);
    }
    //签到
    public function sign()
    {
        $data = input("post.data");
        $arr = json_decode($data, true);
        if(empty($arr) || empty($arr['openId']))
            return json(['code' => 1, 'msg' => '参数错误']);

        $openId = $arr['openId'];

        $key = $this->redisCachePrefix.'user:'.$openId;
        $info = $this->redis->hmget($key, ['openId', 'signTime', 'signCount']);
        if(empty($info['openId']) || $info['openId'] != $openId)
            return json(['code' => 1, 'msg' => '提交数据失败']);

        $data = [];
        if(!empty($info['signTime']) && date('Ymd', $info['signTime']) == date('Ymd'))
        //if(!empty($info['signTime']) && $info['signTime'] > time() - 60)
        {
            $data['signTime'] = $info['signTime'];
            $data['signCount'] = $info['signCount'];
            return json(['code' => 0, 'msg' => '提交信息成功', 'data' => $data]);
        }

        if(empty($info['signTime']) || date('Ymd', $info['signTime']) != date("Ymd", strtotime("-1 day")) || $info['signCount'] == 7)
        // if(empty($info['signTime']) || $info['signTime'] < time() - 120 || $info['signCount'] == 7)
        {
            $data['signTime'] = time();
            $data['signCount'] = 1;
        }
        else
        {
            $data['signTime'] = time();
            $data['signCount'] = $info['signCount'] + 1;
        }
        $ret = $this->redis->hmset($key, $data);
        if(!$ret) return json(['code' => 1, 'msg' => '提交信息失败']);
        return json(['code' => 0, 'msg' => '提交信息成功', 'data' => $data]);
    }

    public function game0d()
    {
        $data = input("post.data");
        $arr = json_decode($data, true);
        if(empty($arr) || empty($arr['openId']))
            return json(['code' => 1, 'msg' => '参数错误']);
        $openId = $arr['openId'];
        $key = $this->redisCachePrefix.'user:'.$openId;
        $info = $this->redis->hmget($key, ['openId', 'jsendTime']);
        if(empty($info['openId']) || $info['openId'] != $openId)  return json(['code' => 1, 'msg' => '提交数据失败']);
        $data = [];
        if(!empty($info['jsendTime']) && date('Ymd', $info['jsendTime']) == date('Ymd'))
        //if(!empty($info['signTime']) && $info['signTime'] > time() - 60)
        {
            $data['jsendTime'] = $info['jsendTime'];
            return json(['code' => 0, 'msg' => '提交信息成功', 'data' => $data]);
        }
        if(empty($info['jsendTime']) || date('Ymd', $info['jsendTime']) != date("Ymd"))
        {
            $data['jsendTime'] = time();
        }
        $ret = $this->redis->hmset($key, $data);
        if(!$ret) return json(['code' => 1, 'msg' => '提交信息失败']);
        return json(['code' => 0, 'msg' => '提交信息成功', 'data' => $data]);
    }

    public function zcardRank() {$count = $this->redis->zcard($this->redisCachePrefix.'rank'); return json(['count' => $count]); }
    public function zremRank(){  $xkey = input("post.xkey"); if(strpos($xkey, 'xx') == 0) $this->redis->zrem($this->redisCachePrefix.'rank', substr($xkey, 2)); }
    public function zremRankEndless(){ $xkey = input("post.xkey"); if(strpos($xkey, 'xx') == 0) $this->redis->zrem($this->redisCachePrefix.'rank_endless', substr($xkey, 2)); }
    
 
}