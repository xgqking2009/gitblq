<?php

namespace app\mzblq\controller;

use Workerman\Lib\Timer;

class Gameserver
{
    private static $instance = null;
    private $connectionList = [];
    private $timerCallbackList = [];

    public static function getInstance()
    {
        if(is_null(self::$instance))
            self::$instance = new self();

        return self::$instance;
    }

    /**
     * 当连接建立时触发的回调函数
     * @param $connection
     */
    public function onConnect($connection)
    {
        $this->connectionList[$connection->id] = $connection;
    }

    /**
     * 收到信息
     * @param $connection
     * @param $data
     */
    public function onMessage($connection, $data)
    {
        $data = json_decode($data, true);
        try
        {
            // var_dump($data);
            if(!isset($data['command']))
                throw new \Exception("command not exists!");

            if(!isset($data['action']))
                throw new \Exception("action not exists!");

            $command = $data['command'];
            $action = $data['action'];
            // echo "start [".$command.'.'.$action."]\r\n";var_dump($data);
            // throw new \Exception("action:".$action);
            list($message, $connectionIds) = $this->doCommand($connection, $command, $action, $data);
            // echo "end [".$command.'.'.$action."]".count($connectionIds)."\r\n";
            if(empty($connectionIds))
                $connectionIds = [$connection->id];
            
            foreach($connectionIds as $connectionId=>$value)
            {
                if (isset($this->connectionList[$connectionId]))
                    $this->connectionList[$connectionId]->send($this->getMessage($message));
            }
        }
        catch(\Exception $e)
        {
            $connection->send($this->getErrorMessage($e->getMessage()));
        }
    }

    /**
     * 当连接断开时触发的回调函数
     * @param $connection
     */
    public function onClose($connection)
    {
        Room::debug("onClose $connection->id");
        unset($this->connectionList[$connection->id]);
        list($message, $connectionIds) = Room::deleteConnection($connection->id);
        if(!empty($connectionIds))
        {
            foreach($connectionIds as $connectionId=>$value)
            {
                echo "onClose foreach $connectionId\r\n";
                foreach ($this->connectionList as $key => $value) {
                    echo "onClose connectionList $key\r\n";
                }
                if (isset($this->connectionList[$connectionId])){
                    echo "onClose $connectionId Send Start ======\r\n";
                    $this->connectionList[$connectionId]->send($this->getMessage($message));
                    echo "onClose $connectionId Send End ======\r\n";
                }
            }
        }
    }

    /**
     * 当客户端的连接上发生错误时触发
     * @param $connection
     * @param $code
     * @param $msg
     */
    public function onError($connection, $code, $msg)
    {
    }

    public function startTimer()
    {
        Timer::add(1, [$this, 'doTimer']);
    }

    public function addTimer($index, $callback)
    {
        $this->timerCallbackList[$index] = $callback;
    }

    public function removeTimer($index)
    {
        unset($this->timerCallbackList[$index]);
    }

    public function doTimer()
    {
        foreach($this->timerCallbackList as $index => $callback)
        {
            try
            {
                list($data, $connectionIds) = $callback();
                if(is_null($data))
                    continue;
            }
            catch(\Exception $e)
            {
                $this->removeTimer($index);
                continue;
            }
            foreach($connectionIds as $connectionId)
            {
                if(isset($this->connectionList[$connectionId]))
                    $this->connectionList[$connectionId]->send($this->getMessage($data));
            }
        }
    }

    private function doCommand($connection, $command, $action, $data)
    {
        $command = ucfirst($command);
        $className = "app\\mzblq\\controller\\".$command;
        if(!class_exists($className))
            throw new \Exception("command[".$command."] not found");

        $object = new $className($connection, $data);
        if(!method_exists($object, $action))
            throw new \Exception("action[".$action."] not found");

        return $object->$action();
    }

    private function getErrorMessage($message)
    {
        return json_encode([
            'code' => 1,
            'msg' => $message,
        ]);
    }

    private function getMessage($data)
    {
        return json_encode([
            'code' => 0,
            'msg' => 'success',
            'data' => $data
        ]);
    }
}