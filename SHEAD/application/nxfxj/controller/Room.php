<?php
namespace app\nxfxj\controller;

class Room extends Command
{
    const MIN_ROOM_PLAYER = 2;
    const MAX_ROOM_PLAYER = 32;

    public static $roomList = [];
    public static $robotList = [];
    public static $connectionIdToRoomId = [];

    public function __construct($connection, $data)
    {
        parent::__construct($connection, $data);

    }

    /**
     * 创建房间
     */
    public function create()
    {
        $roomId = uniqid();
        if(isset(self::$roomList[$roomId]))
            throw new \Exception('room already exists');

        //最大人数
        $maxPlayer = isset($this->data['maxPlayer']) && intval($this->data['maxPlayer']) > self::MIN_ROOM_PLAYER ? intval($this->data['maxPlayer']) : self::MIN_ROOM_PLAYER;
        $maxPlayer = $maxPlayer > self::MAX_ROOM_PLAYER ? self::MAX_ROOM_PLAYER : $maxPlayer;
        //初始化房间
        $this->initRoom($roomId, $maxPlayer);
        //进入房间
        $this->enterRoom($roomId);
        //增加定时器回调
        GameServer::getInstance()->addTimer($roomId, function() use ($roomId) { return self::gameStatus($roomId);});

        return $this->reply(self::broadcast($roomId, 'create_room'), self::getConnections($roomId));
    }

    public function match()
    {
        $roomId = self::getRandomRoom(1);

        //如果房间不存在，则初始化它
        if(!isset(self::$roomList[$roomId]))
        {
            //最大人数
            $maxPlayer = isset($this->data['maxPlayer']) && intval($this->data['maxPlayer']) > self::MIN_ROOM_PLAYER ? intval($this->data['maxPlayer']) : self::MIN_ROOM_PLAYER;
            $maxPlayer = $maxPlayer > self::MAX_ROOM_PLAYER ? self::MAX_ROOM_PLAYER : $maxPlayer;
            //初始化房间
            $this->initRoom($roomId, $maxPlayer, 1);
            //进入房间
            $this->enterRoom($roomId);
            //增加定时器回调
            GameServer::getInstance()->addTimer($roomId, function() use ($roomId) { return self::gameStatus($roomId);});
            //return $this->reply(self::broadcast($roomId, 'create_room'), self::getConnections($roomId));
        }
        else
        {
            //进入房间
            $this->enterRoom($roomId);
        }

        return $this->reply(self::broadcast($roomId, 'join_room'), self::getConnections($roomId));
    }

    /**
     * 加入房间
     */
    public function join()
    {
        $roomId  = empty($this->data['roomId']) ? '' : $this->data['roomId'];

        //进入房间
        $this->enterRoom($roomId);

        return $this->reply(self::broadcast($roomId, 'join_room'), self::getConnections($roomId));
    }

    /**
     * 离开房间
     */
    public function leave()
    {
        $connectionId = $this->getConnectionId();
        $openId = $this->getOpenId();
        $roomId = $this->getRoomId();

        //离开房间
        if(isset(self::$roomList[$roomId]))
        {
            if(isset(self::$roomList[$roomId]['connectionIds'][$openId]))
                unset(self::$roomList[$roomId]['connectionIds'][$openId]);
            if(self::$roomList[$roomId]['status'] == 0 && isset(self::$roomList[$roomId]['playersInfo'][$openId]))
                unset(self::$roomList[$roomId]['playersInfo'][$openId]);
            if(self::$roomList[$roomId]['status'] == 0 && isset(self::$roomList[$roomId]['readyStatus'][$openId]))
                unset(self::$roomList[$roomId]['readyStatus'][$openId]);
        }

        if(isset(self::$connectionIdToRoomId[$connectionId]))
            unset(self::$connectionIdToRoomId[$connectionId]);

        $message = [
            'event' => 'leave_room',
            'openId' => $openId
        ];
        return $this->reply($message, self::getConnections($roomId));
    }

    public function status()
    {
        $message = [];
        if(isset($this->data['status']) && $this->data['status'] == 112)
        {
            $total = count(self::$roomList);
            $list = [];
            if($total > 0)
                $list = array_keys(self::$roomList);
            $message = ['event' => 'status_112', 'count' => $total, 'list' => $list];

        }
        if(isset($this->data['status']) && $this->data['status'] == 113 && isset($this->data['roomId']))
        {
            $roomId = $this->data['roomId'];

            if(isset(self::$roomList[$roomId]))
                $message = ['event' => 'status_113', 'info' => self::$roomList[$roomId]];

        }
        return $this->reply($message, []);
    }

    /**
     * 进入房间
     */
    private function enterRoom($roomId)
    {
        if(empty($roomId))
            throw new \Exception("parameter[roomId] error");

        //房间不存在
        if(!isset(self::$roomList[$roomId]))
            throw new \Exception("room is not exists");

        if(self::$roomList[$roomId]['status'] != 0)
            throw new \Exception("game is already started");

        //已有人数
        $total = count(self::$roomList[$roomId]['connectionIds']);
        //房间已满
        if($total >= self::$roomList[$roomId]['maxPlayer'])
            throw new \Exception("room is full");

        $connectionId = $this->getConnectionId();

        $openId = $this->getOpenId();

        self::$roomList[$roomId]['connectionIds'][$openId] = $connectionId;
        self::$roomList[$roomId]['playersInfo'][$openId] = [
            'name' => empty($this->data['name']) ? '' : $this->data['name'],
            'avatar' => empty($this->data['avatar']) ? '' : $this->data['avatar'],
            'order' => $total,
            'isDead' => 0,
            'isReady' => 0,
            'score' => 0
        ];
        self::$roomList[$roomId]['updateTime'] = time();

        self::$connectionIdToRoomId[$connectionId] = $roomId;
    }

    /**
     * 准备
     */
    public function ready()
    {
        $roomId = $this->getRoomId();
        $openId = $this->getOpenId();

        //玩家准备
        if(self::$roomList[$roomId]['status'] > 0)
            throw new \Exception("game is already started");

        if(count(self::$roomList[$roomId]['readyStatus']) >= self::$roomList[$roomId]['maxPlayer'])
            throw new \Exception("room is full");

        self::$roomList[$roomId]['readyStatus'][$openId] = $this->getConnectionId();
        self::$roomList[$roomId]['updateTime'] = time();
        self::$roomList[$roomId]['playersInfo'][$openId]['isReady'] = 1;

        return $this->reply(self::broadcast($roomId, 'set_ready_status'), self::getConnections($roomId));
    }

    /**
     * 取消准备
     */
    public function no_ready()
    {
        $roomId = $this->getRoomId();
        $openId = $this->getOpenId();

        //玩家取消准备
        if(self::$roomList[$roomId]['status'] > 0)
            throw new \Exception("game is already started");

        unset(self::$roomList[$roomId]['readyStatus'][$openId]);
        self::$roomList[$roomId]['updateTime'] = time();
        self::$roomList[$roomId]['playersInfo'][$openId]['isReady'] = 0;

        return $this->reply(self::broadcast($roomId, 'set_ready_status'), self::getConnections($roomId));
    }

    /**
     * 获取roomId
     */
    private function getRoomId()
    {
        $connectionId = $this->getConnectionId();
        if(!isset(self::$connectionIdToRoomId[$connectionId]))
            throw new \Exception("player is not in room");

        $roomId = self::$connectionIdToRoomId[$connectionId];
        //房间不存在
        if(!isset(self::$roomList[$roomId]))
            throw new \Exception("room is not exists");

        return $roomId;
    }

    /**
     * 初始房间数据
     */
    private function initRoom($roomId, $maxPlayer, $type = 0)
    {
        self::$roomList[$roomId] = [
            'type' => $type, //房间类型 0 邀请对战 1 匹配对战
            'status' => 0, //房间状态 0 未开始 1 开始 2 结束
            'maxPlayer' => $maxPlayer, 
            'connectionIds' => [], 
            'playersInfo' => [], //玩家数据
            'readyStatus' => [], //准备好的玩家
            'deadStatus' => [], //死掉的玩家
            'createTime' => time(),
            'updateTime' => time(),
        ];
    }

    /**
     * 随机获取房间
     */
    public static function getRandomRoom($type = 0)
    {
        $roomId = '';
        //房间是不是有空位
        foreach(self::$roomList as $id => $info)
        {
            if($info['type'] != $type || $info['status'] != 0)
                continue;
            if(count($info['connectionIds']) < $info['maxPlayer'])
                $roomId = $id;
        }
        //没空房间，返回新的房间ID
        if($roomId == '')
            $roomId = uniqid();
        return $roomId;
    }

    /**
     * 返回房间内的所有用户连接
     */
    public static function getConnections($roomId)
    {
        if(!isset(self::$roomList[$roomId]))
            return [];
        else
            return self::$roomList[$roomId]['connectionIds'];
    }

    /**
     * 删除房间内的用户连接
     */
    public static function deleteConnection($connectionId)
    {
        if(isset(self::$connectionIdToRoomId[$connectionId]))
        {
            $roomId = self::$connectionIdToRoomId[$connectionId];
            unset(self::$connectionIdToRoomId[$connectionId]);
            //如果房间还没被强制回收
            if(isset(self::$roomList[$roomId]))
            {
                $openId = array_search($connectionId, self::$roomList[$roomId]['connectionIds']);
                if($openId !== false)
                {
                    if(isset(self::$roomList[$roomId]['connectionIds'][$openId]))
                        unset(self::$roomList[$roomId]['connectionIds'][$openId]);
                    if(self::$roomList[$roomId]['status'] == 0 && isset(self::$roomList[$roomId]['playersInfo'][$openId]))
                        unset(self::$roomList[$roomId]['playersInfo'][$openId]);
                    if(self::$roomList[$roomId]['status'] == 0 && isset(self::$roomList[$roomId]['readyStatus'][$openId]))
                        unset(self::$roomList[$roomId]['readyStatus'][$openId]);
                }
            }
        }
    }

    public static function broadcast($roomId, $event)
    {
        $data = self::$roomList[$roomId];
        return [
            'event' => $event,
            'roomId' => $roomId,
            'gameStatus' => $data['status'],
            'playersInfo' => $data['playersInfo'],
            'readyOpenId' => array_keys($data['readyStatus']),
        ];
    }

    /**
     * 定时器回调方法
     */
    public static function gameStatus($roomId)
    {
        $status = self::$roomList[$roomId]['status'];
        $type = self::$roomList[$roomId]['type'];
        switch($status)
        {
            case 0:
                //准备游戏，如果准备玩家大于等于最少开始数量，游戏开始
                if(count(self::$roomList[$roomId]['readyStatus']) >= self::MIN_ROOM_PLAYER)
                {
                    if(!isset(self::$roomList[$roomId]['readyStartTime']))
                        self::$roomList[$roomId]['readyStartTime'] = time();

                    if(time() - self::$roomList[$roomId]['readyStartTime'] > 2)
                    {
                        self::$roomList[$roomId]['status'] = 1;//开始
                        self::$roomList[$roomId]['updateTime'] = time();
                        return [self::broadcast($roomId, 'game_start'), self::getConnections($roomId)];
                    }
                }
                else
                {
                    //如果用户都退出或者超过1分钟没消息，回收
                    if(count(self::$roomList[$roomId]['connectionIds']) == 0 || time() - self::$roomList[$roomId]['updateTime'] > 120)
                    {
                        GameServer::getInstance()->removeTimer($roomId);
                        unset(self::$roomList[$roomId]);
                    }
                    //如果匹配不到用户，使用机器人
                    if($type == 1 && time() - self::$roomList[$roomId]['updateTime'] > 10)
                        return self::addRobot($roomId);
                    return [null, null];
                }
                break;
            case 1:
                //游戏中，如果玩家都死掉了
                if(count(self::$roomList[$roomId]['readyStatus']) == count(self::$roomList[$roomId]['deadStatus']) || count(array_diff(self::$roomList[$roomId]['connectionIds'], self::$roomList[$roomId]['deadStatus'])) == 0)
                {
                    self::$roomList[$roomId]['status'] = 2;//结束
                    self::$roomList[$roomId]['updateTime'] = time();
                    return [self::broadcast($roomId, 'game_over'), self::getConnections($roomId)];
                }
                else
                {
                    //如果用户都退出或者超过1分钟没消息，回收
                    if(count(self::$roomList[$roomId]['connectionIds']) == 0 || time() - self::$roomList[$roomId]['updateTime'] > 60)
                    {
                        GameServer::getInstance()->removeTimer($roomId);
                        unset(self::$roomList[$roomId]);
                    }
                    //如果有机器人，更新状态
                    if($type == 1 && isset(self::$robotList[$roomId]))
                        return self::doRobot($roomId);
                    return [null, null];
                }
                break;
            case 2:
                //游戏结束，如果无连接或者结束时间超过1分钟，回收
                if(count(self::$roomList[$roomId]['connectionIds']) == 0 || time() - self::$roomList[$roomId]['updateTime'] > 60)
                {
                    GameServer::getInstance()->removeTimer($roomId);
                    unset(self::$roomList[$roomId]);
                }
                return [null, null];
                break;
            default:
                return [null, null];
        }
    }

    public static function doRobot($roomId)
    {
        $openId = self::$robotList[$roomId];
        $score = self::$roomList[$roomId]['playersInfo'][$openId]['score'];
        $level = self::$roomList[$roomId]['playersInfo'][$openId]['level'];
        $maxScore = $level < 5 ? 99 * $level * 1.05 : 199 * $level * 1.2;
        self::$roomList[$roomId]['playersInfo'][$openId]['score'] = $score + 10;
        self::$roomList[$roomId]['updateTime'] = time();

        if($score > $maxScore)
        {
            self::$roomList[$roomId]['playersInfo'][$openId]['isDead'] = 1;
            self::$roomList[$roomId]['deadStatus'][$openId] = self::$roomList[$roomId]['connectionIds'][$openId];
            unset(self::$robotList[$roomId]);
            $message = [
                'event' => 'dead',
                'openId' => $openId
            ];
            return [$message, self::getConnections($roomId)];
        }
        $message = [
            'event' => 'update_score',
            'openId' => $openId,
            'score' => $score
        ];
        return [$message, self::getConnections($roomId)];
    }

    public static function addRobot($roomId)
    {
        //已有人数
        $total = count(self::$roomList[$roomId]['connectionIds']);
        //房间已满
        if($total >= self::$roomList[$roomId]['maxPlayer'])
            return [null, null];

        $openId = substr(md5(uniqid(mt_rand(), true)), 0, -4);
        $connectionId = rand(50000, 60000);
        $avatar = '';
        $level = rand(1, 10);

        self::$roomList[$roomId]['connectionIds'][$openId] = $connectionId;
        $nameArr = ['乐','静','醉','潇','糊','极','冷','情','柔','爱','快','义','真','威','帅','统','亮','然','一','听','睡','待','默','活','心','兴','超','率','轻','痴'];
        $nameIdx = rand(0, 29);
        $name = $nameArr[$nameIdx];
        $nameIdx = rand(0, 29);
        $name .= $nameArr[$nameIdx];
        $avatar = 'https://api.sg.jiawanhd.com/nxfxj/public/static/robot_avatar/'.rand(1, 10).'.jpg';

        self::$roomList[$roomId]['playersInfo'][$openId] = [
            'name' => $name,
            'avatar' => $avatar,
            'level' => $level,
            'order' => $total,
            'isDead' => 0,
            'isReady' => 1,
            'score' => 0
        ];

        self::$robotList[$roomId] = $openId;

        self::$roomList[$roomId]['readyStatus'][$openId] = $connectionId;
        return [self::broadcast($roomId, 'join_room'), self::getConnections($roomId)];
    }

}