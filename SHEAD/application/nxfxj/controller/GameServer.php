<?php

namespace app\nxfxj\controller;

use Workerman\Lib\Timer;

class Gameserver
{
    private static $instance = null;
    private $connectionList = [];
    private $timerCallbackList = [];

    public static function getInstance()
    {
        if(is_null(self::$instance))
            self::$instance = new self();

        return self::$instance;
    }

    /**
     * 当连接建立时触发的回调函数
     * @param $connection
     */
    public function onConnect($connection)
    {
        $this->connectionList[$connection->id] = $connection;
    }

    /**
     * 收到信息
     * @param $connection
     * @param $data
     */
    public function onMessage($connection, $data)
    {
        $data = json_decode($data, true);
        try
        {
            if(!isset($data['command']))
                throw new \Exception("command not exists!");

            if(!isset($data['action']))
                throw new \Exception("action not exists!");

            $command = $data['command'];
            $action = $data['action'];
            list($message, $connectionIds) = $this->doCommand($connection, $command, $action, $data);
            if(empty($connectionIds))
                $connectionIds = [$connection->id];

            foreach($connectionIds as $connectionId)
            {
                if (isset($this->connectionList[$connectionId]))
                    $this->connectionList[$connectionId]->send($this->getMessage($message));
            }
        }
        catch(\Exception $e)
        {
            $connection->send($this->getErrorMessage($e->getMessage()));
        }
    }

    /**
     * 当连接断开时触发的回调函数
     * @param $connection
     */
    public function onClose($connection)
    {
        unset($this->connectionList[$connection->id]);
        Room::deleteConnection($connection->id);
    }

    /**
     * 当客户端的连接上发生错误时触发
     * @param $connection
     * @param $code
     * @param $msg
     */
    public function onError($connection, $code, $msg)
    {
    }

    public function startTimer()
    {
        Timer::add(1, [$this, 'doTimer']);
    }

    public function addTimer($index, $callback)
    {
        $this->timerCallbackList[$index] = $callback;
    }

    public function removeTimer($index)
    {
        unset($this->timerCallbackList[$index]);
    }

    public function doTimer()
    {
        foreach($this->timerCallbackList as $index => $callback)
        {
            try
            {
                list($data, $connectionIds) = $callback();
                if(is_null($data))
                    continue;
            }
            catch(\Exception $e)
            {
                $this->removeTimer($index);
                continue;
            }
            foreach($connectionIds as $connectionId)
            {
                if(isset($this->connectionList[$connectionId]))
                    $this->connectionList[$connectionId]->send($this->getMessage($data));
            }
        }
    }

    private function doCommand($connection, $command, $action, $data)
    {
        $command = ucfirst($command);
        $className = "app\\nxfxj\\controller\\".$command;
        if(!class_exists($className))
            throw new \Exception("command[".$command."] not found");

        $object = new $className($connection, $data);
        if(!method_exists($object, $action))
            throw new \Exception("action[".$action."] not found");

        return $object->$action();
    }

    private function getErrorMessage($message)
    {
        return json_encode([
            'code' => 1,
            'msg' => $message,
        ]);
    }

    private function getMessage($data)
    {
        return json_encode([
            'code' => 0,
            'msg' => 'success',
            'data' => $data
        ]);
    }
}