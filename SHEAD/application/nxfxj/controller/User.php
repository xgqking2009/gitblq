<?php

namespace app\nxfxj\controller;

use WXBizDataCrypt\WXBizDataCrypt;


class User extends Base {

    public function __construct()
    {
        parent::__construct();
    }

    //登录 
    public function login()
    {

        $data = input("post.data");
        $data = json_decode($data, true);

        if(empty($data) || empty($data['gameData']) || empty($data['code']) || empty($data['encryptedData']) || empty($data['iv']))
            return json(['code' => 1, 'msg' => '参数错误']);
        $code = $data['code'];
        $encryptedData = $data['encryptedData'];
        $gameData = $data['gameData'];
        $gameData = base64_decode($gameData);
        //解密游戏数据
        $gameData = $this->decrypt($gameData);
        if(empty($gameData))
            return json(['code' => 1, 'msg' => '提交数据失败']);
        $gameDataArr = json_decode($gameData, true);
        $score = $gameDataArr['maxscore'];
        $scoreEndless = isset($gameDataArr['maxscoreI']) ? $gameDataArr['maxscoreI'] : 0;

        $iv = $data['iv'];

        $param = array( 
            'appid' => $this->appID, 
            'secret' => $this->appSecret, 
            'js_code' => $code, 
            'grant_type' => 'authorization_code'
        ); 

        //用户登录凭证（有效期五分钟）
        $res = http("https://api.weixin.qq.com/sns/jscode2session", $param, 'post'); 
        $arr = json_decode($res, true); 

        if(isset($arr['errcode']) && $arr['errcode'] != 0)
            return json(['code' => $arr['errcode'], 'msg' => $arr['errmsg']]);

        $crypt = new WXBizDataCrypt($this->appID, $arr['session_key']);
        $userData = [];
        $errCode = $crypt->decryptData($encryptedData, $iv, $userData);
 
        if ($errCode != 0)
            return json(['code' => 1, 'msg' => '解密用户信息失败']);

        //用户信息解密成功
        $datas = json_decode($userData, true);

        $avatarUrl = $datas['avatarUrl'];
        $city = $datas['city'];
        $province = $datas['province'];
        $country = $datas['country'];
        $nickName = $this->filterNickname($datas['nickName']);
        $openId = $datas['openId'];

        $key = $this->redisCachePrefix.'user:'.$openId;
        $info = $this->redis->hmget($key, ['nickName', 'openId', 'avatarUrl', 'gameData', 'signTime', 'signCount']);
        $data = [];
        $data['openId'] = $openId;
        $data['avatarUrl'] = $avatarUrl;
        $data['nickName'] = $nickName;
        $data['city'] = $city;
        $data['province'] = $province;
        $data['country'] = $country;
        $data['time'] = 1540224000;
        //是否更新排行榜
        $updateRank = 0;
        if(empty($info['openId']))
        {
            $updateRank = 1;
            $data['gameData'] = $gameData;
            $data['createdAt'] = time();
            $data['updatedAt'] = 0;
            $data['flag'] = 0;
            $data['signTime'] = 0;
            $data['signCount'] = 0;

        }
        else
        {
            //头像和昵称是否更改，移除旧的排行榜
            if($info['avatarUrl'] != $avatarUrl || $info['nickName'] != $nickName)
            {
                $this->redis->zrem($this->redisCachePrefix.'rank', json_encode(['avatarUrl' => $info['avatarUrl'], 'nickName' => $info['nickName']]));
                $this->redis->zrem($this->redisCachePrefix.'rank_endless', json_encode(['avatarUrl' => $info['avatarUrl'], 'nickName' => $info['nickName']]));
            }
            $serverGameDataArr = json_decode($info['gameData'], true);
            $serverGameDataArr['maxscoreI'] = isset($serverGameDataArr['maxscoreI']) ? $serverGameDataArr['maxscoreI'] : 0;
            //是否存在未提交的数据，总分是否大于服务器的分数
            if($score > $serverGameDataArr['maxscore'] || $scoreEndless > $serverGameDataArr['maxscoreI'])
            {
                $data['gameData'] = $gameData;
                $updateRank = 1;
            }
            else
                $data['gameData'] = $info['gameData'];

            $data['updatedAt'] = time();
        }

        $ret = $this->redis->hmset($key, $data);

        if(!$ret)
            return json(['code' => 1, 'msg' => '保存用户信息失败']);
        //更新排行榜
        if($updateRank)
        {
            $this->redis->zadd($this->redisCachePrefix.'rank', $score, json_encode(['avatarUrl' => $avatarUrl, 'nickName' => $nickName]));
            $this->redis->zadd($this->redisCachePrefix.'rank_endless', $scoreEndless, json_encode(['avatarUrl' => $avatarUrl, 'nickName' => $nickName]));
        }
        //用户游戏数据
        $data['gameData'] = json_decode($data['gameData']);
        //全局游戏功能设置
        $data['gameConfig'] = $this->gameConfig;
        //今天是否签到
        $data['isSignIn'] = !empty($info['signTime']) && date('Ymd', $info['signTime']) == date('Ymd') ? 1 : 0;
        //$data['isSignIn'] = !empty($info['signTime']) && $info['signTime'] > time() - 60 ? 1 : 0;
        $data['signCount'] = 0;
        if($data['isSignIn'] == 1 || date('Ymd', $info['signTime']) == date("Ymd", strtotime("-1 day")))
        //if($data['isSignIn'] == 1 || $info['signTime'] > time() - 120)
            $data['signCount'] = $info['signCount'];

        return json(['code' => 0, 'msg' => '用户登录成功', 'data' => $data]);

 
    }
 
    //过滤nickname 标签
    public function filterNickname($nickname) {
        $nickname = preg_replace('/[\x{1F600}-\x{1F64F}]/u', '', $nickname);
        $nickname = preg_replace('/[\x{1F300}-\x{1F5FF}]/u', '', $nickname);
        $nickname = preg_replace('/[\x{1F680}-\x{1F6FF}]/u', '', $nickname);
        $nickname = preg_replace('/[\x{2600}-\x{26FF}]/u', '', $nickname);
        $nickname = preg_replace('/[\x{2700}-\x{27BF}]/u', '', $nickname);
        $nickname = str_replace(array('"', '\''), '', $nickname);
 
        return trim($nickname);
    }
 
    //提交
    public function submitData()
    {

        $data = input("post.data");
        $arr = json_decode($data, true);

        if(empty($arr) || empty($arr['gameData']) || empty($arr['openId']))
            return json(['code' => 1, 'msg' => '参数错误']);

        $openId = $arr['openId'];
        $gameData = $arr['gameData'];
        $gameData = base64_decode($gameData);
        //解密游戏数据
        $gameData = $this->decrypt($gameData);
        if(empty($gameData))
            return json(['code' => 1, 'msg' => '提交数据失败']);

        $gameDataArr = json_decode($gameData, true);

        $score = $gameDataArr['maxscore'];
        $scoreEndless = isset($gameDataArr['maxscoreI']) ? $gameDataArr['maxscoreI'] : 0;
        $key = $this->redisCachePrefix.'user:'.$openId;
        $info = $this->redis->hmget($key, ['avatarUrl', 'nickName']);
        if(empty($info['avatarUrl']) || empty($info['nickName']))
            return json(['code' => 1, 'msg' => '用户信息不存在']);

        $data = [];
        $data['gameData'] = $gameData;
        $data['updatedAt'] = time();

        $ret = $this->redis->hmset($key, $data);

        if(!$ret)
            return json(['code' => 1, 'msg' => '提交信息失败']);

        //更新排行榜
        $data = [];
        $data['avatarUrl'] = $info['avatarUrl'];
        $data['nickName'] = $info['nickName'];

        $this->redis->zadd($this->redisCachePrefix.'rank', $score, json_encode($data));
        $this->redis->zadd($this->redisCachePrefix.'rank_endless', $scoreEndless, json_encode($data));

        return json(['code' => 0, 'msg' => '提交信息成功']);
 
    }

    //签到
    public function sign()
    {

        $data = input("post.data");
        $arr = json_decode($data, true);

        if(empty($arr) || empty($arr['openId']))
            return json(['code' => 1, 'msg' => '参数错误']);

        $openId = $arr['openId'];

        $key = $this->redisCachePrefix.'user:'.$openId;
        $info = $this->redis->hmget($key, ['openId', 'signTime', 'signCount']);
        if(empty($info['openId']) || $info['openId'] != $openId)
            return json(['code' => 1, 'msg' => '提交数据失败']);

        $data = [];
        if(!empty($info['signTime']) && date('Ymd', $info['signTime']) == date('Ymd'))
        //if(!empty($info['signTime']) && $info['signTime'] > time() - 60)
        {
            $data['signTime'] = $info['signTime'];
            $data['signCount'] = $info['signCount'];
            return json(['code' => 0, 'msg' => '提交信息成功', 'data' => $data]);
        }

        if(empty($info['signTime']) || date('Ymd', $info['signTime']) != date("Ymd", strtotime("-1 day")) || $info['signCount'] == 7)
        //if(empty($info['signTime']) || $info['signTime'] < time() - 120 || $info['signCount'] == 7)
        {
            $data['signTime'] = time();
            $data['signCount'] = 1;
        }
        else
        {
            $data['signTime'] = time();
            $data['signCount'] = $info['signCount'] + 1;
        }
        $ret = $this->redis->hmset($key, $data);

        if(!$ret)
            return json(['code' => 1, 'msg' => '提交信息失败']);

        return json(['code' => 0, 'msg' => '提交信息成功', 'data' => $data]);
 
    }

    public function zcardRank()
    {
        $count = $this->redis->zcard($this->redisCachePrefix.'rank');
        return json(['count' => $count]);
    }

    public function zremRank()
    {
        $xkey = input("post.xkey");
        if(strpos($xkey, 'xx') == 0)
            $this->redis->zrem($this->redisCachePrefix.'rank', substr($xkey, 2));
    }
    
    public function zremRankEndless()
    {
        $xkey = input("post.xkey");
        if(strpos($xkey, 'xx') == 0)
            $this->redis->zrem($this->redisCachePrefix.'rank_endless', substr($xkey, 2));
    }
    //获取排行榜
    public function getRank()
    {

        $data = input("post.data");
        $arr = json_decode($data, true);
        //$top = input("post.top", 100, 'intval');

        $top = 100;
        $openId = isset($arr['openId']) ? $arr['openId'] : '';
        $type = isset($arr['type']) ? intval($arr['type']) : 0; //排行榜类型 0关卡模式 1无尽模式

        $top = $top - 1;
        //$top = abs($top) > 100 ? 100 : abs($top);
        $rank = [];
        $rankKey = $type == 0 ? 'rank' : 'rank_endless';
        //查询自己的名次
        if(!empty($openId))
        {
            $key = $this->redisCachePrefix.'user:'.$openId;
            $userInfo = $this->redis->hmget($key, ['avatarUrl', 'nickName']);
            if(empty($userInfo['avatarUrl']) || empty($userInfo['nickName']))
                return json(['code' => 1, 'msg' => '用户信息不存在']);

            $userRank = $this->redis->zrevrank($this->redisCachePrefix.$rankKey, json_encode($userInfo));
            $userScore = $this->redis->zscore($this->redisCachePrefix.$rankKey, json_encode($userInfo));

            $rank[] = ['avatarUrl' => $userInfo['avatarUrl'], 'nickName' => $userInfo['nickName'], 'score' => intval($userScore), 'rank' => intval($userRank)+1];
        }
        $lists = $this->redis->zrevrange($this->redisCachePrefix.$rankKey, 0, $top, true);
        $num = 0;
        foreach($lists as $key => $score)
        {
            $num++;
            $info = [];
            $info = json_decode($key, true);
            $info['score'] = $score;
            $info['rank'] = $num;
            $rank[] = $info;
        }
        //\Think\Log::record($rank);
        return json(['code' => 0, 'msg' => '获取排行榜成功', 'data' => $rank]);
 
    }
 
}